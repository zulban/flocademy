"Run this file with 'locust', a stress testing framework."
"locust -f locust_anonymous_student --host=http://flocademy.ca"

import random

from locust import HttpLocust, TaskSet, task

from constants import PROMPT_LIST
SEARCH_TERMS = [
    "",
     "science",
     "personal",
     "medicine",
     "nope",
     "yes",
     "what",
     "relativity",
     "links",
     "made",
     "scene",
     "3d",
     "model",
     "css",
     "html",
     "javascript",
     "python"]
UUIDS = [
    "6204d5c19d5148ce832a57e7e91bb25a",
    "b9017193cf1640ceace52e5e9c270e49",
    "cf0791058f364674962e2937605888a7",
    "4099323171d34fbaa3cb7072f7f711b8",
    "22aef763cb7f495dbc3db4e740b3c96d",
    "969d124613d3479a94ba67cdc69f369f",
    "7b27f07be20b436ebeab52a5e852d3fd",
    "7c2607cbf3884c5092b0e055e5c19ccd",
    "84146fbc83d24a08b6923895e188d05e",
    "3fb38aae176b42b6a82946f1632edddc",
    "c2cbea2c72bb4a389d8a2f41dfcc34b4",
    "c5908fe55ae6488ab0dbc917cd342a29",
    "8e9edaae89bc4961bff339dc38d4a723",
    "7b56ff7be9f246a68c5b3cd5fc7d50a7",
    "e87fec32b8a84d4481243ced2a197bbe",
    "c733b48abb0443b084a4a0efe1bc6feb",
    "e06918e91fa747298c11378625a3a834",
    "05acc780fd404faa91bd96db90518d0b",
    "363ec14bceca49688f74400fedcdd0c3",
    "013002d0a0f6401a9a38fb495c7ad815",
    "cafde9a78a514d43befdd1745f78bd21",
    "7645f0889de240928b33a430b6196a87",
    "45734a20d14b42e38550634e31c7defb",
    "959ff3e9a80d46afaf5923e560d8ed3b",
    "18fcc1b7b4d2475ba7cdabbdbee88f20",
    "1bd48b9de1f44a598a7c5819590e420c",
    "ad9f1a47e4714eef9a00a18e1724a69a",
    "833238332ac542bfaf48e01cf982d652",
    "678bbf3d3cdd4ec09722acba9491f1da",
    "a798b2b110cf4a459190483cf664e912",
    "ed276a92824c4d998d8963f66c6b2ce0",
    "052226cda4334a7dba5b92ae37c90c5c",
    "fd0cd78d24d7487d8bd6ea83c1411e62",
    "ad862262375d43a691374f5b224dea22",
    "9ce045df371d4d29a603cd8b6039ab82",
    "002291043a7341b1a7a57274845b1f1d",
    "df87b8b0f1484ce0bb4365cd2a7337dd",
    "52f0cad7e4e3467ba7b4ea650d53759b",
    "3c3b877a023640a3a7ba9edbd6972d9c",
    "75b927b9408445a883fa6c95bc67a71c",
    "c39f1732b62c4713a3a02f2a89d00b43",
    "7ca0ccb8b91e4c34a1909553f7c75590",
    "f49f6c506c684f43b3c80335bc7e1742",
    "dfd21a96aa7b475ab913dcee2c795f10",
    "40102fd7437542ac97a052f0a88e17b2",
    "c06e88d7534948af9a5dda0cf94a37df",
    "5b6679a0424949a883648b9339f4c366",
    "bd1d6557b9de4f83acb32caadfde9b6f",
    "a058ae92a6524c368069de95da4c67cd",
    "cc3beba6b2644a39a06aa5801eb3d018",
    "3a00642aa4014664ab254e264e4e3d1f",
    "bfabba59e9b24a06a8b86ecfb699cfe7",
    "21aff3fb784842c683ff8f04e8a6f48d",
    "c9e666f5b7924fbb9699636508c528a5",
    "eec7af394a7943c3842b2fb78d9a89f7",
    "a2e361a4e60745b7b8c8030b9c554bc2",
    "0c54472d2786479eba0ea368839d8761",
    "ac9561bb8778494399690176130f594e",
    "de87db8ae7604bfeaec2c8a39dbaa403",
    "38691fb78bd84f9f8cd56ab1c2fe5496",
    "31d62e90759b4303974c8f1e66e0a66c",
    "f302b101d5384854949a24461f3c7caf",
    "24701c16ac1246b1844ceca1643cf8d3",
    "190fdd814c394524b0e37c7b584ae8d9",
    "c05c5c096d874c1591de4b74029eba8f",
    "7a710923dc354f709b452418a401e8f6",
    "ca0282faa2534632bfc00363ad651990",
    "b57ea36b345449d089496543a3e751fa",
    "4d41556b585845898910d741b1ebbdc6",
    "2c0b8111dc8044a7945536f5fc26c036",
    "7e5b78aeff5b484893070882cd19f003",
    "ed31238041f54824bc87bae7a6b98c74",
    "6312c2aa78034018b5d594c587930834",
    "67a59a68f277479c8a2817be7221dfc4",
    "c9cf1055715c48868459cd5c376663ca",
    "4453fc90ba994349b3a290e86389acff",
    "83736bded2614eff9f306eeee315fa63",
    "09cfef0b99664a3fa76f56fb47a1d69b",
    "43cf516df0ab4b7782250c73b19ee5ac",
    "616cf0bad3b249ec90e75ca3b20993cf",
    "9ed85bdbe8ed4f75b34fe3dabe7502b9",
    "a2760c686a334dc0a5eb106c93eced5c",
    "a18e9b08d34449c5b301187276b28187",
    "5146ddb0ac414b39b38c03539335e766",
    "beee826e5fc64209a61b8cd0b918ab12",
    "319c3b9038e34dea979de40ab00e5208",
    "50f43b04743d4cb9aa92be6fb8e47a58",
    "0dd3e333d60247e1a3586f4e7cb83857",
    "d8d465a627d24aa3a0b70ca815ee0fe0",
    "8a4a67fe8e0246f48e2233baedfbe212",
    "dd2f875e312342ac96602f6a313a9776",
    "64ae032db1564d038b33b2a4507e0ac3",
    "698790c003aa45bbbb2e60ffb1e50dfb",
    "90e1bc31299741f9bc09bb064ae2de8c",
    "d861c343527d46cd9ab371106c006607",
    "f4b7765e30b948fb8b042059b80ca666",
    "9b831d7eb5234581bddfaf78a3de2562",
    "890b707fdfca4919b50562e8a9065534",
    "f610d3d4b8d64d10960be94546d73572",
    "b3683cceb20e4e9fa20a30bf58a0bc4b",
    "e15c438b17a5485ea096f5926175159e",
    "e80915987f6f4101a3def4f08f016a64",
    "ec73fd60e4ab4d83910a8547e286f8bb",
    "df621445b0df46c4b3ff050862582624",
    "3b147a3fb79544b6a13f78ccf9c9f06e",
    "5abe7e8b1f32485b83a1977160ec4d60",
    "c2f96131eede4d938bfb8210037229ae",
    "789e3d709c504fa5ad099d07903397e0",
    "b1e813cb9acf4f40912fa21ae5057701",
    "8e52bc619cba4f22b296d325722fd015",
    "6a319a681969452ab86d513c39e9467d",
    "1f30b31457df41ccbb2189ffefa62595",
    "e443430f6d1e4b43bd9711862985ee0b",
    "648356f1ebe34365a20c8dff662c8c86",
    "06bfaa01df7f460a81adbdd344ea2e30",
    "e83569b351734128b66334bc2f05184e",
    "93b0843d74454b84b3f2dc477c53838a",
    "3c9cd05ff11740bbacf63dea8c809e9c",
    "99b8d7c826c3495e8c4033618b56fa70",
    "c8d5bee8f15d4cf0b89e5e4e751a4765",
    "9e27c7ba652a4ea9840468b86f54faa4",
    "a95118de1dc84c35afb6916e92d63a54",
    "fff3d963ff914217ba5d322b93677ed2",
    "8bd1e38e37f942cea1adaf438e3a4523",
    "c96fcae591bb4b868887e803ec559f07",
    "db08333c71ff45378008296010f08977",
    "d6c3584d5a54451b91b89fa8da6e0356",
    "9f33977ef55542a09cfe88094dae2848",
    "da77ed01ca6a4ccaabdf5dfeafc26d93",
    "978f3a2a0ca5445d95bcaa37a3469c38",
    "9a10c4aa89d64f068e5bf732198424d4",
    "8c7a0debf00b4274bb3184425bfe6b64",
    "e4612812156e4625908990f4ac103b03",
    "2a4f8f9565a14d808a35c0d18d88a683",
    "5fac84bcc5c34ff2a32012726754a80f",
    "e463efea79044545a30e410bfb2bacc0",
    "260798edb57747ceafc2d9de6182b6ad",
    "ebe8b267ec654730a85d4fdded44e03d",
    "3c13740cef58404ebd27710884556d17",
    "ff18e97549554e77a7c382cb7806cca1",
    "ffab9f40af974f9aac4a29e58db9e3c2",
    "b47e0de33a3b4acfa6884838f1c07b0f",
    "5a28e5b000964bc6958593d5815efcb8",
    "95d3ebe256b440a4bca697062d815e07",
    "04c9fff1c8184dedad1c20c5573fc0b9",
    "d56862ec61d64fef871b567c1d090cb1",
    "a89e466cd502401cb86e16d65cae87c1",
    "834261da2aaa4d86a19297bb34917fc0",
    "09fc18ae8b304485bb4ce08416fd63a7"
]


TAG_UUIDS = [
    "589b29a090664b21a5ae9c10fd77de36",
    "968084c4173043d5a18485e8b285b16c",
    "a5699b8fc09e4524a6e08cdf7472a12b",
    "5fcc284b739d48da93c090929e014163",
    "36ff11a2cb7342069d4955ba8a4511f0",
    "2f53299dcea64cf9932485053fc692e1",
    "78e1a75b699e4e0a8be90cde77b71d9c",
    "af979fee03ab4a2391b583268170ea32",
    "9b919d9eb61d4effb962a41343882077",
    "3b009b70706a488b815b423020bc57c0",
    "10031feba0524a4cb5c7cb03e186fd0f",
    "9d6a6a894f744b3d9b2865d38c10997a",
    "0837b8bb339840a48564d7a877028054",
    "1966b70f3ddc431b90324ea5bdd27766",
    "974510fe3fd2443cabecaa6abb5282e8",
    "c4ca9ecf99db420abab05fd4cdaab7d1",
    "63b57f2556fa48b4b4361510fa885c4a",
    "cb3f06a7181740b0885b0a83c0bdad04",
    "caf65ca4b1a3456cb55fb07616eebd70",
    "7b3d6fcdb6df4ff48e61019124ee4ef8",
    "2472dc1baad04ae3b6b767fcdeb428ae",
    "32ef02e387dd48ebbfb2056b06a58412",
    "bc49a10af2e2404abd7160cc3f675fae",
    "801e91cb81d242ac970d2ace3551d937",
    "e984f047062b4dcaa4f19073bba2e97b",
    "96005b3dc6f64e4098033b0b60639184",
    "0ac7bdb1e41e42e68bf4e62a4792c7f6",
    "510ad995da3d409ca0afba733e87a2d7",
    "59a5976508154a4983283af1e3891f75",
    "36989b4ccb1a40b59c22da3875d6188e",
    "e8a0590c3e7d472d8e306ec6206b7f40",
    "dcedc0b3b31f42fcbddc5b23042740b7",
    "a1f3838556234bd595cbaf16a0e919a1",
    "b1925d3a0f9c44069af2e02b8785ad95",
    "62257a28d62545c9872a0d839276a04d",
    "c320dd3cfd3c470d8fd7eac5f3dcf1e9",
    "8244c7727e5a4d83919ea96fe15f8d6d",
    "4e5d3e0d59d6472bbc9d7a8fb6a2216e",
    "e8deb5a1fefd4fffb1e496407ee68161",
    "5b0203162b604ed09a1bb8f63d627777",
    "f9f62ee04a8d4d6d900f6e453b706ec9",
    "cf0f705dec1943348b297ec51d076ac4",
    "2ecf6c43572e450b99b616503c2fc69c",
    "f40f1a3573954d7b8229717ffa9f6c08",
    "ed2fe2306e6b4626acdebe8723e6b3cc",
    "12a2c3b6171a499da2db9a3dc6844d9c",
    "1dddcbcdb2ae47f691f3e069975ff4a7",
    "c0788d6fa2264548adc10a76eec85e31",
    "61de135d4c4d454f93e75e1f1222c5a3",
    "33be8fa4cfc248448644fb9cf045207a",
    "e320657d80f44df88b9cff78347c505f",
    "6562e8d25dac45dda3e1444eb1d061bc",
    "124e03ed1cef478ba8d3128741674fb4",
    "d83f21d46883404a99aeb52f4cf87c98",
    "7c894bf073144d8e872625b8bdc30fc0",
    "a51fe92614c84935b66249b5f77a28f8",
    "60bfa8831cea407b99834bbce7f21300",
    "5e3372edabe94fb4a0340b3b12773dda",
    "d21e65972e9b484b8ed96a97274b6b20",
    "72cac0bf55744634b36d16306bf3f830",
    "469893912724402c98468044e216df1c",
    "22ee59fe69614d02b04edf56eb646b51",
    "4b1b421f4c2142359c779c4158fb0f34"
]


class AnonymousTasks(TaskSet):

    def on_start(self):
        self.client.post("/login", {
            "username": "test_user",
            "password": ""
        })

    @task(10)
    def home(self):
        self.client.get("/")
        self.client.get("/api/next_question")
        self.client.get("/api/feed?number=10")

    @task(1)
    def search(self):
        query = random.choice(SEARCH_TERMS)
        self.client.get("/search?q=" + query)

    @task(3)
    def render_flo(self):
        uuid = random.choice(UUIDS)
        self.client.get("/flo?uuid=" + uuid)
        self.client.post("/api/get_plank", {"_id": uuid})

    @task(1)
    def set_prompt_answer(self):
        prompt_name = random.choice(PROMPT_LIST)
        answer = "1"
        self.client.post(
            "/api/set_prompt_answer",
            {"prompt_name": prompt_name,
             "answer": answer})

    @task(5)
    def send_answer(self):
        uuid = random.choice(TAG_UUIDS)
        params = {"_id": uuid,
                  "index": random.choice((0, 1)),
                  "next": random.choice((0, 1))}
        self.client.get(
            "/api/send_answer?uuid={uuid}&index={index}&weight=0.6&get_next_question={next}".format(**params))
        self.client.get("/api/feed?number=10")


class WebsiteUser(HttpLocust):
    task_set = AnonymousTasks
    min_wait = 2000
    max_wait = 10000
