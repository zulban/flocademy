
import unittest
import json
import time
from app import app, db
from utilities.colors import *
from constants import TestingSettings
from utilities import show_duration, DEFAULT


class FLOcademyTest(unittest.TestCase):

    """This is a generic abstract class that all FLOcademy tests should inherit from."""

    @classmethod
    def setUpClass(cls):
        cls.class_start_time = time.time()
        db.configure(TestingSettings, app=app)

    def setUp(self):
        self.start_time = time.time()
        self.app = app.test_client()
        self.assertTrue(app.config["TESTING"])

    @classmethod
    def tearDownClass(cls):
        label = "Class " + str(cls).split(".")[-1][:-2]
        show_duration(label, cls.class_start_time, print_function=print_orange)

    def tearDown(self):
        self.logout()

        label = ".".join(self.id().split(".")[2:])
        show_duration(label, self.start_time)

    def get_matching_desires(self, uuid):
        """Returns matching desires, and the matching score for this uuid.
        Matching desires don't necessarily maximize score, they merely match the tag scopes for that uuid."""

        desires = {}    # desires = {uuid:(value,weight,desire_type)}
        tags = db.get_voted_tags_from_uuid(uuid)
        for tag_uuid in tags:
            scope = db.calculate_tag_scope_for_uuid(uuid, tag_uuid)
            desire_type = db.get_desire_type_from_tag_uuid(tag_uuid)
            desires[tag_uuid] = (scope, 1, desire_type)
        "Returns a dictionary instead of the regular tuple for testing modifications."
        desires = {key: list(desires[key]) for key in desires.keys()}
        matching_score = db.calculate_score_for_uuid_with_desires(
            uuid, desires)
        return desires, matching_score

    def assertNotInResponse(self, expected, rv, msg=""):
        data = str(rv.data)
        self.assertNotIn(expected, data, msg=msg)

    def assertInResponse(self, expected, rv, msg=""):
        data = str(rv.data.decode())
        
        replaces=("\n","\t")
        for c in replaces:
            if c in expected:
                raise ValueError("assertInResponse strips newlines and tabs for brevity in reports, but 'expected' has those chars.")
            data=data.replace(c,"")
            
        self.assertIn(expected, data, msg=msg)

    def assertSuccessResponse(self, rv):
        self.assertEqual(rv.status_code, 200)
        try:
            data = json.loads(rv.data.decode("utf-8"))
            self.assertEqual(type(data), dict)
            return data
        except:
            return {}

    def answer_question_for_tests(self, uuid, index):
        return self.app.get('/api/send_answer?uuid=%s&index=%s' % (uuid, index))

    def get_feed_uuids(self, offset=0, number=0):
        if not number:
            number = DEFAULT.FEED_LENGTH
        rv = self.app.get('/api/feed?number=%s&offset=%s' % (number, offset))
        feed = json.loads(rv.data.decode("utf-8"))["feed"]
        uuids = [item["uuid"] for item in feed]
        return uuids

    def register(self, username, password, confirm, email):
        return self.app.post('/register', data=dict(
            username=username,
            password=password,
            confirm=confirm,
            email=email), follow_redirects=True)

    def login(self, username, password):
        return self.app.post('/login', data=dict(
            username=username,
            password=password), follow_redirects=True)

    def logout(self):
        return self.app.get('/logout', follow_redirects=True)

    def is_logged_in(self):
        rv = self.app.get('/home')
        return "logout" in str(rv.data)
