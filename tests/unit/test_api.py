
import json
from testfixtures import compare as assertEqualCompare

from app import db
from flask_login import current_user

from tests.flocademy_test import FLOcademyTest
from utilities import *


class TestAPI(FLOcademyTest):

    @classmethod
    def setUpClass(cls):
        FLOcademyTest.setUpClass()
        db.load_baseline_data()

    def test_submit_flo(self):
        with self.app:
            self.logout()
            "create admin user so we can submit a flo"
            username, password = "test_submit_flo username", "abc123"
            db.create_user(username, password, "", 1)
            self.login(username, password)
            url = "api/admin/submit_flo"
            data = {
                "json_string": """{"submission":"123","recommended_searches":[],"schema_uuid":"123","instructions":"","mirrors":[],"uuid":"","label":"","description":"123","extra":""}"""}
            # anonymous answer
            rv = self.app.post(url, data=data)
            data = self.assertSuccessResponse(rv)
            self.assertIn("new_uuid", data)

    def test_embed_urls(self):
        flo = db.get_flo_from_uuid(UUID.LESSON)
        self.assertTrue(flo)
        msg=json.dumps(flo,indent=4,sort_keys=1)
        with_embed = db.get_mirrors_for_flo(flo, add_embed_urls=True)
        without_embed = db.get_mirrors_for_flo(flo, add_embed_urls=False)

        self.assertGreater(len(with_embed), 0, msg=msg)
        self.assertGreater(len(without_embed), 0, msg=msg)
        for mirror in with_embed:
            self.assertIn("embed_url", mirror)
        for mirror in without_embed:
            self.assertNotIn("embed_url", mirror)

    def test_get_flo(self):
        fv = FLOValidator(DATA_PATHS.SCHEMAS)

        url = "api/flo?uuid=" + UUID.LESSON
        rv = self.app.get(url)
        data = self.assertSuccessResponse(rv)
        self.assertIn("response", data)
        flo = data["response"]
        
        msg=fv.get_validation_error_message(flo)+"\n\nflo = "+str(flo)
        self.assertTrue(fv.is_valid_flo(flo),msg=msg)

    def test_next_question(self):
        def three_unique_questions(msg=""):
            url = "api/next_question"
            rv = self.app.get(url)
            data = self.assertSuccessResponse(rv)
            self.assertIn("next_question", data)            
            self.assertIn("uuid",data["next_question"])
            
            uuid = data["next_question"]["uuid"]
            self.assertTrue(is_string_a_uuid(uuid))

            url = "api/send_answer?index=1&uuid=" + \
                uuid + "&get_next_question=1"
            rv = self.app.get(url)
            self.assertSuccessResponse(rv)
            data = json.loads(rv.data.decode("utf-8"))

            next_uuid = data["next_question"]["uuid"]
            self.assertNotEqual(uuid, next_uuid, msg=msg)

            url = "api/send_answer?index=1&uuid=" + \
                next_uuid + "&get_next_question=1"
            rv = self.app.get(url)
            self.assertSuccessResponse(rv)
            data = json.loads(rv.data.decode("utf-8"))

            last_uuid = data["next_question"]["uuid"]
            self.assertNotEqual(
                uuid,
                last_uuid,
                msg="first question same as third question")
            self.assertNotEqual(
                next_uuid,
                last_uuid,
                msg="second question same as first question")

        with self.app:
            username = "test_api_username123"
            self.logout()
            three_unique_questions(
                msg="before '%s' login, anonymous" %
                username)
            self.register(username, MOCK_PASSWORD, MOCK_PASSWORD, "")
            three_unique_questions(msg="after '%s' login" % username)
            self.logout()

    def test_no_answers_get_feed_planks(self):
        def good_feed(params=""):
            rv = self.app.get("api/feed" + params)
            data = self.assertSuccessResponse(rv)
            self.assertIn("feed", data)
            return data

        data = good_feed()
        self.assertGreater(len(data["feed"]), 0)
        self.logout()
        data = good_feed("?number=5")
        self.assertEqual(len(data["feed"]), 5)

    def test_send_answer_get_next_question(self):
        uuid = UUID.TAG_TEACHERS
        index = 1

        with self.app:
            self.logout()
            url = "api/send_answer?index=%s&uuid=%s" % (
                index, uuid) + "&get_next_question=1"
            # anonymous answer
            rv = self.app.get(url)
            data = self.assertSuccessResponse(rv)
            self.assertIn("next_question", data)
            self.assertTrue(current_user.is_anonymous)
            result = db.get_answer_index_from_username_uuid(
                current_user.username, uuid)
            msg = "username = '%s' uuid = '%s'" % (current_user.username, uuid)
            self.assertEqual(result, index, msg=msg)

            # registered answer
            self.register(
                "new username 5555",
                MOCK_PASSWORD,
                MOCK_PASSWORD,
                "")
            rv = self.app.get(url)
            data = self.assertSuccessResponse(rv)
            self.assertIn("next_question", data)
            self.assertFalse(current_user.is_anonymous)
            self.assertEqual(
                db.get_answer_index_from_username_uuid(
                    current_user.username,
                    uuid),
                index)

    def test_send_bad_answers(self):
        def check_bad_answer(url):
            rv = self.app.get(url)
            self.assertNotEqual(rv.status_code, 200)
            data = json.loads(rv.data.decode("utf-8"))
            self.assertEqual(type(data), dict)

        uuid = UUID.TAG_TEACHERS
        url = "api/send_answer?index=%s&uuid=%s" % (
            "bobjoe", uuid) + "&get_next_question=1"
        check_bad_answer(url)

        url = "api/send_answer?index=1&uuid=not-a-uuid&get_next_question=1"
        check_bad_answer(url)

        url = "api/send_answer?index=1&get_next_question=1"
        check_bad_answer(url)

    def test_transfer_answers_from_anonymous(self):
        "answering questions anonymously then registering should have the same feed."
        with self.app:
            "get feed for anonymous user with no answers"
            anonymous_uuids_before = self.get_feed_uuids()

            "feed is not empty"
            self.assertTrue(anonymous_uuids_before)

            "submit an answer"
            uuid = db.get_next_question_uuid("not a username! 333")
            self.app.get('/api/send_answer?uuid=%s&index=1' % uuid)

            "get feed for anonymous user after first answer"
            anonymous_uuids_after = self.get_feed_uuids()

            username = "username test_transfer_answers_from_anonymous"
            self.register(username, MOCK_PASSWORD, MOCK_PASSWORD, "")
            registered_uuids = self.get_feed_uuids()
            self.assertTrue(registered_uuids)

            "registered answers different from anonymous-no-answers"
            self.assertNotEqual(registered_uuids, anonymous_uuids_before)

            "registering does not change feed"
            assertEqualCompare(registered_uuids, anonymous_uuids_after)

    def test_feed_offset(self):
        "test that feed offset properly truncates top score feed results"
        def test_feed(username):
            if username:
                self.register(username, MOCK_PASSWORD, MOCK_PASSWORD, "")
            else:
                self.logout()
            offset = 5
            results1 = self.get_feed_uuids(number=10)
            results2 = self.get_feed_uuids(number=10, offset=offset)

            self.assertEqual(len(results1), len(results2))
            self.assertGreater(len(results2),0)
            self.assertEqual(results1[offset], results2[0])

        with self.app:
            "anonymous feed offset"
            test_feed("")

            "logged in feed offset"
            test_feed("test_feed_offset_username")

        number = 10
        "when offset is higher than the number of FLOs, return nothing"
        offset = db.get_recommendable_count()
        results = self.get_feed_uuids(number=number, offset=offset)
        self.assertFalse(results)

        "offset is almost higher than number of FLOs, return fewer than 'number' results"
        results = self.get_feed_uuids(number=number, offset=offset - 5)
        self.assertEqual(
            len(results), 5, msg=("\noffset = %s\nnumber = %s\nUUIDs = %s" %
                                  (offset, number, str(results))))
