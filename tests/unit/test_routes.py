
import os
import json

from tests.flocademy_test import FLOcademyTest
from constants import *
from app import app


class TestAccounts(FLOcademyTest):

    def test_500_page(self):
        has_zero_error = False
        try:
            self.app.get("/admin/test_server_error")
        except ZeroDivisionError:
            has_zero_error = True
        self.assertTrue(has_zero_error)

    def test_http_code_page(self):
        for code in (400, 500):
            url = "/admin/test_http_error/%s" % code
            rv = self.app.get(url)
            result = rv.status_code
            self.assertEqual(result, code, msg="url = '%s'" % url)

    def test_robots_txt(self):
        rv = self.app.get("/robots.txt")
        result = rv.status_code
        self.assertEqual(result, 200)
