import json
import copy

from app import db

from tests.flocademy_test import FLOcademyTest
from utilities import get_matching_paths_recursively
from utilities import *


class TestRecommendWithMock(FLOcademyTest):

    """This class uses mock data to test recommendations.

    note: desires={uuid:(value,weight,desire_type)} """

    @classmethod
    def setUpClass(cls):
        FLOcademyTest.setUpClass()
        db.execute_csv_script(DATA_PATHS.TEST_RECOMMEND)

    def test_vote_outweigh(self):
        """greater: 100% desire 90/100
        lesser: 100% desire 10/10 """
        tag_uuid = "tag103"
        desire_type = db.get_desire_type_from_tag_uuid(tag_uuid)

        desire = (1, DEFAULT.WEIGHT, desire_type)
        desires = {tag_uuid: desire}
        score1 = db.calculate_score_for_uuid_with_desires("flo1", desires)
        score2 = db.calculate_score_for_uuid_with_desires("flo2", desires)
        self.assertLess(score1, score2)

    def test_low_vote_high_desire_outweigh(self):
        """100% desire on many 200/230
        add one more vote on desired 0/0 changes score less than 1%"""
        uuid = "flo6"
        tag_uuid = "tag201"
        desires, _ = self.get_matching_desires(uuid)

        score1 = db.calculate_score_for_uuid_with_desires(uuid, desires)

        "desire of 0/0 tag does not influence score"
        desire = (1, DEFAULT.WEIGHT, DESIRE_TYPES.BONUS_PENALTY)
        desires[tag_uuid] = desire
        score2 = db.calculate_score_for_uuid_with_desires(uuid, desires)
        self.assertEqual(score1, score2)

        "desire of now 1/0 does not significantly influence score"
        db.set_tag_vote(uuid, tag_uuid, TAG_VOTES.UP, 1)
        score3 = db.calculate_score_for_uuid_with_desires(uuid, desires)
        msg = "\nscore2 = '%.4f'\nscore3 = '%.4f'" % (score2, score3)
        threshold = max(score2, score3) * 0.05
        self.assertLess(abs(score2 - score3), threshold, msg=msg)

    def test_tag_power(self):
        "ensure that tag_power is high on popular tags, low on unpopular tags"

        "note: less/greater values here are somewhat arbitrary."
        uuid = "flo6"
        tag_power = db.calculate_tag_power(uuid, "tag101")
        self.assertTrue(0 <= tag_power <= 1)
        self.assertGreater(tag_power, 0.1)

        tag_power = db.calculate_tag_power(uuid, "tag202")
        self.assertTrue(0 <= tag_power <= 1)
        self.assertLess(tag_power, 0.01)

        "sum of tag powers for all tags is 1"
        tag_uuids = db.get_voted_tags_from_uuid(uuid)
        tag_power = sum([db.calculate_tag_power(uuid, tag_uuid)
                        for tag_uuid in tag_uuids])
        "tag_power equals 1, aside from possible float addition errors"
        self.assertLess(abs(1 - tag_power), 0.00001)

    def test_mehs_overpowered_by_vote_count(self):
        """greater: lots of weakly desired 5/10, one greatly desired 95/100
        lesser: lots of weakly desired 6/10, one greatly desired 6/10"""
        desires = {}
        for i in range(1, 6):
            tag_uuid = "tag20%s" % i
            desire_type = db.get_desire_type_from_tag_uuid(tag_uuid)
            value = 0.5 if i < 5 else 1
            desires[tag_uuid] = (value, 0.4, desire_type)

        score1 = db.calculate_score_for_uuid_with_desires("flo3", desires)
        score2 = db.calculate_score_for_uuid_with_desires("flo4", desires)
        self.assertGreater(score1, score2)

    def test_matching_desires(self):
        "Perfect desires for a FLO should have the highest of all FLO scores."

        "desires={uuid:(value,weight,desire_type)}"
        best_uuid = "flo5"
        desires, _ = self.get_matching_desires(best_uuid)
        uuid_scores = []
        for uuid in db.get_uuids():
            score = db.calculate_score_for_uuid_with_desires(uuid, desires)
            item = (score, uuid)
            uuid_scores.append(item)
        uuids = [i[1] for i in reversed(sorted(uuid_scores))]
        self.assertIn(uuid, uuids)
        self.assertIn(best_uuid, uuids)
        self.assertEqual(uuids.index(best_uuid), 0)

    def test_get_total_votes_on_uuid(self):
        expected = 1152
        uuid = "flo6"
        result = db.get_total_votes_on_uuid(uuid)
        self.assertEqual(result, expected)

    def test_next_question(self):
        username = "test_next_question username"
        expire_time = 1000

        "first question has absolute most votes"
        result = db.get_next_question_uuid(username)
        expected = "tag301"
        self.assertEqual(result, expected)

        answer = (expected, 0, DEFAULT.WEIGHT)
        db.set_answer(username, answer, expire_time, recalculate_scores=True)
        "deciding the second question ignores the votes for the previously answered question"
        result = db.get_next_question_uuid(username)
        expected = "tag303"
        self.assertEqual(result, expected)

    def test_get_question_plank_from_username_uuid(self):
        "uuid = None should return a False question plank"
        for uuid in (None, False, {}, 0):
            plank = db.get_question_plank_from_username_uuid(
                "get_question_plank_from_username_uuid username",
                uuid)
            self.assertFalse(plank)
