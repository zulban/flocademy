import json
from testfixtures import compare

from app import db

from tests.flocademy_test import FLOcademyTest
from utilities import get_matching_paths_recursively
from utilities import *


class TestStableData(FLOcademyTest):

    "These are tests that require loading all stable data. Concentrate those tests here to avoid reloading stable data and slowing tests."

    @classmethod
    def setUpClass(cls):
        FLOcademyTest.setUpClass()
        db.load_baseline_data()

    def test_all_render_pages(self):
        "make sure rendering one example of each schema returns 200, not error codes."
        unused_schemas = set(db.get_schema_uuids())
        self.assertGreater(len(unused_schemas), 0)

        for uuid in db.get_uuids():
            flo = db.get_flo_from_uuid(uuid)
            schema_uuid = flo.get("schema_uuid")
            if schema_uuid in unused_schemas:
                unused_schemas.remove(schema_uuid)
                rv = self.app.get("/flo?uuid=" + uuid)
                self.assertSuccessResponse(rv)

        "also test the render of the ultimate FLO meta schema"
        rv = self.app.get("/flo?uuid=" + UUID.FLO_META_SCHEMA)
        self.assertSuccessResponse(rv)
        unused_schemas.remove(UUID.FLO_META_SCHEMA)

        self.assertEqual(len(unused_schemas), 0, msg=str(unused_schemas))
        
    def test_anonymous_has_next_question(self):
        username="test_anonymous_has_next_question-username"
        uuid=db.get_next_question_uuid(username)
        self.assertTrue(uuid)

    def test_non_existing_flos(self):
        "json and render page for non-existing FLOs doesn't crash, gives 404"
        non_existing_uuid = "1c23c390d15a47b7b627278378f282cb"
        rv = self.app.get("/flo?uuid=" + non_existing_uuid)
        self.assertEqual(rv.status_code, 404)

        rv = self.app.get("/api/flo?uuid=" + non_existing_uuid)
        self.assertEqual(rv.status_code, 404)

    def test_almost_matching_desires(self):
        """Matching score is different to almost matching score.
        score(100% desire match) != score(99% desire match)"""
        uuid = UUID.UNIQUE_DESIRES
        factor = 0.95
        matching, matching_score = self.get_matching_desires(uuid)

        for tag in sorted(matching.keys()):
            imperfect = copy.deepcopy(matching)
            imperfect[tag][0] *= factor
            score = db.calculate_score_for_uuid_with_desires(uuid, imperfect)
            self.assertNotEqual(
                score, matching_score, msg="tag = '%s' score = '%s'" %
                (tag, score))

    def test_feed_length_at_least_recommendable_length(self):
        number = min(20, db.count_recommendable_uuids())
        username = "test_feed_length username 8890"

        feed = db.get_feed_planks(username, number)
        "anonymous has decent feed length"
        self.assertEqual(len(feed), number)

        "newly registered has decent feed length"
        self.register(username, MOCK_PASSWORD, MOCK_PASSWORD, "")
        feed = db.get_feed_planks(username, number)
        self.assertEqual(len(feed), number)

        "newly registered answer 1 question has decent feed length"
        self.answer_question_for_tests(UUID.TAG_AUTOMATION, 1)
        feed = db.get_feed_planks(username, number)
        self.assertEqual(len(feed), number)

    def test_apathy_answer(self):
        "Answering 'I don't care' to tags of all desire_types does not change recommendations."

        username = "test_apathy_answer username1235"
        desire_type_uuids = (
            UUID.TAG_YES_BONUS,
            UUID.TAG_BONUS_PENALTY,
            UUID.TAG_FULL,
            UUID.TAG_JUST_PENALTY)

        def get_feed_uuids(username):
            number = 20
            feed = db.get_feed_planks(username, number)
            return [i.get("uuid") for i in feed]

        self.logout()
        self.register(username, MOCK_PASSWORD, MOCK_PASSWORD, "")
        rv = self.app.get('/home')
        self.assertTrue(self.is_logged_in())

        "answer one baseline question."
        "otherwise, desires being just penalty will give 'best possible score' of 0 regardless of desire."
        self.answer_question_for_tests(UUID.TAG_AUTOMATION, 1)
        
        for i,uuid in enumerate(desire_type_uuids):
            uuids1 = get_feed_uuids(username)
            self.assertTrue(uuids1)
            self.assertTrue(all(uuids1),msg=str(uuids1))
            rv = self.answer_question_for_tests(uuid, 1)
            self.assertSuccessResponse(rv)

            uuids2 = get_feed_uuids(username)
            self.assertTrue(uuids2)
            self.assertTrue(all(uuids2),msg=str(uuids2))
            
            "verify that a change occurs"
            msg="\ndesire[%s]  desire_type_uuid = '%s'" % (i,uuid)
            self.assertNotEqual(uuids1,uuids2,msg=msg)
            
            "verify that we got results"
            self.assertTrue(uuids2)

            "setting vote back to 'i don't care' returns the feed to how it was."
            rv = self.answer_question_for_tests(uuid, NO_DESIRE_INDEX)
            self.assertSuccessResponse(rv)
            uuids3 = get_feed_uuids(username)
            self.assertTrue(uuids3)

            compare(uuids1, uuids3)

    def test_hidden_not_in_feed(self):
        username = "test_username-test_hidden_not_in_feed"
        db.create_user(username,username+"pw",username+"c@c.com")
        requested_feed_length = 100

        "hide number one recommendation from no answer feed"
        uuids = db.get_top_scoring_uuids_for_username(
            username, requested_feed_length)
        self.assertTrue(uuids)
        to_hide_uuid = uuids[0]
        db.add_uuid_to_user_list(to_hide_uuid, username, USER_LISTS.HIDDEN)
        uuids = db.get_top_scoring_uuids_for_username(
            username, requested_feed_length)
        
        "uuid not in feed"
        self.assertNotIn(to_hide_uuid, uuids)
        self.assertGreater(len(uuids), 0)

        "send one answer"
        expire_time = 1000
        uuid = UUID.TAG_AUTOMATION
        answer = (uuid, 0, DEFAULT.WEIGHT)
        db.set_answer(username, answer, expire_time)

        "hide number one recommendation from one answer feed"
        uuids = db.get_top_scoring_uuids_for_username(
            username, requested_feed_length)
        to_hide_uuid = uuids[0]
        db.add_uuid_to_user_list(to_hide_uuid, username, USER_LISTS.HIDDEN)
        uuids = db.get_top_scoring_uuids_for_username(
            username, requested_feed_length)
        "uuid not in feed"
        self.assertNotIn(to_hide_uuid, uuids)
        self.assertGreater(len(uuids), 0)

    def test_recalc_same_as_adjust(self):
        "Verifies that the complete recalculation returns the same results as incremental adjusts."
        username = "username-test_recalc_same_as_adjust"
        requested_feed_length = 100
        expire_time = 1000
        uuids = (
            UUID.TAG_AUTOMATION,
            UUID.TAG_BONUS_PENALTY,
            UUID.TAG_FULL,
            UUID.TAG_JUST_PENALTY,
            UUID.TAG_YES_BONUS)

        "get top scoring from incremental scoring"
        for uuid in uuids:
            answer = (uuid, 1, DEFAULT.WEIGHT)
            db.set_answer(username, answer, expire_time)
        recommended = db.get_top_scoring_uuids_for_username(
            username, requested_feed_length)

        "get top scoring from total recalculation"
        db.recalculate_scores(username)
        recommended2 = db.get_top_scoring_uuids_for_username(
            username, requested_feed_length)

        self.assertGreater(len(recommended), 0)
        self.assertEqual(recommended, recommended2)

    def test_all_questions_get_asked(self):
        username = "username-test_all_questions_get_asked"
        expire_time = 1000

        questions = db.get_tag_uuids()
        self.assertGreater(len(questions), 0)

        for i in range(len(questions)):
            uuid = db.get_next_question_uuid(username)
            self.assertTrue(uuid)
            answer = (uuid, 1, DEFAULT.WEIGHT)
            db.set_answer(
                username,
                answer,
                expire_time,
                adjust_scores=False,
                recalculate_scores=False)

        uuid = db.get_next_question_uuid(username)
        self.assertFalse(uuid)

    def test_weight_changes_score(self):
        """Changing weight on matching desire must change score."""

        uuid = UUID.UNIQUE_DESIRES
        matching, matching_score = self.get_matching_desires(uuid)

        "Adjust weight slightly for each tag in a separate trial, compare scores."
        for factor in (0.5, 2):
            for tag in matching:
                reweighted = copy.deepcopy(matching)
                reweighted[tag][1] *= factor
                score = db.calculate_score_for_uuid_with_desires(
                    uuid, reweighted)
                self.assertNotEqual(
                    score, matching_score, msg="tag = '%s' factor = '%s' score = '%s'" %
                    (tag, factor, score))

    def test_recommended_can_be_recommended(self):
        "The entire feed is only made up of UUIDs that can be recommended."

        username = ""
        number = db.get_recommendable_count() * 10
        db.create_user(username,username+"pw",username+"@c.com")
        uuids = db.get_top_scoring_uuids_for_username(username, number)
        self.assertTrue(uuids)

        "all recommended UUIDs can be recommended."
        for i,uuid in enumerate(uuids):
            flo=db.get_flo_from_uuid(uuid)
            msg="flo[%s] for uuid = '%s' = \n"%(i,uuid)+json.dumps(flo,indent=4,sort_keys=1)
            self.assertTrue(db.can_recommend_this_flo(flo),msg=msg)

        "all recommendable UUIDs are present."
        self.assertEqual(set(uuids), set(db.get_recommendable_uuids()))

    def test_recommend_schema_shuffle(self):
        """schema_shuffle guarantees that the first n items in a feed have different schemas, given a feed with n different schemas."""
        username = ""
        requested_feed_length = 100
        feed = db.get_feed_planks(
            username,
            requested_feed_length,
            schema_shuffle=True)
        schemas = [i.get("schema_uuid") for i in feed]
        schema_set = set(schemas)

        "first guarantee that the test is testing something"
        self.assertTrue(all(schemas))
        self.assertGreater(len(schema_set), 1)

        first_schemas = set(schemas[:len(schema_set)])
        "first check length for easier test fail messages"
        self.assertEqual(len(schema_set), len(first_schemas))
        "the actual complete test"
        self.assertEqual(schema_set, first_schemas)

    def test_structure_of_schemas(self):
        fv = FLOValidator(DATA_PATHS.SCHEMAS, verbose=0)
        for path in get_matching_paths_recursively(DATA_PATHS.SCHEMAS,".json"):
            j=get_json(path)
            msg=fv.get_validation_error_message(j)
            self.assertTrue(fv.is_valid_flo(j),msg=msg)

    def test_recommended_search_values(self):
        "recommended searches shouldn't have newlines in them... this is my problem, not worth putting in schema"
        flos = db.get_flos()
        self.assertGreater(len(flos), 0)
        key = "recommended_searches"
        for flo in flos:
            if key in flo:
                for query in flo[key]:
                    msg = "\ntitle = '%s'\nuuid = '%s'\nquery = '%s'" % (
                        flo.get("title"), flo.get("_id"), query)
                    self.assertNotIn("\n", query, msg=msg)

    def test_get_mirrors_for_flo(self):
        mirrors = db.get_mirrors_for_flo(None)
        self.assertFalse(mirrors)
        self.assertEqual(len(mirrors), 0)

        flo = db.get_flo_from_uuid(UUID.LESSON)
        mirrors = db.get_mirrors_for_flo(flo)
        self.assertTrue(mirrors)
        self.assertGreater(len(mirrors), 0)

    def test_can_recommend_this_uuid(self):
        result = db.can_recommend_this_uuid(UUID.TAG_AUTOMATION)
        self.assertFalse(result)

        result = db.can_recommend_this_uuid(UUID.MIRROR_SCHEMA)
        self.assertFalse(result)

        result = db.can_recommend_this_uuid(UUID.REFERENCE_WITH_PARENT_FLO)
        self.assertFalse(result)

        result = db.can_recommend_this_uuid(UUID.FLO_META_SCHEMA)
        self.assertFalse(result)
        
        result = db.can_recommend_this_uuid(UUID.NOT_A_CHILD)
        self.assertTrue(result)
        
        result = db.can_recommend_this_uuid(UUID.RECOMMENDABLE.ASSIGNMENT_WRITTEN_INSTRUCTIONS)
        self.assertTrue(result)
        
        result = db.can_recommend_this_uuid(UUID.RECOMMENDABLE.REFERENCE)
        self.assertTrue(result)

    def test_get_value_from_tag_and_index(self):
        uuid = UUID.TAG_AUTOMATION
        index = 1
        result = db.get_value_from_tag_and_index(uuid, index)
        self.assertEqual(result, 0.5)

    def test_duplicate_uuids(self):
        paths = get_matching_paths_recursively("data", ".json")
        self.assertGreater(len(paths), 0)
        path_uuids = {}

        uuids = set()
        for path in paths:
            with open(path, "r") as f:
                data = json.loads(f.read())
            self.assertIn("_id", data)
            uuid = data["_id"]
            msg = "\npath1 = '%s'\npath2 = '%s'" % (path, path_uuids.get(uuid))
            self.assertNotIn(uuid, uuids, msg=msg)
            path_uuids[uuid] = path
            uuids.add(uuid)

    def test_render_mirrors_embeds(self):
        "Verify one FLO for every schema type, that their mirror URL is somewhere on their render page."
        verified_schemas = set()
        for uuid in db.get_uuids(include_tags=False):
            flo = db.get_flo_from_uuid(uuid)
            schema_uuid = flo.get("schema_uuid")
            if schema_uuid in verified_schemas:
                continue

            children = db.get_mirrors_for_flo(flo)
            children_uuids = [child.get("mirror_for") for child in children]
            msg="\nchildren_uuids = "+str(children_uuids)+"\nchildren = "+str(children)
            self.assertTrue(all(children_uuids),msg=msg)
            if children_uuids:
                verified_schemas.add(schema_uuid)
                self.verify_response_shows_children(uuid, children_uuids)
                continue

    def verify_response_shows_children(self, uuid, children_uuids):
        rv = self.app.get('/flo?uuid=' + uuid)
        for child_uuid in children_uuids:
            "response contains the child UUID"
            msg="\n\nuuid = '%s' children_uuids = "%uuid+str(children_uuids)
            self.assertInResponse(child_uuid, rv, msg=msg)
            data = str(rv.data)

            "response contains URL to the mirror, or the page contains a youtube.com/embed"
            urls = db.get_urls_for_uuid(child_uuid)
            contains_url = any([url in data for url in urls])
            contains_youtube = "youtube.com/embed" in data
            self.assertTrue(
                contains_url or contains_youtube, msg="uuid '%s' does not contain URL of child '%s'" %
                (uuid, child_uuid))
