
from tests.flocademy_test import FLOcademyTest
from constants import *
from app import app, db


class TestGroups(FLOcademyTest):

    def setUp(self):
        FLOcademyTest.setUp(self)
        db.drop_all()

    def test_create_group(self):
        username = "test_create_group username"
        group_name = "test_create_group groupname"
        color = "#123123"
        group_id = db.create_group(username, group_name, color)
        self.assertTrue(group_id)

        result = db.get_group_name(group_id)
        self.assertEqual(group_name, result)
        expected = "test_create_group new name"
        db.set_group_name(group_id, expected)
        result = db.get_group_name(group_id)
        self.assertEqual(expected, result)

        result = db.get_group_color(group_id)
        self.assertEqual(color, result)
        expected = "#345345"
        db.set_group_color(group_id, expected)
        result = db.get_group_color(group_id)
        self.assertEqual(expected, result)

        "deleted group no longer exists"
        db.delete_group(group_id)
        result = db.get_group_name(group_id)
        self.assertFalse(result)
        
        "delete non-existent group doesn't crash"
        db.delete_group(group_id)

    def test_join_group(self):
        usernames = ["test_join_group-username%s" % i for i in range(10)]
        for username in usernames:
            db.create_user(username,username+"password",username+"@gmail.com")
            
        group_name = "test_join_group groupname"
        color = "#123123"
        group_id = db.create_group(usernames[0], group_name, color)

        "username starts with no groups"
        result = db.get_group_ids_from_username(usernames[4])
        self.assertFalse(result)

        "group starts with creator as sole member"
        result = db.get_usernames_from_group_id(group_id)
        self.assertEqual([usernames[0]], result)

        "group usernames includes new joined usernames"
        for username in usernames[1:]:
            db.join_group(username, group_id, "")
        result = db.get_usernames_from_group_id(group_id)
        self.assertEqual(usernames, result)

        "username4 is in the one group"
        result = db.get_group_ids_from_username(usernames[4])
        self.assertEqual(result, [group_id])

        "leave group no longer in group"
        username = usernames[5]
        self.assertTrue(db.is_username_in_group_id(username, group_id))
        db.leave_group(username, group_id)
        self.assertFalse(db.is_username_in_group_id(username, group_id))
        result = db.get_group_ids_from_username(username)
        self.assertNotIn(group_id, result)

    def test_group_permissions(self):
        creator_username="test_group_permissions-creator-username"
        username = "test_group_permissions-username"
        db.create_user(creator_username,creator_username+"password",creator_username+"@gmail.com")
        group_id = db.create_group(creator_username, "group name", "AAFFCC")

        "random user starts with no permissions"
        result = db.get_group_rights(username, group_id)
        self.assertEqual(result, "")
        db.join_group(username, group_id, "")
        self.assertEqual(result, "")

        "set and get"
        rights="rw"
        db.set_group_rights(username, group_id, rights)
        result = db.get_group_rights(username, group_id)
        self.assertEqual(result, rights)
        
        "setting some other non-existent group doesn't change rights"
        db.set_group_rights(username, group_id+"not this group id", "")
        result = db.get_group_rights(username, group_id)
        self.assertEqual(result, rights)

    def test_join_codes(self):
        "no group id for random join code"
        self.assertFalse(db.get_group_id_from_join_code("not a join code 456"))
        
        username="test_join_codes-username"
        db.create_user(username,username+"password",username+"@gmail.com")
        gid = db.create_group(username, "group name", "AAFFCC")
        
        code1 = db.get_group_join_code(gid)
        self.assertEqual(len(code1), JOIN_CODE_LENGTH)
        self.assertFalse(set(code1).difference(set(JOIN_CODE_CHARS)))

        "reset doesn't affect other group ids"
        db.reset_group_join_code(gid + " gid#2")
        code2 = db.get_group_join_code(gid)
        self.assertEqual(code1, code2)

        "reset makes new join id"
        db.reset_group_join_code(gid)
        code3 = db.get_group_join_code(gid)
        self.assertNotEqual(code1, code3)

        "after all this tampering, join code can retrieve correct group id"
        result = db.get_group_id_from_join_code(code3)
        self.assertEqual(gid, result)

    def test_create_group_join_code(self):
        "create a group"
        username1 = "test_join_codes user1"
        username2 = "test_join_codes user2"
        
        for username in (username1,username2):
            db.create_user(username,username+"password",username+"@gmail.com")
        
        group_id = db.create_group(username1, "group name", "AAFFCC")
        self.assertTrue(db.is_username_in_group_id(username1, group_id))
        self.assertFalse(db.is_username_in_group_id(username2, group_id))

        "join with join code"
        join_code = db.get_group_join_code(group_id)
        db.join_group_with_join_code(username2, join_code, "")
        self.assertTrue(db.is_username_in_group_id(username2, group_id))
        usernames = db.get_usernames_from_group_id(group_id)
        self.assertIn(username1, usernames)
        self.assertIn(username2, usernames)

    def test_group_lock_status(self):
        "default is unlocked"
        username="test_group_lock_status-username"
        db.create_user(username,username+"pw",username+"@g.com")
        gid = db.create_group("test_group_lock_status username", "group name", "AAFFCC")
        
        result = db.get_group_lock_status(gid)
        self.assertFalse(result)

        "set true, now true"
        db.set_group_lock_status(gid, True)
        result = db.get_group_lock_status(gid)
        self.assertTrue(result)

        "set false, now false"
        db.set_group_lock_status(gid, False)
        result = db.get_group_lock_status(gid)
        self.assertFalse(result)
