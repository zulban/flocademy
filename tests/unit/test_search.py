
import json

from app import db

from tests.flocademy_test import FLOcademyTest
from constants import *


class TestSearch(FLOcademyTest):

    @classmethod
    def setUpClass(cls):
        FLOcademyTest.setUpClass()
        db.load_baseline_flos()
        db.load_baseline_mirrors()

    def good_search_results(self, results):
        for result in results:
            self.assertTrue(result)
            self.assertIn("type", result)

            t = result["type"]
            self.assertIn(t, ["plank_data"])
            if t == "plank_data":
                self.assertIn("plank_data", result)

    def test_uuid_search(self):
        username = "test_username97563434"
        for uuid in (UUID.LESSON, UUID.TAG_SCHEMA):
            results = db.search(username, uuid)
            self.good_search_results(results)
            self.assertGreater(len(results), 0)
            self.assertIn(uuid, json.dumps(results))

    def test_string_search(self):
        username = "test_username56314"
        results = db.search(username, "for loop")
        self.good_search_results(results)
        self.assertGreater(len(results), 0)

        results = db.search(username, "garbage-with-no-results-90234523434634")
        self.good_search_results(results)
        self.assertEqual(len(results), 0)

    def test_url_search(self):
        username = "test_username056314"
        urls = [
            "http://qz.com/691614/american-schools-are-teaching-our-kids-how-to-code-all-wrong/",
                "http://qz.com/691614/american-schools-are-teaching-our-kids-how-to-code-all-wrong",
                "https://qz.com/691614/american-schools-are-teaching-our-kids-how-to-code-all-wrong/",
                "qz.com/691614/american-schools-are-teaching-our-kids-how-to-code-all-wrong"]
        uuid = "05acc780fd404faa91bd96db90518d0b"

        for url in urls:
            results = db.search(username, url)
            msg="\nurl = '%s'"%url
            self.good_search_results(results)
            self.assertGreater(len(results), 0, msg=msg)
            self.assertIn(uuid, str(results),msg=msg)
