import unittest
import os
import os.path
from copy import deepcopy

from app import db
from tests.flocademy_test import FLOcademyTest
from utilities import *
from constants import TestingSettings


class TestUtilities(FLOcademyTest):

    def test_unittest_config(self):
        "have these unit tests been configured? test is necessary because flask configuration is still a hazy mystery."
        self.assertEqual(db.config.REDIS_DB, TestingSettings.REDIS_DB)
        self.assertEqual(db.config, TestingSettings)

    def test_is_string_a_url(self):
        valid1 = "http://qz.com/691614/american-schools-are-teaching-our-kids-how-to-code-all-wrong/"
        valid2 = "qz.com/691614/american-schools-are-teaching-our-kids-how-to-code-all-wrong/"
        valid3 = "https://www.reddit.com"

        invalid1 = "http://almostaurl."
        invalid2 = "https:/almostaurl.com"

        self.assertTrue(is_string_a_url(valid1))
        self.assertTrue(is_string_a_url(valid2))
        self.assertTrue(is_string_a_url(valid3))
        self.assertFalse(is_string_a_url(invalid1))
        self.assertFalse(is_string_a_url(invalid2))

    def test_is_uuid(self):
        uuid = "not a uuid"
        self.assertFalse(is_string_a_uuid(uuid), msg=uuid)
        uuid = "287ba87d8ca448e498b28966223af215"
        self.assertTrue(is_string_a_uuid(uuid), msg=uuid)
        uuid = random_uuid()
        self.assertTrue(is_string_a_uuid(uuid), msg=uuid)

    def test_schema_shuffle(self):
        a = (("item1", "schema2"),
             ("item2", "schema1"),
             ("item3", "schema1"),
             ("item4", "schema3"),
             ("item5", "schema1"),
             ("item6", "schema4"))
        expected = ["item1", "item2", "item4", "item6", "item3", "item5"]
        result = reorder_prioritizing_uniques(a)
        self.assertEqual(len(a), len(result))
        self.assertEqual(expected, result)

    def test_parse_route_line(self):
        route, method = get_route_method_from_line("not a route line('123')")
        self.assertFalse(route)
        self.assertFalse(method)

        line = """@app.route('/api/set_user_list', methods=["POST"])"""
        route, method = get_route_method_from_line(line)
        expected = "/api/set_user_list"
        self.assertEqual(route, expected)
        self.assertEqual(method, "post")

    def test_is_flo_a_schema(self):
        uuids = (UUID.TAG_SCHEMA, UUID.LESSON)
        db.load_baseline_flos(uuids=uuids)

        flo = db.get_flo_from_uuid(UUID.TAG_SCHEMA)
        self.assertTrue(is_flo_a_schema(flo),msg=str(flo))
        flo = db.get_flo_from_uuid(UUID.LESSON)
        self.assertFalse(is_flo_a_schema(flo),msg=str(flo))
    
    def test_search_flo_for_children_uuids(self):
        expected=[UUID.FLO_META_SCHEMA,
               UUID.LESSON,
               UUID.MIRROR_SCHEMA]
        
        flo={"uuid thing":expected[0],
             "not uuid":123,
             "also not uuid":"123abc",
             "uuid array stuff":[expected[1],expected[2]]}
        
        children=search_flo_for_children_uuids(flo)
        self.assertEqual(set(children),set(expected))
        
    def test_concatenate_strings_from_data(self):
        data={"a":1,
              "b":[2,"3",[4,5,6,7]],
              "c":{"d":8,"e":[9,10,[11,12]]}
              }
        delimit=""
        result=concatenate_strings_from_data(data,delimit=delimit)
        expected="123456789101112"
        self.assertEqual(result,expected)
        
    def test_strip_password_from_data(self):
        data={"stuff":1,
              "password":1,
              "bob":{"password":2,"verify_password":2,"confirm_password":2,"joe":3},
              "array":[{"password":1,"stuff":2},{"other stuff":1}]}
        strip_password_from_data(data)
        self.assertNotIn("password",data)
        self.assertNotIn("password",data["bob"])
        self.assertNotIn("verify_password",data["bob"])
        self.assertNotIn("confirm_password",data["bob"])
        for item in data["array"]:
            self.assertNotIn("password",item)

    def test_swap_uuid(self):
        invalid = "not uuid"
        valid = "1a2b3c"
        invalid2 = "also not uuid"
        valid2 = "3c4d5e"
        old_new = {invalid: valid, invalid2: valid2}

        data = {1: 1,
                "b": 1,
                "a": [1,
                      2,
                      3,
                      {"_id": invalid}],
                "_id": invalid}
        result = swap_uuids_in_data(deepcopy(data), old_new)
        expected = {1: 1, "b": 1, "a": [
            1, 2, 3, {"_id": valid}], "_id": valid}
        self.assertEqual(result, expected)

        data = {1: 1,
                "b": 1,
                "a": [1,
                      2,
                      3,
                      {"_id": invalid}],
                "_id": invalid2}
        result = swap_uuids_in_data(deepcopy(data), old_new)
        expected = {
            1: 1, "b": 1, "a": [1, 2, 3, {"_id": valid}], "_id": valid2}
        self.assertEqual(result, expected)

    def test_find_uuids(self):
        invalid = "not uuid"
        valid = "574663b5abf94fdcb4a9e12c01582ec4"
        invalid2 = "also not uuid"
        datas = (
            {1: 1,
                "b": 1,
                "a": [1,
                      2,
                      3,
                      {"_id": invalid}],
                "_id": invalid2},
                {1: 1,
                 "b": 1,
                 "a": [1,
                       2,
                       3,
                       {"_id": invalid2}],
                    "_id": invalid},
                {1: 1, "b": 1, "a": [1, 2, 3, {"_id": valid}], "_id": invalid})
        folder, paths, data_dict = write_uuid_test_data(datas)
        old_new = find_uuids(paths)
        self.assertIn(invalid, old_new)
        self.assertIn(invalid2, old_new)
        self.assertIn(valid, old_new)

        self.assertTrue(is_string_a_uuid(old_new[invalid]))
        self.assertTrue(is_string_a_uuid(old_new[invalid2]))
        self.assertEqual(old_new[valid], valid)

    def test_uuid_repair(self):
        invalid = "not uuid"
        valid = "574663b5abf94fdcb4a9e12c01582ec4"
        invalid2 = "also not uuid"
        datas = (
            {1: 1,
                "b": 1,
                "a": [1,
                      2,
                      3,
                      {"_id": invalid}],
                "_id": invalid2},
                {1: 1,
                 "b": 1,
                 "a": [1,
                       2,
                       3,
                       {"_id": invalid2}],
                    "_id": invalid},
                {1: 1, "b": 1, "a": [1, 2, 3, {"_id": valid}], "_id": invalid})
        folder, paths, data_dict = write_uuid_test_data(datas)
        uuid_repair(folder, verbose=False)

        def get_data(path):
            with open(path, "r") as f:
                return json.loads(f.read())

        results = [get_data(path) for path in paths]
        # all uuids are valid
        for i in range(3):
            self.assertTrue(
                is_string_a_uuid(results[i]["a"][3]["_id"]), msg="index=%s" %
                i)
            self.assertTrue(
                is_string_a_uuid(results[i]["_id"]), msg="index=%s" %
                i)

        # uuids match across files
        self.assertEqual(results[0]["a"][3]["_id"], results[1]["_id"])
        self.assertEqual(results[0]["_id"], results[1]["a"][3]["_id"])
        self.assertEqual(results[2]["_id"], results[1]["_id"])

    def test_flo_mover(self):
        fm = FloMover(PATH.DATA)

        flo = {
            "_id": "123",
            "schema_uuid": UUID.MIRROR_SCHEMA,
            "type": "video"}
        result = fm.suggest_flo_folder_from_flo(flo)
        expected = "data/mirrors/video/"
        self.assertEqual(result, expected)
        result = fm.suggest_path_from_flo(flo)
        expected = "data/mirrors/video/123.json"
        self.assertEqual(result, expected)

        flo = {"_id": "123", "_schema": "abc"}
        result = fm.suggest_flo_folder_from_flo(flo)
        expected = "data/schemas/"
        self.assertEqual(result, expected)

        flo = {"_id": "123", "schema_uuid": UUID.VIDEO_LESSON_SCHEMA}
        result = fm.suggest_flo_folder_from_flo(flo)
        expected = "data/video-lesson-plan-for-teachers/"
        self.assertEqual(result, expected)
        result = fm.suggest_path_from_flo(flo)
        expected = expected + "123.json"
        self.assertEqual(result, expected)


def write_uuid_test_data(datas):
    """writes jsons into a test folder
    where datas key=path value=data"""

    indexes = range(len(datas))
    folder = PATH.TESTS_WRITE_FOLDER
    paths = [folder + "data%s.json" % i for i in indexes]
    data_dict = {paths[i]: datas[i] for i in indexes}

    if not os.path.exists(folder):
        os.makedirs(folder)

    for path in data_dict:
        data = data_dict[path]
        with open(path, "w") as f:
            f.write(json.dumps(data))

    return folder, paths, data_dict
