
import os
import json
import logging
import io

from tests.flocademy_test import FLOcademyTest
from constants import *
from app import db
from app.models import User
from testfixtures import LogCapture
from tests.constants import MOCK_PASSWORD


class TestAccounts(FLOcademyTest):

    def setUp(self):
        FLOcademyTest.setUp(self)
        db.drop_all()
        self.logout()

    def test_home(self):
        rv = self.app.get("/")
        self.assertEqual(rv.status_code, 200)
        self.assertInResponse("login", rv)

    def test_mystery_login(self):
        # guessing a password wrong and trying a non-existent username should
        # show the same error
        rv = self.login("not-a-account-555", MOCK_PASSWORD)
        self.assertInResponse(STRINGS.BAD_LOGIN, rv)
        rv = self.app.get('/home')
        self.assertInResponse("login", rv)

        # the user exists
        username = "test bad password login"
        db.create_user(username, MOCK_PASSWORD, "")
        self.assertTrue(User.get_user_from_username(username, db))

        rv = self.login(username, "not zulbans password")
        self.assertInResponse(STRINGS.BAD_LOGIN, rv)
        rv = self.app.get('/home')
        self.assertInResponse("login", rv)

    def test_logging_strips_passwords(self):
        username = "test_logging_strips_passwords-username123"
        password = "test_logging_strips_passwords-password-abc567"

        logger = self.app.application.logger
        original_log_level = logger.getEffectiveLevel()
        logger.setLevel(logging.DEBUG)
        
        def is_string_in_logs(text):
            self.assertGreater(db.mongo.logs.count(),0)
            for doc in db.mongo.logs.find():
                doc["_id"]=str(doc["_id"])
                if text in json.dumps(doc):
                    return True
            return False
        
        try:
            with self.app:
                self.assertFalse(is_string_in_logs(username))
    
                "no password in logs after register"
                self.register(username, password, password, "")
                self.assertTrue(is_string_in_logs(username))
                self.assertFalse(is_string_in_logs(password))
    
                self.logout()
    
                "no password in logs after login"
                self.login(username, password)
                self.assertFalse(is_string_in_logs(password))
        finally:
            logger.setLevel(original_log_level)

    def test_register(self):
        username = "test_register_username12345"
        with self.app:
            result=self.register(username, MOCK_PASSWORD, MOCK_PASSWORD, "")
            code=result.status_code
            
            "register success"
            self.assertEqual(code,200)
            rv = self.app.get('/home')
            self.assertNotInResponse(STRINGS.NAME_TAKEN, rv)
            self.assertTrue(self.is_logged_in())
            self.assertInResponse(username, rv)
            rv = self.logout()
            self.assertFalse(self.is_logged_in())
            
            "accept and reject passwords"
            self.assertTrue(db.check_password(username,MOCK_PASSWORD))
            self.assertFalse(db.check_password(username,MOCK_PASSWORD+"123"))

            "login as the new user"
            self.login(username, MOCK_PASSWORD)
            rv = self.app.get('/home')
            self.assertInResponse(username, rv)
            self.assertTrue(self.is_logged_in())
            self.logout()
            rv = self.app.get('/home')
            self.assertFalse(self.is_logged_in())
            
            "try to register again with same username"
            result=self.register(username, MOCK_PASSWORD+"abc", MOCK_PASSWORD+"abc", "")
            code=result.status_code
            self.assertNotEqual(code,200)

    def test_admin_reset_password(self):
        username = "test_admin_reset_password88"
        password = MOCK_PASSWORD
        with self.app:
            self.register(username, password, password, "")
            rv = self.logout()
            new_password = db.reset_user_password(username)

            "fail login with old password"
            self.login(username, password)
            rv = self.app.get('/home')
            self.assertNotInResponse(username, rv)

            "success login with new password"
            self.login(username, new_password)
            rv = self.app.get('/home')
            self.assertInResponse(username, rv)

    def test_bad_register(self):
        username = "test_bad_register_username567"
        rv = self.register(username, MOCK_PASSWORD, MOCK_PASSWORD, "")
        self.logout()
        # register twice
        rv = self.register(username, MOCK_PASSWORD, MOCK_PASSWORD, "")
        self.assertInResponse(STRINGS.NAME_TAKEN, rv)
        rv = self.app.get('/home')
        self.assertInResponse("login", rv)

        # no password
        rv = self.register(username, "", "", "")
        self.assertInResponse(STRINGS.FIELD_REQUIRED, rv)
        rv = self.app.get('/home')
        self.assertInResponse("login", rv)

        # no match password
        rv = self.register(username, MOCK_PASSWORD, "not " + MOCK_PASSWORD, "")
        self.assertInResponse(STRINGS.PASSWORD_NOMATCH, rv)
        rv = self.app.get('/home')
        self.assertInResponse("login", rv)
