
import os, json
from werkzeug.security import check_password_hash

from app import db

from tests.flocademy_test import FLOcademyTest
from utilities import *
from tests.constants import MOCK_PASSWORD


class TestRedis(FLOcademyTest):

    def setUp(self):
        FLOcademyTest.setUp(self)
        db.drop_all()

    def test_get_stable_uuids(self):
        uuids = (UUID.LESSON, UUID.TAG_SCHEMA)
        db.load_baseline_flos(uuids=uuids)
        uuids = db.get_uuids()
        self.assertGreater(len(uuids), 0)
        self.assertIn(UUID.LESSON, uuids)
        self.assertIn(UUID.TAG_SCHEMA, uuids)

    def test_get_counts(self):
        number = 5
        "flo count"
        for i in range(number):
            flo = {"_id": str(i) * 20, "schema_uuid": "123"}
            db.set_flo(flo)
        self.assertEqual(number, db.get_flo_count())

        "recommendable count unchanged by adding a schema"
        db.set_flo({"_id": "123123", "_schema": "123"})
        msg="\n"+json.dumps(db.get_flos(),indent=4,sort_keys=True)
        self.assertEqual(number, db.get_recommendable_count(),msg=msg)

    def test_get_uuids(self):
        "add test FLOs to db"
        uuids = ["test_get_uuids_%s" %
                 i for i in range(3)] + [random_uuid()]
        flo = {"_id": uuids[0], "schema_uuid": UUID.VIDEO_LESSON_SCHEMA}
        db.set_flo(flo)
        flo = {"_id": uuids[1], "schema_uuid": UUID.TAG_SCHEMA}
        db.set_flo(flo)
        flo = {"_id": uuids[2],
               "schema_uuid": UUID.MIRROR_SCHEMA,
               "mirrors": [uuids[3]]}
        db.set_flo(flo)
        db.set_flo({"_id": uuids[3]})

        "check that all UUIDs now exist"
        all_uuids = db.get_uuids()
        for uuid in uuids:
            self.assertIn(uuid, all_uuids)

        "test all UUIDs minus tags"
        no_tags = db.get_uuids(include_tags=False)
        self.assertIn(uuids[0], no_tags)
        msg = "tags = '%s'" % str(db.get_tag_uuids())
        self.assertNotIn(uuids[1], no_tags, msg=msg)
        self.assertIn(uuids[2], no_tags)
        self.assertIn(uuids[3], no_tags)

    def test_drop_users(self):
        db.load_debug_users()
        db.drop_users()
        usernames = db.get_usernames()
        self.assertEqual(len(usernames), 0)

    def test_set_get_usernames(self):
        db.drop_users()
        username = "test_set_get_user username890"
        db.create_user(username, MOCK_PASSWORD, "")
        usernames = db.get_usernames()
        self.assertEqual(len(usernames), 1)

    def test_get_set_uuid_score(self):
        uuid = "5588abc"
        score = 5
        username = "test_get_set_uuid_score-username"

        result = db.get_uuid_score_for_username(uuid, username)
        self.assertEqual(0, result)

        db.set_uuid_score_for_username(uuid, score, username)
        result = db.get_uuid_score_for_username(uuid, username)
        self.assertEqual(score, result)

    def test_get_top_scoring_uuids(self):
        username = "test_get_top_scoring_uuids-username"

        "Build and set the mock FLOs for this test"
        uuids = []
        for i in range(10):
            uuid = "test-uuid-%s" % i
            uuids.append(uuid)
            score = 15 - i
            schema_uuid = "schema-" + uuid
            db.set_flo({"_id": uuid, "schema_uuid": schema_uuid})
            self.assertTrue(db.can_recommend_this_uuid(uuid))
            db.set_uuid_score_for_username(uuid, score, username)

        number = 3
        result = db.get_top_scoring_uuids_for_username(username, number)
        "returns correct number of UUIDs"
        self.assertEqual(len(result), number)

        "offset=1 returns correct number of results, and second is now first result"
        offset = 1
        offset_result = db.get_top_scoring_uuids_for_username(
            username, number, offset=offset)
        self.assertEqual(len(offset_result), number)
        msg = "\noffset_result = %s\nresult = %s" % (offset_result, result)
        self.assertEqual(offset_result[0], result[1], msg=msg)

        "top UUIDs are highest score UUIDs"
        for i, uuid in enumerate(uuids[:number]):
            self.assertIn(uuid, result)
            self.assertEqual(result.index(uuid), i)

    def test_set_get_flo(self):
        mirrors = ["456", "678"]
        expected = {
            "_id": "12345",
            "title": "test_set_get_flo mock flo",
            "mirrors": mirrors}
        db.set_flo(expected)
        uuid = expected.get("_id")
        result = db.get_flo_from_uuid(uuid)
        self.assertTrue(result)
        result_uuid = result.get("_id")
        self.assertEqual(uuid, result_uuid)

    def test_get_answered_uuids_from_username(self):
        username = "not a user 4567"
        expire_time = 1000
        uuids = db.get_answered_uuids_from_username(username)
        self.assertEqual(len(uuids), 0)
        count = db.get_answer_count_from_username(username)
        self.assertEqual(count, 0)

        uuid = "abc123"
        db.load_debug_users()
        username = "zulban"
        answer = (uuid, 0, DEFAULT.WEIGHT)
        db.set_answer(username, answer, expire_time)
        uuids = db.get_answered_uuids_from_username(username)
        self.assertEqual(len(uuids), 1)
        self.assertIn(uuid, uuids)
        count = db.get_answer_count_from_username(username)
        self.assertEqual(count, 1)

    def test_get_specific_uuids(self):
        self.assertEqual(db.get_number_of_tags(), 0)
        self.assertEqual(len(db.get_tag_uuids()), 0)
        self.assertEqual(len(db.get_schema_uuids()), 0)

        uuids = (UUID.TAG_AUTOMATION, UUID.MIRROR_SCHEMA)
        db.load_baseline_flos(uuids=uuids)
        self.assertGreater(db.get_number_of_tags(), 0)
        self.assertGreater(len(db.get_tag_uuids()), 0)
        self.assertGreater(len(db.get_schema_uuids()), 0)

    def test_get_urls_for_uuid(self):
        db.load_baseline_mirrors()

        # non-existing UUID
        mirrors = db.get_urls_for_uuid("not a uuid")
        self.assertEqual(len(mirrors), 0)

        # valid UUID with no mirrors
        mirrors = db.get_urls_for_uuid(UUID.LESSON)
        self.assertEqual(len(mirrors), 0)

        # valid UUID with mirrors
        mirrors = db.get_urls_for_uuid(
            UUID.REFERENCE_WITH_URLS)
        self.assertGreater(len(mirrors), 0)
        self.assertTrue(all(is_string_a_url(i) for i in mirrors))

    def test_get_pw_hash_from_username(self):
        db.create_user(*TEST_USER)
        username, password, email = TEST_USER
        pw_hash = db.get_pw_hash_from_username(username)
        self.assertTrue(pw_hash)
        self.assertTrue(check_password_hash(pw_hash, password))

    def test_get_email_from_username(self):
        db.create_user(*TEST_USER)
        username, password, email = TEST_USER
        result = db.get_email_from_username(username)
        self.assertEqual(email, result)

    def test_get_answer_index_from_username_uuid(self):
        username = "not a user 4567"
        uuid = UUID.LESSON
        answer = db.get_answer_index_from_username_uuid(username, uuid)
        self.assertEqual(answer, NO_DESIRE_INDEX)

        "set answer for non-existing user should throw an error if no expiry"
        uuid = "abc123"
        expire_time = 0

        def no_expire_error():
            answer = (uuid, 0, DEFAULT.WEIGHT)
            db.set_answer(username, answer, expire_time)
        self.assertRaises(ValueError, no_expire_error)

        db.load_debug_users()
        username = TEST_USER[0]
        expected = 5
        expire_time = 1000
        answer = (uuid, expected, DEFAULT.WEIGHT)
        db.set_answer(username, answer, expire_time)
        answer = db.get_answer_index_from_username_uuid(username, uuid)
        self.assertEqual(expected, answer)

    def test_user_tag_weight(self):
        "empty weights for anonymous unregistered"
        username = "anonymous user 4567"
        weights = db.get_user_weights(username)
        self.assertEqual(len(weights), 0)

        "anonymous unregistered users cannot set weight"
        uuid = UUID.TAG_AUTOMATION
        new_weight = DEFAULT.WEIGHT / 2
        expire_time = 1000
        answer = (uuid, 1, DEFAULT.WEIGHT)
        db.set_answer(username, answer, expire_time)
        result = db.get_user_weight_from_username_uuid(username, uuid)
        self.assertEqual(result, DEFAULT.WEIGHT)
        "still empty weights"
        weights = db.get_user_weights(username)
        self.assertEqual(len(weights), 0)

        "existing user set weight"
        db.load_debug_users()
        username = TEST_USER[0]
        uuid = UUID.TAG_AUTOMATION
        answer = (uuid, 1, new_weight)
        db.set_answer(username, answer, expire_time)
        result = db.get_user_weight_from_username_uuid(username, uuid)
        self.assertEqual(result, new_weight)

        "now all weights is not empty, has uuid"
        weights = db.get_user_weights(username)
        self.assertEqual(len(weights), 1)
        self.assertIn(uuid, weights)

    def test_set_tag_weight(self):
        uuids = (UUID.TAG_AUTOMATION, UUID.LESSON)
        db.load_baseline_flos(uuids=uuids)
        uuid = UUID.TAG_AUTOMATION
        not_tag_uuid = UUID.LESSON

        # not tag uuid is not in weights
        weights = db.get_tag_weights()
        self.assertNotIn(not_tag_uuid, weights)

        # get all weights returns default even if unset
        self.assertIn(uuid, weights)
        self.assertEqual(weights[uuid], DEFAULT.WEIGHT)

        # check defaults for unset
        result = db.get_tag_weight(uuid)
        self.assertEqual(result, DEFAULT.WEIGHT)

        # check set weight
        new_weight = DEFAULT.WEIGHT + 1
        db.set_tag_weight(uuid, new_weight)
        result = db.get_tag_weight(uuid)
        self.assertEqual(result, new_weight)
        weights = db.get_tag_weights()
        self.assertEqual(new_weight, weights[uuid])

        # not tag uuid still is not in weights
        self.assertNotIn(not_tag_uuid, weights)

    def test_set_get_tag_vote(self):
        uuid, tag_uuid, category, number = "123", "abc", TAG_VOTES.UP, 5
        db.set_tag_vote(uuid, tag_uuid, category, number)
        result = db.get_tag_vote(uuid, tag_uuid, category)
        self.assertEqual(result, number)

    def test_vote_count_after_set_vote(self):
        "the counter for the number of votes cast on a uuid updates after set vote"
        uuid, tag_uuid, number = "123", "abc", 5
        db.set_tag_vote(uuid, tag_uuid, TAG_VOTES.UP, number)
        db.set_tag_vote(uuid, tag_uuid, TAG_VOTES.DOWN, number)
        result = db.calculate_tag_vote_count(uuid)
        self.assertEqual(result, number * 2)

        db.set_tag_vote(uuid, tag_uuid, TAG_VOTES.DOWN, number * 3)
        result = db.calculate_tag_vote_count(uuid)
        self.assertEqual(result, number * 4)

    def test_get_desire_type_from_tag_uuid(self):
        uuid = UUID.TAG_AUTOMATION
        uuids = (uuid,)
        db.load_baseline_flos(uuids=uuids)
        result = db.get_desire_type_from_tag_uuid(uuid)
        self.assertEqual(
            result,
            DESIRE_TYPES.BONUS_PENALTY,
            msg="tag uuid = '%s'" %
            uuid)

    def test_calculate_tag_scope_for_uuid(self):
        # non-existing returns 0
        uuid = "123"
        tag_uuid = "abc"
        result = db.calculate_tag_scope_for_uuid(uuid, tag_uuid)
        self.assertEqual(result, 0)

        # missing down vote returns 1
        db.set_tag_vote(uuid, tag_uuid, TAG_VOTES.UP, 10)
        result = db.calculate_tag_scope_for_uuid(uuid, tag_uuid)
        self.assertEqual(result, 1)

        # up=10 down=10  returns 0.5
        db.set_tag_vote(uuid, tag_uuid, TAG_VOTES.DOWN, 10)
        result = db.calculate_tag_scope_for_uuid(uuid, tag_uuid)
        self.assertEqual(result, 0.5)

    def test_get_tag_scopes_for_uuid(self):
        # non-existing returns empty
        uuid = "123"
        tag_uuid = "abc"
        result = db.get_tag_scopes_for_uuid(uuid)
        self.assertFalse(result)

        # one up/down combo returns dictionary of length 1, with tag_uuid as a
        # key
        db.set_tag_vote(uuid, tag_uuid, TAG_VOTES.UP, 10)
        db.set_tag_vote(uuid, tag_uuid, TAG_VOTES.DOWN, 10)
        result = db.get_tag_scopes_for_uuid(uuid)
        self.assertIn(tag_uuid, result)
        self.assertEqual(len(result), 1)

    def test_user_list(self):
        uuid = UUID.LESSON
        username = "test_username90330"
        for list_name in USER_LIST_NAMES:
            "originally not on the list"
            self.assertEqual(db.get_user_list_count(username, list_name), 0)
            self.assertNotIn(uuid, db.get_user_list(username, list_name),
                             msg="list_name = '%s'" % list_name)
            self.assertFalse(
                db.is_uuid_on_user_list(
                    uuid,
                    username,
                    list_name))

            "add it to the list"
            db.add_uuid_to_user_list(uuid, username, list_name)
            self.assertIn(uuid, db.get_user_list(username, list_name),
                          msg="list_name = '%s'" % list_name)
            self.assertEqual(db.get_user_list_count(username, list_name), 1)
            self.assertTrue(db.is_uuid_on_user_list(uuid, username, list_name))

            "remove it from the list"
            db.remove_uuid_from_user_list(uuid, username, list_name)
            self.assertNotIn(uuid, db.get_user_list(username, list_name),
                             msg="list_name = '%s'" % list_name)
            self.assertEqual(db.get_user_list_count(username, list_name), 0)
            self.assertFalse(
                db.is_uuid_on_user_list(
                    uuid,
                    username,
                    list_name))

    def test_get_active_plank_buttons(self):
        uuid = UUID.LESSON
        username = "test_username24572"
        active = db.get_active_plank_buttons(username, uuid)

        "starts empty"
        self.assertEqual(len(active), 0)

        "all on"
        for list_name in USER_LIST_NAMES:
            db.add_uuid_to_user_list(uuid, username, list_name)
        active = db.get_active_plank_buttons(username, uuid)
        self.assertEqual(set(active), set(USER_LIST_NAMES))

        "used to be all on, now all off"
        for list_name in USER_LIST_NAMES:
            db.remove_uuid_from_user_list(uuid, username, list_name)
        active = db.get_active_plank_buttons(username, uuid)
        self.assertEqual(len(active), 0)

    def test_admin_level(self):
        username = "test_admin_level username abc"
        db.create_user(username,username+"pw",username+"@c.com")
        "unset defaults to 0"
        level = db.get_admin_level(username)
        self.assertEqual(level, 0)

        "set to 1"
        db.set_admin_level(username, 1)
        level = db.get_admin_level(username)
        self.assertEqual(level, 1)

        "set to back to 0"
        db.set_admin_level(username, 0)
        level = db.get_admin_level(username)
        self.assertEqual(level, 0)

    def test_prompt_answer(self):
        username = "test_answer_prompt username"
        prompt_name = "test_answer_prompt prompt_name"
        answer = "test_answer_prompt answer"
        db.create_user(username,username+"pw",username+"@c.com")

        "starts off as False"
        result = db.get_prompt_answer(username, prompt_name)
        self.assertFalse(result)

        db.set_prompt_answer(username, prompt_name, answer)
        result = db.get_prompt_answer(username, prompt_name)
        self.assertEqual(answer, result)
        self.assertTrue(result)
