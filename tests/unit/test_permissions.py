
import os

from tests.flocademy_test import FLOcademyTest
from app import app, db
from utilities import PATH, get_route_method_from_line


class TestPermissions(FLOcademyTest):

    def test_admin_pages(self):
        "get all urls from the admin.py file"
        with open("app/views/admin.py", "r") as f:
            lines = f.readlines()

        urls = []
        for line in lines:
            url, method = get_route_method_from_line(line)
            if url and method:
                urls.append((url, method))

        "make sure we got at least one url, for future proofing."
        self.assertGreater(len(urls), 0)

        "pretend we are not in testing mode"
        app.config["TESTING"] = False
        import logging
        app.logger.setLevel(logging.INFO)

        try:
            for url, method in urls:
                if method == "get":
                    rv = self.app.get(url)
                elif method == "post":
                    rv = self.app.post(url)

                result = rv.status_code
                self.assertEqual(
                    result, 403, msg="url = '%s' method = '%s'" %
                    (url, method))
        finally:
            "return to testing mode"
            app.logger.setLevel(logging.CRITICAL + 10)
            app.config["TESTING"] = True
