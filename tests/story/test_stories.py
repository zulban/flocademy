
from app import app, db
from tests.story.browser_test import BrowserTest
from constants import *

import time


class TestStories(BrowserTest):

    def test_load_resources(self):
        "All resources load on the home page."
        with self.app:
            self.browser.get(self.get_server_url())
            self.assertIn("FLOcademy",self.browser.title)
            self.assertGreater(len(app.db.get_flos()), 0)
            self.assertNoConsoleErrors()

    def test_clicking_flo_sends_to_render_page(self):
        "Clicking a FLO sends to a user friendly page, not to JSON or some other thing."
        with self.app:
            self.browser.get(self.get_server_url())
            time.sleep(2)

            title = self.browser.find_elements_by_css_selector(
                ".unhidden-plank > .plank-center > .plank-title")[0]
            title.click()
            time.sleep(1)

            self.assertIn("flocademy.css\"", self.browser.page_source)

    def test_see_previous_question(self):
        "Clicking a FLO sends to a user friendly page, not to JSON or some other thing."
        with self.app:
            self.browser.get(self.get_server_url())
            time.sleep(1)

            previous_class = ".previous-question-button"
            previous_button = self.browser.find_elements_by_css_selector(
                previous_class)[0]
            self.assertFalse(previous_button.is_displayed())
            answers = self.browser.find_elements_by_css_selector(
                ".question-plank:not(.template) > .question-center > .question-answers > .answer")
            answers[1].click()
            time.sleep(1)
            previous_button = self.browser.find_elements_by_css_selector(
                previous_class)[0]
            self.assertTrue(previous_button.is_displayed())

    def test_answer_all_questions(self):
        "no errors upon answering all questions. also tests appearance of prompts (like welcome prompt)"
        interval = 1
        with self.app:
            self.browser.get(self.get_server_url())
            time.sleep(interval)

            question_count = len(db.get_tag_uuids())
            self.assertGreater(question_count, 0)

            for i in range(question_count):
                if i in (0, EXPLORE_PROMPT_QUESTION_THRESHOLD, STARS_PROMPT_QUESTION_THRESHOLD):
                    self.assertPromptAppearedAndClickDisappears()

                self.assertNoConsoleErrors()

                self.click_answer_in_browser(
                    message="test_answer_all_questions i = %s" %
                    i)
                time.sleep(interval)

                self.assertNoConsoleErrors()

            "finally, verify that the 'all questions answered' prompt appears. don't click though because it goes to another page."
            self.assertPromptAppeared()
