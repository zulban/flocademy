
import time

from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

from tests.flocademy_test import FLOcademyTest
from app import create_app

from flask_testing import LiveServerTestCase


class BrowserTest(FLOcademyTest, LiveServerTestCase):

    def create_app(self):
        app = create_app()
        app.db.load_baseline_data()
        return app

    def setUp(self):
        FLOcademyTest.setUp(self)
        self.browser = BrowserTest.browser

        if self.app.application.config["HEADLESS_BROWSER_TESTS"]:
            BrowserTest.display = Display(visible=0, size=(800, 600))
            BrowserTest.display.start()

    @classmethod
    def setUpClass(cls):
        FLOcademyTest.setUpClass()

        desired = DesiredCapabilities.CHROME
        desired['loggingPrefs'] = {'browser': 'ALL'}
        BrowserTest.browser = webdriver.Chrome(
            executable_path="_chromedriver/chromedriver",
            desired_capabilities=desired)

    @classmethod
    def tearDownClass(cls):
        FLOcademyTest.tearDownClass()
        BrowserTest.browser.quit()

    def tearDown(self):
        if self.app.application.config["HEADLESS_BROWSER_TESTS"]:
            BrowserTest.display.stop()

    def assertNoConsoleErrors(self):
        "This assert fails upon any console errors, like failing to load resources."
        for entry in self.browser.get_log('browser'):
            if "level" in entry:
                self.assertNotEqual(entry["level"], "SEVERE", msg=str(entry))

    def assertPromptAppeared(self, message=""):
        "checks that a prompt (like the welcome prompt) is visible."
        button = self.browser.find_elements_by_css_selector(
            ".prompt-button")[0]
        self.assertTrue(button.is_displayed(), msg=message)
        return button

    def assertClickedPromptDisappears(self, button, message=""):
        "checks that a clicked prompt disappears."
        button.click()
        time.sleep(1)
        self.assertFalse(button.is_displayed(), msg=message)

    def assertPromptAppearedAndClickDisappears(self, message=""):
        "checks that a prompt (like the welcome prompt) is visible, clicks it, then checks that it is no longer visible."
        button = self.assertPromptAppeared(message=message)
        self.assertClickedPromptDisappears(button, message=message)

    def click_answer_in_browser(self, index=0, message=""):
        "finds a question on the current browser page and clicks an answer"
        selector = ".question-plank:not(.template) > .question-center > .question-answers > .answer"
        answers = self.browser.find_elements_by_css_selector(selector)
        answer = answers[index]
        self.assertTrue(answer.is_displayed(), msg=message)
        answers[index].click()
