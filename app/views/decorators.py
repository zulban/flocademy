import time, copy
import json, logging

from functools import wraps
from flask import jsonify, request, render_template, Blueprint, current_app
from flask_login import current_user
from app.views.utilities import json_response

from constants.config import TestingSettings
from constants import VERSION
from app.views import views_blueprint
from utilities import strip_password_from_data

@views_blueprint.context_processor
def inject_global_constants():
    description = "FLOcademy is a website built for educators with a focus on free culture, free learning objects, and personalized education plans."
    return dict(VERSION=VERSION,
                RECAPTCHA_PUBLIC_KEY=current_app.config["RECAPTCHA_PUBLIC_KEY"],
                USE_RECAPTCHA=current_app.config["RECAPTCHA_PRIVATE_KEY"]!=TestingSettings.RECAPTCHA_PRIVATE_KEY,
                description=description)

def user_data_logging(f):
    """Decorate routes with this to record user data events like favoriting items
    or voting for potential later analysis."""
    @wraps(f)
    def decorated_function(*args, **kwargs):
        values = request.values.to_dict()
        strip_password_from_data(values)
        doc = dict(username=current_user.username,
                   time=time.time(),
                   path=request.path,
                   request_values=values,
                   version=VERSION,
                   ip=request.remote_addr,
                   user_agent=request.user_agent.string)
        
        current_app.db.mongo.logs.insert_one(doc)
        
        if current_app.config.get("LOGGING_LEVEL")<=logging.DEBUG:
            doc["_id"]="mongo:"+str(doc["_id"])
            print("Inserting new log event. (printing here as well because logging level is DEBUG). Log event:")
            print(json.dumps(doc,indent=4,sort_keys=True))
            
        return f(*args, **kwargs)

    return decorated_function


def admin_required(f):
    "decorate routes with this to only allow admins access, or everyone in debug/test modes."
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if current_app.config["TESTING"] or current_user.is_admin:
            return f(*args, **kwargs)
        elif request.method == "GET":
            return render_template("403.html"), 403
        else:
            return json_response(403)

    return decorated_function
