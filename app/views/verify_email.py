from flask import redirect, render_template, request
from flask_login import current_user, login_required

from dbms import db
from app.forms import *
from app.views import views_blueprint, rate_limit_manager
from app.views.utilities import json_response

from utilities import send_email
from constants import *


@views_blueprint.route("/account/verify_email", methods=["GET"])
@login_required
def verify_email_page():
    verify_email_form = VerifyEmailForm()
    return render_template("user/verify_email.html",
                           verify_email_form=verify_email_form)


@views_blueprint.route("/account/verify_email", methods=["POST"])
@login_required
@rate_limit_manager.limit(limit=3, per=60 * 5)
def verify_email():
    verify_email_form = VerifyEmailForm(username=current_user.username)

    if verify_email_form.validate_on_submit():
        password = request.values.get("password")
        if current_user.check_password(password):
            email = request.values.get("email")
            db.set_user_email(current_user.username, email)
            token = current_app.url_safe_timed_serializer.dumps(
                email, salt=SECURITY.EMAIL_VERIFICATION_SALT)
            confirm_url = "/account/verify_email_result?token=" + token
            html = render_template("emails/verify_email_confirmation.html",
                                   confirm_url=confirm_url)
            subject = "Confirm your email"
            send_email(email, subject, html)
            return json_response()
        else:
            verify_email_form.custom_error("password", STRINGS.BAD_PASSWORD)
    return jsonify(400, form_errors=verify_email_form.errors)


@views_blueprint.route("/account/verify_email_result", methods=["GET"])
@login_required
@rate_limit_manager.limit(limit=3, per=60 * 5)
def verify_email_result():
    token = request.values.get("token")
    max_age = 86400  # one day in seconds
    template = "user/verify_email_result.html"
    try:
        email = current_app.url_safe_timed_serializer.loads(
            token,
            salt=SECURITY.EMAIL_VERIFICATION_SALT,
            max_age=max_age)
    except:
        return render_template(template, success=False)

    if current_user.email != email:
        return render_template(template, success=False)

    db.set_email_verified_for_username(current_user.username, True)

    return render_template(template, success=True)


@views_blueprint.route("/account/verify_email_sent", methods=["GET"])
@login_required
def verify_email_sent():
    email = request.values.get("email")
    if not email:
        return redirect("home")
    return render_template("/user/verify_email_sent.html", email=email)
