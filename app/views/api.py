import uuid
import json

from flask import request, jsonify, session, Blueprint, current_app
from flask_login import login_required, current_user
from app.views.decorators import admin_required, user_data_logging
from app.views.utilities import json_response

from app.views import views_blueprint, rate_limit_manager
from dbms import db

from utilities import *


@views_blueprint.route('/api/version')
def api_version():
    return json_response(version=VERSION)


@views_blueprint.route('/api/next_question')
def api_next_question():
    next_question = db.get_next_question_plank(current_user.username)
    return json_response(next_question=next_question)


@views_blueprint.route('/api/get_plank', methods=["POST"])
def api_get_plank():
    return _plank_response(request, is_question=False)


@views_blueprint.route('/api/get_question_plank', methods=["POST"])
def api_get_question_plank():
    return _plank_response(request, is_question=True)


def _plank_response(request, is_question=False):
    """Attempts to merge plank and question_plank on front-end are too cumbersome.
    So this generic response function is used instead."""
    uuid = request.form.get("uuid")
    if not uuid:
        return json_response(400, message="bad uuid provided. uuid = '%s'" % uuid)

    if is_question:
        plank = db.get_question_plank_from_username_uuid(
            current_user.username, uuid)
        key = "question_plank"
    else:
        plank = db.get_plank_from_username_uuid(current_user.username, uuid)
        key = "plank"

    if plank:
        return json_response(**{key: plank})
    return json_response(400, message="found no plank with uuid = '%s'" %
                         uuid)


@views_blueprint.route('/api/feed')
def api_feed():
    "get feed planks. number is how many planks, offset is optional and defaults to zero."
    try:
        number = int(request.args.get("number", DEFAULT.FEED_LENGTH))
        offset = int(request.args.get("offset", 0))
    except ValueError:
        return json_response(400)
    feed = db.get_feed_planks(current_user.username, number, offset=offset)
    return json_response(feed=feed)


@views_blueprint.route('/api/get_username')
def api_get_username():
    return json_response(username=current_user.username)


@views_blueprint.route('/api/send_answer')
@user_data_logging
def api_send_answer():
    get_next_question = request.args.get("get_next_question")
    try:
        uuid = request.args["uuid"]
        index = int(request.args["index"])
        weight = float(request.args.get("weight", DEFAULT.WEIGHT))
    except (ValueError, KeyError):
        return json_response(400)

    if not db.is_uuid_a_tag(uuid):
        current_app.logger.error("api_send_answer uuid is not a tag. uuid='%s'" % uuid)
        return json_response(400)
    
    answer = (uuid, index, weight)
    
    "Sending an answer automatically disables the welcome prompt."
    db.set_prompt_answer(current_user.username, PROMPTS.WELCOME, "yes")

    "Sending a weighted answer automatically disables the star weight prompt."
    if weight != DEFAULT.WEIGHT:
        db.set_prompt_answer(current_user.username, PROMPTS.STARS, "yes")

    db.set_answer_with_user(current_user, answer)
    if get_next_question:
        next_question = db.get_next_question_plank(current_user.username)
        return json_response(next_question=next_question)
    return json_response()


@views_blueprint.route('/api/flo')
def api_flo():
    uuid = request.args.get("uuid")
    flo = db.get_flo_from_uuid(uuid)
    if flo:
        return json_response(response=flo)
    return json_response(404)


@views_blueprint.route('/api/admin/submit_flo', methods=["POST"])
@admin_required
def api_submit_flo():
    new_uuid = random_uuid()
    flo = json.loads(request.form["json_string"])

    # don't throw a form error if no write_json_file key
    write_json_file = dict(request.form).get("write_json_file")

    flo["_id"] = new_uuid
    db.save_flo(flo, write_json_file=write_json_file)
    return json_response(new_uuid=new_uuid)


def api_unauthorized():
    return json_response(401)


@views_blueprint.route('/api/set_prompt_answer', methods=["POST"])
def api_set_prompt_answer():
    prompt_name = request.form["prompt_name"]
    answer = request.form["answer"]
    if(prompt_name in PROMPT_LIST):
        db.set_prompt_answer(current_user.username, prompt_name, answer)
        return json_response()
    else:
        return json_response(400, message="Got a weird prompt_name = '%s'" % prompt_name)


@views_blueprint.route('/api/set_user_list', methods=["POST"])
@login_required
@user_data_logging
def api_set_user_list():
    uuid = request.form["uuid"]
    action = request.form["action"]
    list_name = request.form["list_name"]
    if not is_string_a_uuid(uuid) or action not in ("remove", "add") or list_name not in USER_LIST_NAMES:
        return json_response(400, message="Bad arguments = '%s'" % str(
            request.args))

    if action == "remove":
        db.remove_uuid_from_user_list(uuid, current_user.username, list_name)
    elif action == "add":
        db.add_uuid_to_user_list(uuid, current_user.username, list_name)

    return json_response()


@views_blueprint.route('/api/groups/<action>', methods=["GET", "POST"])
@login_required
def api_group(action):
    if current_app.config.get("SIMULATE_LATENCY"):
        time.sleep(FAKE_LOADING_TIME)

    group_id = request.values.get("group_id", "")
    post_username = request.values.get("username", "")
    username = current_user.username
    permissions = db.get_group_rights(current_user.username, group_id)

    if not db.is_username_in_group_id(username, group_id):
        return json_response(400, message="username '%s' not in group." % username)
    if not group_id:
        return json_response(400, message="bad group_id '%s'" % group_id)

    "actions that require no permissions"
    if action == "get_join_code":
        join_code = db.get_group_join_code(group_id)
        return json_response(join_code=join_code)
    elif action == "lock_group":
        db.set_group_lock_status(group_id, True)
        return json_response()
    elif action == "unlock_group":
        db.set_group_lock_status(group_id, False)
        return json_response()
    elif action == "get_lock_status":
        lock_status = db.get_group_lock_status(group_id)
        return json_response(lock_status=lock_status)
    elif action == "leave_group" and not post_username:
        "when post_username is empty, the current user leaves the group"
        db.leave_group(username, group_id)
        return json_response()

    "actions that require write access"
    if "w" not in permissions:
        return json_response(401, message="You don't have write permissions on this group.")

    if action == "reset_join_code":
        db.reset_group_join_code(group_id)
        return json_response()
    elif action == "set_group_rights":
        post_permissions = request.values.get("permissions", "")
        lock_status = db.set_group_rights(
            post_username,
            group_id,
            post_permissions)
        return json_response()
    elif action == "leave_group":
        db.leave_group(post_username, group_id)
        return json_response()
    elif action == "delete_group":
        db.delete_group(group_id)
        return json_response()
    return json_response(404, message="unknown action '%s'" % action)
