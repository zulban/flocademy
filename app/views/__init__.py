
from flask import Blueprint
views_blueprint = Blueprint(
    "views_blueprint",
     __name__,
     static_folder="static")

from app.views.ratelimit import RateLimitManager
rate_limit_manager = RateLimitManager()

from app.views import api, account, error_handlers, legal, public, admin, ratelimit, utilities, verify_email
