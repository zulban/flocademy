import json
import time
import logging

from flask import redirect, render_template, url_for, request, session, jsonify, send_from_directory, current_app
from flask_login import current_user
from dbms import db
from app.models import User
from app.views import views_blueprint, rate_limit_manager
from app.views.decorators import user_data_logging
from app.views.utilities import json_response
from app.forms import LoginForm, RegisterForm

from utilities.markdown import markdownify
from utilities import is_flo_a_schema, DEFAULT, random_uuid, PROMPTS


def login_redirect(username):
    user = User.get_user_from_username(username, db)
    login_user(user)
    return redirect(url_for('recommend_page'))

@views_blueprint.route('/too_many_requests')
def too_many_requests():
    return render_template("429.html")

@views_blueprint.route('/')
@views_blueprint.route('/index')
@views_blueprint.route('/home')
@views_blueprint.route('/recommend')
def recommend_page():
    login_form = LoginForm()
    if login_form.validate_on_submit():
        return login_redirect(login_form.user.username)

    register_form = RegisterForm()
    if register_form.validate_on_submit():
        username = register_form.username.data
        db.create_user(
            username,
            register_form.password.data,
            register_form.email.data)
        user = User.get_user_from_username(username, db)
        return login_redirect(user.username)

    show_welcome_prompt = not db.get_prompt_answer(
        current_user.username, PROMPTS.WELCOME)

    show_version = current_app.config["TESTING"]
    section="recommend"
    return render_template('navigation/%s.html'%section,
                           section=section,
                           show_version=show_version,
                           DEFAULT=DEFAULT,
                           show_welcome_prompt=show_welcome_prompt,
                           login_form=login_form,
                           register_form=register_form,
                           use_social_buttons=True)

@views_blueprint.route('/search', methods=['GET'])
@user_data_logging
@rate_limit_manager.limit(limit=10, per=60)
def search_page():
    query = request.args.get("q")
    results = db.search(current_user.username, query)
    plank_data_jsons = [json.dumps(result["plank_data"]) for result in results]
    section="search"
    return render_template("navigation/%s.html"%section,
                           section=section,
                           plank_data_jsons=plank_data_jsons,
                           query=query,
                           use_social_buttons=False)

@views_blueprint.route('/about')
def about_page():
    section="about"
    return render_template('navigation/%s.html'%section,section=section) 

@views_blueprint.route('/data')
def data_page():
    section="data"
    return render_template('navigation/%s.html'%section,section=section) 

@views_blueprint.route('/contact')
def contact_page():
    section="contact"
    return render_template('navigation/%s.html'%section,section=section) 

@views_blueprint.route('/<filename>')
def static_from_root(filename):
    "This returns files from app/static as though they were top level. For favicon, robots.txt, etc."
    return send_from_directory(current_app.static_folder, filename)


@views_blueprint.route('/.well-known/acme-challenge/<filename>')
def lets_encrypt_static_reroute(filename):
    "This is used by lets encrypt's webroot setup method. The webroot can thus be /home/zulban/flocademy/app/static"
    "This route makes flocademy.ca/static/<lets-encrypt> equal to flocademy.ca/<lets-encrypt>"
    return send_from_directory(current_app.static_folder + "/.well-known/acme-challenge/", filename)


@views_blueprint.route('/flo', methods=['GET'])
@user_data_logging
def flo():
    uuid = request.args.get("uuid")
    flo = db.get_flo_from_uuid(uuid)
    if not flo:
        return render_template('404.html'), 404
    
    title = flo.get("title", uuid)
    description = flo.get("description", title) 
    template = db.get_template_path_from_flo(flo)
    mirrors = db.get_mirrors_for_flo(flo, add_embed_urls=True)
    flo = markdownify(flo)
    return render_template(template,
                           mirrors=mirrors,
                           flo=flo,
                           title=title,
                           description=description,
                           use_social_buttons=True)

