import json
import traceback

from flask import redirect, render_template, request, jsonify, abort
from flask_login import current_user

from dbms import db
from utilities import UUID, is_flo_a_schema
from app.views import views_blueprint, rate_limit_manager
from app.views.decorators import admin_required
from app.views.utilities import json_response
from constants import *
from utilities import is_flo_a_schema


@views_blueprint.route('/admin/test_http_error/<code>')
@admin_required
@rate_limit_manager.limit(limit=10, per=60 * 5)
def admin_test_http_error(code):
    code = int(code)
    abort(code)


@views_blueprint.route('/admin/test_server_error')
@admin_required
@rate_limit_manager.limit(limit=10, per=60 * 5)
def admin_test_server_error():
    "generate a real 500 for testing."
    1 / 0


@views_blueprint.route('/admin')
@admin_required
def admin_home():
    return render_template('admin/home.html')


@views_blueprint.route('/admin/user_lists')
@admin_required
def admin_user_lists():
    usernames = db.get_usernames()
    "where a user_list is a dictionary, key=user_list_name, value=UUIDs for that user list"
    user_lists = {username: {uname: db.get_user_list(username, uname)
                             for uname in USER_LIST_NAMES} for username in usernames}

    display_names = {
        username: db.get_display_name_from_username(
            username) for username in usernames}

    uuids = set()
    for username in user_lists:
        for user_list_name in user_lists[username]:
            uuids = uuids.union(set(user_lists[username][user_list_name]))
    titles = {
        uuid: db.get_flo_from_uuid(
            uuid).get(
            "title",
             "unknown") for uuid in uuids}
    return render_template('admin/user_lists.html', user_lists=user_lists, titles=titles, display_names=display_names)


@views_blueprint.route('/admin/flo_map')
@admin_required
def admin_console():
    flos = db.get_flo_planks(current_user.username)
    return render_template('admin/flo_map.html', flos=flos)


@views_blueprint.route("/admin/zero_vote_flos", methods=["GET"])
@admin_required
def admin_zero_vote_flos():
    flos = []
    for uuid in db.get_uuids(include_tags=False):
        if not db.get_voted_tags_from_uuid(uuid):
            flo = db.get_flo_from_uuid(uuid)
            flos.append((flo.get("title"), flo))
    flos = sorted(flos, key=lambda x: x[0])
    flos = [i[1] for i in flos]
    return render_template("admin/zero_vote_flos.html", flos=flos)


@views_blueprint.route("/admin/tag_voting", methods=["GET"])
@admin_required
def admin_tag_voting():
    flos = sorted(db.get_flos(), key=lambda x: x.get("title"))
    flos = [f for f in flos if not is_flo_a_schema(f)]
    tags = sorted(db.get_tags(), key=lambda x: x.get("title"))
    tag_votes = db.get_all_tag_votes()
    user_weights = db.get_user_weights(current_user.username)
    tag_weights = db.get_tag_weights()
    return render_template(
        "admin/tag_voting.html", flos=flos, tags=tags, tag_votes=tag_votes,
                           user_weights=user_weights, tag_weights=tag_weights)


@views_blueprint.route('/admin/submit_flo', methods=['GET', 'POST'])
@admin_required
def admin_submit_flo():
    uuid = request.args.get("uuid")
    flo = db.get_flo_from_uuid(uuid)
    if is_flo_a_schema(flo):
        schema_uuid = uuid
        title = "Submit new '%s'" % flo.get("title", "FLO")
        schema_string = json.dumps(flo)
        return render_template("admin/submit_flo.html", schema_uuid=schema_uuid, schema_string=schema_string, title=title)
    else:
        messages = [
            "Could not submit a new FLO. The UUID provided is not a schema: '%s'" %
            uuid]
        return render_template("home.html", schema_string=None, messages=messages)


@views_blueprint.route("/admin/set_vote", methods=["POST"])
@admin_required
def admin_set_vote():
    uuid = request.form.get("uuid")
    tag_uuid = request.form.get("tag_uuid")
    value_string = request.form.get("value")
    if not value_string:
        value_string = "0"
    category = request.form.get("category")
    try:
        number = int(value_string)
        db.set_tag_vote(uuid, tag_uuid, category, number)
        return json_response()
    except ValueError:
        return json_response(400)


@views_blueprint.route("/admin/set_tag_weight", methods=["POST"])
@admin_required
def admin_set_tag_weight():
    uuid = request.form.get("uuid")
    value_string = request.form.get("value")
    category = request.form.get("category")
    try:
        weight = int(value_string)
    except ValueError:
        return json_response(400)

    if category == "user_weight":
        db.set_user_tag_weight(current_user.username, uuid, weight)
    elif category == "tag_weight":
        db.set_tag_weight(uuid, weight)
    else:
        return json_response(400)
    return json_response()


@views_blueprint.route("/admin/reset_user_password", methods=["POST"])
@admin_required
def admin_reset_user_password():
    username = request.form.get("username")
    if db.is_username_taken(username):
        new_password = db.reset_user_password(username)
        return json_response(new_password=new_password)
    return json_response(404)
