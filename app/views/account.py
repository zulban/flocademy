import json
import hashlib
import random

from flask import redirect, render_template, url_for, request, jsonify, current_app, send_file
from flask_login import login_user, logout_user, login_required, current_user

from dbms import db
from app.forms import *
from app.views import views_blueprint, rate_limit_manager
from app.views.api import api_unauthorized
from app.views.utilities import json_response
from app.models.anonymous_user import AnonymousUser
from app.models import User
from app.views.decorators import admin_required, user_data_logging

from utilities import get_random_group_color, send_email, verify_recaptcha
from constants import *


@views_blueprint.route('/get_username', methods=['GET'])
def get_username():
    return jsonify(username=current_user.username)


@views_blueprint.route('/register', methods=['POST'])
@user_data_logging
@rate_limit_manager.limit(limit=5, per=60 * 5)
def register():
    form = RegisterForm()

    if not verify_recaptcha(request.form, current_app.config["RECAPTCHA_PRIVATE_KEY"]):
        form_errors = {"recaptcha": ["Bad recaptcha response."]}
        return json_response(400, form_errors=form_errors)

    if form.validate_on_submit():
        username = form.username.data
        if current_user.is_anonymous:
            db.transition_from_anonymous(current_user.username, username)
        db.create_user(
            username,
            form.password.data,
            form.email.data,
            ip=request.remote_addr,
            user_agent=request.user_agent.string)
        user = User.get_user_from_username(username, db)
        login_user(user)
        return json_response()
    else:
        return json_response(400, form_errors=form.errors)


@views_blueprint.route('/set_user_settings', methods=['POST'])
@login_required
@user_data_logging
def set_user_settings():
    "This is how the user page submits changes to account settings."
    "Returns {status:200} or a JSON of errors."
    form = UserSettingsForm()
    if form.validate_on_submit():
        old_email = current_user.email
        set_unverified = old_email != form.email.data
        db.set_user_email(
            current_user.username,
            form.email.data,
            set_unverified=set_unverified)
        db.set_user_display_name(current_user.username, form.display_name.data)
        return json_response()
    else:
        return json_response(401, form_errors=form.errors)


@views_blueprint.route('/join_group', methods=['GET'])
@login_required
@user_data_logging
def join_group():
    join_code = request.args.get("join_code", "").upper()
    join_group_form = JoinGroupForm(join_code=join_code)
    group_id = db.get_group_id_from_join_code(join_code)
    group_panel = {} if not group_id else db.get_group_panel_from_group_id(
        group_id)
    return render_template("join_group.html", join_group_form=join_group_form, group_panel=group_panel, show_header=False)


@views_blueprint.route('/join_group', methods=['POST'])
@login_required
@user_data_logging
@rate_limit_manager.limit(limit=10, per=60 * 5)
@rate_limit_manager.limit(limit=10, per=60 * 5, scope_func=lambda: current_user.username)
def join_group_post():
    join_group_form = JoinGroupForm()
    if join_group_form.validate_on_submit():
        join_code = request.form["join_code"]
        if (db.group_exists_from_join_code(join_code)):
            db.join_group_with_join_code(
                current_user.username,
                join_code,
                "")
            return json_response()
        else:
            return json_response(404)
    return json_response(form_errors=join_group_form.errors)


@views_blueprint.route('/create_group', methods=['GET'])
@login_required
@user_data_logging
def create_group():
    return render_template("create_group.html", create_group_form=CreateGroupForm(), show_header=False)


@views_blueprint.route('/create_group', methods=['POST'])
@login_required
@user_data_logging
@rate_limit_manager.limit(limit=10, per=60 * 5)
def create_group_post():
    create_group_form = CreateGroupForm()
    if create_group_form.validate_on_submit():
        group_name = request.form['group_name']
        color = get_random_group_color()
        db.create_group(current_user.username, group_name, color)
        return json_response()
    return json_response(400, form_errors=create_group_form.errors)


@views_blueprint.route('/login', methods=['GET'])
@user_data_logging
def login():
    login_form = LoginForm()
    register_form = RegisterForm()
    return render_template("login.html",
                           login_form=login_form,
                           register_form=register_form,
                           skip_login_and_register_modal=True) #skip_login_and_register_modal is used so only one recaptcha is on the page, otherwise the modal steals the recaptcha.


@views_blueprint.route('/login', methods=['POST'])
@user_data_logging
@rate_limit_manager.limit(limit=5, per=60)
def login_post():
    login_form = LoginForm()

    "set a limit on login attempts to a specific account regardless of IP"
    limit_key = "%s/%s" % (request.endpoint, login_form.username.data)
    minutes = 5
    limit = 3
    per = 60 * minutes
    if rate_limit_manager.check_limit(limit_key, limit, per):
        form_errors = {
            "password": "Too many wrong password attempts. Please wait %s minutes." %
            minutes}
        return json_response(429, form_errors=form_errors)

    "validate and login"
    if login_form.validate_on_submit():
        rate_limit_manager.reset_limit(limit_key, limit, per)
        user = User.get_user_from_username(login_form.user.username, db)
        login_user(user)
        return json_response()
    return json_response(401, form_errors=login_form.errors)


@views_blueprint.route('/change_password', methods=['POST', 'GET'])
@user_data_logging
@login_required
def change_password():
    change_password_form = ChangePasswordForm(current_user.username)
    if request.method == "POST":
        if change_password_form.validate_on_submit():
            new_password = request.values.get("new_password")
            db.set_user_password(current_user.username, new_password)
            return json_response()
        return json_response(401, form_errors=change_password_form.errors)
    elif request.method == "GET":
        return render_template("change_password.html", change_password_form=change_password_form)


@views_blueprint.route("/logout", methods=["GET"])
@user_data_logging
def logout():
    if current_user.is_authenticated:
        logout_user()
    return redirect("home")


@views_blueprint.route("/user/groups", methods=["GET"])
@login_required
def groups():
    return groups_with_username(current_user.username)


@views_blueprint.route("/user/<username>/groups", methods=["GET"])
@login_required
def groups_with_username(username):
    user = User.get_user_from_username(username, db)
    section = "groups"

    "user not found, or anonymous"
    if not user or user.is_anonymous:
        return render_template("404.html"), 404

    "user is viewing another user's page"
    if current_user.username != username:
        return render_template("403.html"), 403

    group_panels = db.get_group_panels_from_username(username)
    group_ids = db.get_group_ids_from_username(username)

    try:
        index = int(request.args.get("index"))
    except:
        index = 0

    group_id = ""
    if group_ids and len(group_ids) > index:
        group_id = group_ids[index]
        member_panels = db.get_member_panels(group_id)
    else:
        member_panels = []

    permissions = db.get_group_rights(username, group_id)
    permissions = {"write": "w" in permissions, "read": "r" in permissions}

    join_group_form = JoinGroupForm()
    create_group_form = CreateGroupForm()
    kwargs = {"user_page": True, "username": username, "section": section}
    return render_template("user/groups.html",
                           member_panels=member_panels,
                           group_panels=group_panels,
                           selected_group_index=index,
                           user_page=True,
                           username=username,
                           section=section,
                           join_group_form=join_group_form,
                           create_group_form=create_group_form,
                           permissions=permissions)


@views_blueprint.route("/account/<section>", methods=["GET"])
@login_required
def generic_account(section):
    return account(current_user.username, section)


@views_blueprint.route("/user/<username>", methods=["GET"])
@views_blueprint.route("/user/<username>/<section>", methods=["GET"])
@login_required
def account(username, section="account"):
    user = User.get_user_from_username(username, db)

    "user not found, or anonymous"
    if not user or user.is_anonymous:
        return render_template("404.html"), 404

    "user is viewing another user's page"
    if current_user.username != username:
        return render_template("403.html"), 403

    kwargs = {"user_page": True, "username": username, "section": section}
    if section == "account":
        user_settings_form = UserSettingsForm()
        change_password_form = ChangePasswordForm(username)
        user_settings_form.set_with_user(current_user)
        is_email_verified = db.is_email_verified_for_username(username)
        gravatar_url = db.get_gravatar_url_from_username(username)
        return render_template("user/account.html",
                               user_settings_form=user_settings_form,
                               change_password_form=change_password_form,
                               is_email_verified=is_email_verified,
                               gravatar_url=gravatar_url, **kwargs)
    elif section == "questions":
        question_planks = db.get_question_planks_from_username(username)
        question_plank_jsons = [json.dumps(i) for i in question_planks]
        return render_template("user/questions.html", question_plank_jsons=question_plank_jsons, **kwargs)
    else:
        "section is a list_name"
        if section not in USER_LIST_NAMES:
            current_app.logger.error("not list name = '%s'  USER_LIST_NAMES = %s" %
                                    (section, str(USER_LIST_NAMES)))
            return render_template("404.html"), 404
        plank_data_jsons = get_user_list_plank_data(username, section)
        return render_template("user/list.html", plank_data_jsons=plank_data_jsons, **kwargs)


def get_user_list_plank_data(username, list_name):
    "This function is not in the database classes because the json conversion feels more dependent on the views and template."
    uuids = db.get_user_list(username, list_name)
    plank_datas = [
        db.get_plank_from_username_uuid(
            username,
            uuid) for uuid in uuids]
    return [json.dumps(plank_data) for plank_data in plank_datas]

@views_blueprint.route('/data/<filename>')
@login_required
def open_data(filename):
    path=current_app.static_folder+"/open_data/"+filename
    if os.path.isfile(path):
        return send_file(path,
                     mimetype="text/csv",
                     attachment_filename=filename,
                     as_attachment=True)
    else:
        return render_template("404.html"), 404

