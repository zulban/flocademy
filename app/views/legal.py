from flask import render_template, send_from_directory

from flask import current_app
from app.views import views_blueprint

@views_blueprint.route('/legal/FLOcademySharingPolicy.pdf')
def sharing_policy():
    filename="sharing-policy-2017-08-01.pdf"
    return send_from_directory(current_app.static_folder + "/legal/", filename)