import time
from functools import update_wrapper
from flask import request, g, render_template
from app.views.utilities import json_response
from dbms import db
from constants.environment import get_settings_from_env

"""
Adapted from:
http://flask.pocoo.org/snippets/70/
"""


def _on_over_limit(limit):
    return render_template('429.html'), 429


def _get_key_and_reset(key_prefix, limit, per):
    reset = (int(time.time()) // per) * per + per
    key = "rate-limit/" + key_prefix + str(reset)
    return key, reset


class RateLimit(object):
    expiration_window = 10

    def __init__(self, key_prefix, limit, per, send_x_headers):
        self.key, self.reset = _get_key_and_reset(key_prefix, limit, per)
        self.limit = limit
        self.per = per
        self.send_x_headers = send_x_headers
        p = db.redis.pipeline()
        p.incr(self.key)
        p.expireat(self.key, self.reset + self.expiration_window)
        self.current = min(p.execute()[0], limit)

    remaining = property(lambda x: x.limit - x.current)
    over_limit = property(lambda x: x.current >= x.limit)


class RateLimitManager():

    def __init__(self):
        self.app_settings = get_settings_from_env()
        self.use_rate_limits = self.app_settings.USE_RATE_LIMITS

    def reset_limit(self, key_prefix, limit, per):
        "This resets a limit counter used by 'check_limit' matching this key."
        key, _ = _get_key_and_reset(key_prefix, limit, per)
        previous = db.redis.get(key)
        db.redis.set(key, 0)
        current = db.redis.get(key)

    def check_limit(self, key, limit, per):
        "returns true if the rate limiter identified by this key is over its limit."
        "this also increments the counter matching this key."
        "an example of a key could be <route>/<username>, like 'login/zulban'."
        rlimit = RateLimit(key, limit, per, True)
        return rlimit.over_limit

    def limit(self, limit, per=300, send_x_headers=True,
              over_limit=_on_over_limit,
              scope_func=lambda: request.remote_addr,
              key_func=lambda: request.endpoint):
        "this is the limit function used to decorate routes."

        if not self.use_rate_limits:
            return lambda f: f

        def decorator(f):
            def rate_limited(*args, **kwargs):
                key = 'rate-limit/%s/%s/' % (key_func(), scope_func())
                rlimit = RateLimit(key, limit, per, send_x_headers)
                g._view_rate_limit = rlimit
                if over_limit is not None and rlimit.over_limit:
                    return over_limit(rlimit)
                return f(*args, **kwargs)
            return update_wrapper(rate_limited, f)

        return decorator
