import json
from flask import render_template, request, current_app
from flask_login import current_user

from utilities import random_uuid
from app.forms import *
from app.views import views_blueprint
from app.views.api import api_unauthorized
from app.views.account import login

@views_blueprint.errorhandler(401)
def unauthorized(e):
    if str(request.url_rule).startswith("/api/"):
        return api_unauthorized()
    return login()


@views_blueprint.errorhandler(403)
def page_unauthorized(e):
    current_app.logger.error(str(e))
    return render_template('403.html'), 403


@views_blueprint.errorhandler(404)
def page_not_found(e):
    data = dict(error_msg='%s' % e,
                username=current_user.username,
                request_url=request.url)
    message = json.dumps(data, sort_keys=True, indent=4)
    current_app.logger.error(message)
    return render_template('404.html'), 404


@views_blueprint.errorhandler(429)
def page_over_limit(e):
    if request.method == "GET":
        return render_template('429.html'), 429
    return json_response(429)


@views_blueprint.errorhandler(500)
def internal_server_error(e):
    error_id = random_uuid()
    data = dict(username=current_user.username,
                error_msg='%s' % e,
                request_url=request.url,
                request_values=request.values,
                error_id=error_id)
    message = json.dumps(data, sort_keys=True, indent=4)
    current_app.logger.error(message)
    current_app.logger.error(e, exc_info=True)
    return render_template('500.html',
                           error_id=error_id), 500
