
from flask import jsonify


def json_response(http_status=200, **kwargs):
    "This is a generic JSON API response that assumes HTTP 200."
    "Keyword arguments are syntactically easier to read/write than curlies and quotes."
    if not kwargs:
        kwargs = {"status": http_status}
        if http_status == 200:
            kwargs["message"] = "success"
    return jsonify(kwargs), http_status
