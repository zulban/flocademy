"use strict";

var FADE_TIME=400;

function assertEqual (result,expected, message) {
	if (result!==expected){
		throw message+" result="+result+" expected="+expected ||
			"Assert failed" +" result="+result+" expected="+expected;
	}
}

jQuery.fn.visible = function() { return this.css('visibility', 'visible');};
jQuery.fn.invisible = function() { return this.css('visibility', 'hidden');};

//Redraws jquery element(s). After dyunamic DOM creation, sometimes UI is not immediately redrawn.
//use:  $(.p).redraw();
$.fn.redraw = function() {
	$(this).each(function() {
		var redraw = this.offsetHeight;
	});
	return $(this);
};

function clamp(value,minimum,maximum){
	return Math.max(minimum,Math.min(maximum,value));
}

function getKeys(dictionary){
	var keys = [];
	for (var key in dictionary) {
		if (dictionary.hasOwnProperty(key)) {
			keys.push(key);
		}
	}
	return keys;
}

function setLoadingAppearance (element, isLoading, loadingAtBottom, iconWidth){
	// Creates/destroys a loading GIF inside element, at the top of bottom.
	// loadingAtBottom defaults to false
	var loadingImages=$(element).find(".loading-image");
	var newStyle = "visibility: visible;"
	if (iconWidth){
		newStyle += ' width: '+iconWidth+"px;";
	}
	if (isLoading && loadingImages.length == 0){
		var new_element = $('<img>', {class: 'loading-image img-responsive img-center',
			src: URL_FOR_STATIC+'img/loading.gif', style: newStyle});
		if (loadingAtBottom){
			element.append(new_element);
		}else{
			element.prepend(new_element);
		}
		element.redraw();
	}
	
	if ( !isLoading){
		loadingImages.remove();
	}
}

function setAnswerSelectedAppearance(answer){
	//sets unselected appearance for other answers, then selected appearance for answer
	if ($(answer).length != 1){
		console.error("setAnswerAppearance got answer length not equal to 1. length = "+$(answer).length);
	}	
	$(answer).siblings().removeClass("answer-selected");
	$(answer).siblings().find(".answer-icon").removeClass("ion-android-radio-button-on").addClass("ion-android-radio-button-off");
	$(answer).addClass("answer-selected");
	$(answer).find(".answer-icon").removeClass("ion-android-radio-button-off").addClass("ion-android-radio-button-on");
}

function reloadDestination(){
    //Reloads the page after a possible login/register.
    //If no 'destination' parameter in URL, reloads current URL.
    var destination = getURLParameter("destination");
    if (destination){
        window.location.href(destination);
    }else{
        location.reload(true);
    }
}

function makeFadingMessage(container, message, completeFunction){
	"This makes a message appear in container and fade away slowly."
	var className="fading-message";
    $(container).find("."+className).remove();
    $(container).append("<h3 class='"+className+"'>"+message+"</h3>");
    $("."+className).fadeOut(2000,completeFunction);
}

function getURLParameter(key){
	//Given a key, returns the parameter value for that key in the current URL.
	var url = window.location.search.substring(1);
	var splitParameters = url.split('&');
	for (var i = 0; i < splitParameters.length; i++){
		var pair = splitParameters[i].split('=');
		if (pair[0] == key){
			return pair[1];
		}
	}
}

function getYouTubeID(url){
	//Given a YouTube URL, returns the ID parameter for that video.
	if (url.indexOf("www.youtube.com")==-1){
		console.error("getYouTubeVideoID failed for URL = "+url);
		return;
	}
	return getURLParameter("v");
}

function addInlineMenuItem(container,label,yesFunction,yesText,cancelText){
	if (yesText===undefined){
		yesText="Yes";
	}
	if(cancelText===undefined){
		cancelText="Cancel";
	}
	
    $(container).append("<div class='inline-menu'></div>");
	var inlineMenu=$(container).find(".inline-menu").last();
	$(inlineMenu).append("<span class='inline-menu-label'>"+label+"</span>");
	$(inlineMenu).append("<span class='inline-menu-button'><a>"+label+"</a></span>");
	$(inlineMenu).append("<span class='inline-menu-yes'>  <a>"+yesText+"</a></span>");
	$(inlineMenu).append("<span class='inline-menu-cancel'>  <a>"+cancelText+"</a></span>");
	
	var menuLabel=$(inlineMenu).find(".inline-menu-label");
	var menuButton=$(inlineMenu).find(".inline-menu-button");
	var yesButton=$(inlineMenu).find(".inline-menu-yes");
	var cancelButton=$(inlineMenu).find(".inline-menu-cancel");
	
	$(yesButton).on("click",yesFunction);
	
	function setYesCancelVisibility(isVisible){
		$(menuLabel).toggle(isVisible);
		$(menuButton).toggle(!isVisible);
		$(yesButton).toggle(isVisible);
		$(cancelButton).toggle(isVisible);
	}
	
	$(menuButton).on("click",function(){setYesCancelVisibility(true);})
	$(yesButton).on("click",function(){setYesCancelVisibility(false);})
	$(cancelButton).on("click",function(){setYesCancelVisibility(false);})
	setYesCancelVisibility(false);
}

function getTemplateClone(query){
	var clone = $(query).clone(true,true);
	if (clone.length != 1){
		console.error("Uh oh! getTemplateClone query did not find exactly one element. query = '"+query+"'");
	}
	clone.removeClass("template");
	clone.removeAttr("id");
	return clone;
}

function showFormErrors(form,form_errors){
	//Given a dictionary form_errors whose keys are HTML tag 'names' and whose values are lists of strings,
	//creates text HTML elements just below the input or div elements with 'name=' the form_error keys.
	console.log("form_errors = "+JSON.stringify(form_errors));
	form.find("input").removeClass("bad-input");
	hideFormErrors();
	var keys=getKeys(form_errors);
	for(var i=0;i<keys.length;i++){
		var key=keys[i];
		var inputs=form.find("input[name='"+key+"']");
		var divs=form.find("div[name='"+key+"']");
		var elements=inputs.add(divs);
		elements.addClass("bad-input");
		elements.parent().append("<div class='form-error-text'>"+form_errors[key]+"</div>");
	}
}

function hideFormErrors(){
	$(".form-error-text").remove();
}

function clickFormSubmit(submitButton,postURL,successFunction, failureFunction){
    //This will find the form belonging to submitButton and POST its form data to postURL.
    //Shows validation errors within the form if submit is not a success.
	// successFunction is called if form POST succeeds.
    
    var form = $(submitButton).closest("form");
	var success=false;
    $.post(postURL, form.serialize(), successFunction)
	.fail(function(data){
		
		if(data.responseJSON){
			var form_errors=data.responseJSON.form_errors;
		}else{
			var form_errors={};
		}		
		var is_empty=Object.keys(form_errors).length == 0;
		
		// If too many requests error but no form_errors, forward the user to the generic "too many requests" page.
		if(data.status == 429 && is_empty){
			window.location.assign("/too_many_requests");
			return;
		}
		
		var has_errors = typeof form_errors === "object" && !is_empty;
		if(has_errors){			
			showFormErrors(form,form_errors);
		}
		if(failureFunction){
		failureFunction(data);
		}
	});
}