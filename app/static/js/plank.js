"use strict";

function clearFeedItems() {
	var defer = $.Deferred();
	$(".feed-container").children().fadeOut(FADE_TIME, function() {
		$(this).remove();
		defer.resolve();
	});
	return defer.promise();
};

function hidePlank(plank){
	//Hides that plank and reveals the "unhide" button.
	if(! $(plank).hasClass("plank")){
		console.error("hidePlank was not given a plank.");
		return;
	}
	var toHide=$(plank).find(".unhidden-plank");
	var toReveal=$(plank).find(".hidden-plank");	
	toHide.fadeOut( FADE_TIME,function() {
		toReveal.attr("style","display:block;");
		});
}

function unhidePlank(){
	//Reveals the regular plank, and hides the "unhide" button.
	var plank=$(this).closest(".plank");
	if(! $(plank).hasClass("plank")){
		console.error("unhidePlank was called, but no plank was found in the parents of the event element.");
		return;
	}
	$(plank).find(".unhidden-plank").attr("style","display:block;");
	$(plank).find(".hidden-plank").attr("style","display:none;");
}

function setPlankTagWithTagData(plankTag,tagData){
	var text =tagData.title;
	if (tagData.subtitle){
		text+=" ("+tagData.subtitle+")";
	}
	$(plankTag).find(".plank-tag-text").text(text);
	var style="width: "+tagData.percent+"%;";
	$(plankTag).find(".plank-tag-fill").attr("style",style);
	style="width: "+(100-tagData.percent)+"%;";
	$(plankTag).find(".plank-tag-rfill").attr("style",style);
	
	$(plankTag).find(".plank-tag-fill > p").text(tagData.label_up);
	$(plankTag).find(".plank-tag-rfill > p").text(tagData.label_down);
}

function setPlankWithPlankData (plank,plankData){
	if (! plank){
		console.error("setPlankWithPlankData got null plank.");
		return;
	}
	//Updates a single feed item with id="plank-#" where # is index
	$(plank).attr("uuid",plankData.uuid);
	//set plank left
	$(plank).find(".plank-thumbnail > img").attr("src",URL_FOR_STATIC+"img/thumbnails/"+plankData.thumbnail);
	var jsonURL="/api/flo?uuid="+plankData["uuid"];
	var renderURL="/flo?uuid="+plankData["uuid"];
	$(plank).find(".flo-button-a").attr("href",jsonURL);
	
	//set plank center
	$(plank).find(".plank-title").html(plankData["title"]);
	$(plank).find(".plank-description").html(plankData["description"]);
	$(plank).find(".plank-title").attr("href",renderURL);
	$(plank).find(".plank-schema-title").html(plankData["schema_title"]);
	
	//set plank right
	var tagContainer=$(plank).find(".plank-tag-container");
	tagContainer.children().remove();
	var numberOfRelevantTags=5;
	if (plankData.tags && plankData.tags.length>0){
		var tagCount=Math.min(plankData.tags.length,numberOfRelevantTags);
		for(var i=0;i<tagCount;i++){
			var plankTag=getTemplateClone("#plank-tag-template");
			tagContainer.append(plankTag);
			var tagData=plankData.tags[i];
			setPlankTagWithTagData(plankTag,tagData);
		}
	}
	
	//plank buttons
	var active=plankData.active_plank_buttons;
	for(var i=0;i<active.length;i++){
		var plankButton=$(plank).find("span[list-name='"+active[i]+"']");
		setPlankButtonAppearance(plankButton,true);
	}
}

function setFeedWithResponse (feedResponse) {
	if (feedResponse.feed && feedResponse.feed.length > 0){
		var siblingCount = $(".feed-container > .plank").length;
		var total=feedResponse.feed.length;
		var feedContainer = $(".feed-container");
		setLoadingAppearance(feedContainer,false);
		for(var i=0;i<total;i++){
			var plankData=feedResponse.feed[i];
			appendNewPlank(feedContainer,plankData);
		}
		appendMoreFeedButton(feedContainer);
	} else {
		console.error("setFeedWithResponse got a bad data. feedResponse = "+JSON.stringify(feedResponse));
	}						
}
							
function refreshFeed (number) {
	var feedContainer = $(".feed-container");
	if(feedContainer.length==0){
		console.debug("refreshFeed found no plankContainer on this page.");
		return;
	}
	feedContainer.empty();
	setLoadingAppearance(feedContainer,true);
	$.getJSON(SCRIPT_ROOT + '/api/feed', {number:number}, setFeedWithResponse);
}

function clearQuestion(questionContainer) {
	var defer = $.Deferred();
	$(questionContainer).addClass("waiting-for-content")
	var questionPlanks=$(questionContainer).find(".plank");
	questionPlanks.fadeOut( FADE_TIME,function() {
		$(questionPlanks).remove();
		
		//only set loading appearance if data still hasn't arrived after the fade
		if($(questionContainer).hasClass("waiting-for-content")){
			setLoadingAppearance(questionContainer,true);
		}
		defer.resolve();
	});
	return defer.promise();
};

function refreshQuestion(question){
	var questionContainer=$(".question-container");
	if(questionContainer.length==0){
		console.debug("refreshQuestion found no questionContainer on this page.");
		return;
	}
	setLoadingAppearance(questionContainer,true);
	clearQuestion(question);
	$.getJSON(SCRIPT_ROOT + '/api/next_question',
			function(data) {
					setLoadingAppearance(questionContainer,false);
					setQuestionContainer(questionContainer,data["next_question"]);
				}).fail(function(xhr) {
					console.error("refreshQuestion got a bad HTTP status: "+xhr.status);
				});
}

function setQuestionContainer (questionContainer,data){
	//set the question prompt, answers, stars, etc using the question data.
	
	//store this question data. Used by the previous question navigation button
	var currentKey="currentQuestionData";
	var previousKey="previousQuestionData";
	var previousData = $(questionContainer).data(currentKey);
	if(typeof previousData == 'undefined'){
		//This is the first in the series of questions for this page, do not show the previous question button.
		previousData = {};
		previousData.showPreviousButton=false;
	}else{
		previousData.showPreviousButton=true;
	}
	//Set all show_prompts to false. Pressing 'previous question' should not show a prompt again.
	previousData.show_prompts={};
	$(questionContainer).data(previousKey,previousData);
	data.showPreviousButton=true;
	$(questionContainer).data(currentKey,data);
	
	//if no question data, say we are out of questions.
	if (data==null || data.uuid==null){
		console.log("setQuestionContainer got empty data, hiding questionContainer.");
		$(questionContainer).hide(0);
		var container=$(".prompt-container");
		var subtitle="You answered all the questions. You can change your answers, or check out the recommendations below.";
		createPrompt(container,"Done!",subtitle,"Change Answers",
					 function() {window.location.assign("/account/questions");})
		return;
	}
	$(questionContainer).removeClass("waiting-for-content");
	setLoadingAppearance(questionContainer,false);
	var questionElement=getTemplateClone("#question-template");
	var questionCenter=$(questionElement).find(".question-center");
	var jsonURL="/api/flo?uuid="+data["uuid"];	
	
	//left
	questionElement.find(".flo-button-a").attr("href",jsonURL);
	var starPanel=$(questionElement).find(".star-panel")
	starPanel.attr("uuid",data.uuid);
	var stars = Math.round(data.user_weight*5);
	stars = clamp(stars,1,5);
	starPanel.attr("stars",stars);
	mouseLeaveStarPanel.call(starPanel);
	
	//left navigation
	var previousData = $(questionContainer).data(previousKey);
	var showPreviousButton = ! (typeof previousData == 'undefined') && previousData.showPreviousButton;
	questionElement.find(".previous-question-button").toggle(showPreviousButton);
	questionElement.find(".see-all-questions-button").toggle(!IS_ANONYMOUS);
	if(!IS_ANONYMOUS){
		questionElement.find(".see-all-questions-button").attr("href","/account/questions");
	}
	
	//center
	questionCenter.attr("uuid",data["uuid"]);
	questionCenter.find(".prompt").html(data["prompt"]);
	
	//answers
	var answers=questionCenter.find(".question-answers");
	answers.children().remove();
	for(var i=0;i<data["answers"].length;i++){
		var answer=getTemplateClone("#answer-template");
		answer.find(".answer-text").text(data["answers"][i]);
		if (i==data.selected_answer){
			setAnswerSelectedAppearance(answer);	
		}
		answers.append(answer);
	}
	
	//right
	$(questionElement).find(".simple-tooltip-text").html(data.description);
	
	//explore prompt
	if (data.show_prompts && data.show_prompts.explore){
		var promptContainer = $(".prompt-container");
		$(questionContainer).hide(0);
		createPrompt(promptContainer,"Scroll down to see our recommendations for you.",
				 "Or you can keep answering questions to get even better recommendations.",
				 "Okay",
				 function(){
					$.post(SCRIPT_ROOT + '/api/set_prompt_answer', { prompt_name:"explore",answer:"yes" });
					setTimeout(function(){
						$(questionContainer).show(0);
					},FADE_TIME);					
					});	
	}
	
	//star rating prompt
	if (data.show_prompts && data.show_prompts.stars){
		var promptContainer = $(".prompt-container");
		$(questionContainer).hide(0);
		createPrompt(promptContainer,"Stars let you change how much influence each answer has.",
				 "Stars are found on the left of every question.",
				 "Okay",
				 function(){
					$.post(SCRIPT_ROOT + '/api/set_prompt_answer', { prompt_name:"stars",answer:"yes" });
					setTimeout(function(){
						$(questionContainer).show(0);
					},FADE_TIME);					
					});	
	}
	
	$(questionContainer).append(questionElement);
}

function getAnswerDataFromElement(answerElement){
	var index = $(answerElement).closest(".question-answers").children(".answer").index(answerElement);
	var uuid = $(answerElement).closest(".question-center").attr("uuid");
	var weight = getStarWeight(answerElement);
	var questionContainer = $(answerElement).closest(".question-container");
	return {index: index, uuid: uuid, container:questionContainer, weight:weight};
}

function getAnswerIndexFromQuestionPlank(plankElement){
	"Provide any element within an answer plank, and this will return the index of the selected answer, or -1 if none."
	var selected = $(plankElement).closest(".question-plank").find(".answer.answer-selected");
	if ($(selected).length == 0){
		return -1
	}else{
		return getAnswerDataFromElement(selected).index;
	}
}

function clickAnswer(){
	//bind this function to HTML answer elements
	
	//get the tag-answer index, but only consider siblings of the parent tag-answers div
	var answerData = getAnswerDataFromElement(this);
	setAnswerSelectedAppearance(this);
	var promise1 = clearFeedItems();
	var promise2 = clearQuestion(answerData.container);	

	var promise3 = $.getJSON(SCRIPT_ROOT + '/api/send_answer', {
		uuid: answerData.uuid,
		index: answerData.index,
		weight: answerData.weight,
		get_next_question: "1"
	}).fail(function(xhr){
		console.error("/api/send_answer got a bad HTTP status: "+xhr.status)
	}).pipe(function(data){
		//refresh answer tag if we can find a user answers page tag element
		updateQuestionTag(answerData.uuid);
		
		return data;
	});
	
	$.when(promise1,promise3).done(function(data1,data2){
		refreshFeed(DEFAULT_FEED_LENGTH);
	});
	
	$.when(promise2,promise3).done(function(data1,data2){		
		setQuestionContainer(answerData.container,data2["next_question"]);
	});
}

function clickPreviousQuestionButton(){
	var questionContainer=$(this).closest(".question-container");
	var questionData=questionContainer.data("previousQuestionData");
	clearQuestion(questionContainer);
	setQuestionContainer(questionContainer,questionData);
}

function setPlankButtonAppearance(plankButton,isOn){
	if (isOn){
		$(plankButton).addClass("plank-button-on");
	}else{
		$(plankButton).removeClass("plank-button-on");			
	}
}

function clickPlankButton(){
	//bind this function to plank-buttons which add FLOs to user lists
	
	//Anonymous can't do this, only show login modal.
    if(IS_ANONYMOUS){
        showLoginModal("You need to be logged in to have lists.");
        return;
    }
	
	var uuid = $(this).closest(".plank").attr("uuid");
	var listName = $(this).attr("list-name");
	var isOn = $(this).hasClass("plank-button-on");
	var action = isOn ? "remove" : "add";
	setPlankButtonAppearance(this,!isOn);
	
	//Hide this plank if button was off, and it's a hider button
	if(!isOn && $(this).hasClass("plank-button-hider")){
		var plank=$(this).closest(".plank");
		hidePlank(plank);
	}
	
	$.post(SCRIPT_ROOT + '/api/set_user_list', {
		uuid: uuid,
		list_name: listName,
		action: action
	}).fail(function(xhr){
			console.error("clickPlankButton got a bad HTTP status = "+xhr.status);
	});
}

function fillContainerWithPlankDataJsons(selector){
	var container=$(selector).first();
	if (! container){
		console.debug("fillContainerWithPlankDataJsons no container. selector = '"+selector+"'");
		return;
	}
	for(var i=0;i<PLANK_DATA_JSONS.length;i++){
		var data=PLANK_DATA_JSONS[i];
		var plankData=JSON.parse(data);
		appendNewPlank(container,plankData);
	}
}

function appendMoreFeedButton(container){
	var className="more-feed-button";
	$("."+className).remove();
	$(container).append("<div class='"+className+"'>See more results</div>");
	$("."+className).on('click', clickMoreFeedButton);
}

function clickMoreFeedButton(){
	//add DEFAULT_FEED_LENGTH more results to the feed container
	$(".more-feed-button").remove();	
	var feedCount=$(".feed-container").children(".plank").length;
	var loadingAtBottom=true;
	setLoadingAppearance($(".feed-container"),true,loadingAtBottom);
	$.getJSON(SCRIPT_ROOT + '/api/feed', {number:DEFAULT_FEED_LENGTH, offset:feedCount}, setFeedWithResponse);
}

function appendNewPlank(container,plankData){
	"uses plankData to create a new plank and append to container."
	setLoadingAppearance(container,false);
	var plank = getTemplateClone("#plank-template");
	setPlankWithPlankData(plank,plankData);
	container.append(plank);
}

function createPrompt(container,title,subtitle,buttonText,buttonFunction){
	var prompt = getTemplateClone("#plank-prompt-template");
	$(prompt).find(".prompt-title").html(title);
	$(prompt).find(".prompt-subtitle").html(subtitle);
	$(prompt).find(".prompt-button").html(buttonText).on("click",buttonFunction);
	container.prepend(prompt);
	return prompt;
}

function setPlankWithUrlParameter(selector){
	//this function creates one static plank on the page. It's used by flo renders.
	var container=$(selector);
	var uuid = getURLParameter("uuid");
	if(! container){
		console.debug("setPlankWithUrlParameter found no container.");
		return;
	}
	if(! uuid){
		console.debug("setPlankWithUrlParameter found no uuid in URL.");
		return;
	}
	setLoadingAppearance(container,true);
	$.post(SCRIPT_ROOT + '/api/get_plank', { uuid: uuid },
		   function(data) {
				appendNewPlank(container,data.plank);				
			}).fail(function(xhr){
				console.error("setPlankWithUrlParameter got a bad HTTP status = "+xhr.status+" UUID = "+uuid);
		});
}

function showWelcomePrompt(){
	var container=$(".prompt-container");
	if (!container){
		console.error("welcome prompt failed to find container.");
	}
	
	var prompt = createPrompt(container,"Welcome!",
				 "Do you want to learn about tech or teach it, but don't know where to start? Answer questions so we can recommend free learning objects to you.",
				 "Get Started",
				 function(){$.post(SCRIPT_ROOT + '/api/set_prompt_answer', { prompt_name:"welcome",answer:"yes" });});
	$(".answer").on("click",function(){$(prompt).fadeOut(FADE_TIME);});
}



