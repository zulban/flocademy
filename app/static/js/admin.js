"use strict";

function changeTagVote(){
	var uuid = $(this).attr("uuid");
	var tag_uuid = $(this).attr("tag_uuid");
	var value = $(this).val();		
	var category = $(this).attr("category");

	$.post(SCRIPT_ROOT + '/admin/set_vote', {
		uuid: uuid,
		tag_uuid: tag_uuid,
		value: value,
		category: category
	}).fail(function(xhr){
			console.error("changeTagVote got bad HTTP status: "+xhr.status);
		});
}

function changeTagWeight(){
	var uuid = $(this).attr("uuid");
	var value = $(this).val();		
	var category = $(this).attr("category");

	$.post(SCRIPT_ROOT + '/admin/set_tag_weight', {
		uuid: uuid,
		value: value,
		category: category
	}).fail(function(xhr){
			console.error("changeTagWeight got bad HTTP status: "+xhr.status);
	});
}

function clickResetPassword(){
	var username = $(this).attr("username");
	showConfirmModal("Reset password for user '"+username+"'?",
					 function(){
						$.post(SCRIPT_ROOT + '/admin/reset_user_password', {username: username},
							   function(data) {
									window.alert("The new password for user '"+username+"' is '"+data.new_password+"'");
								}).fail(function(xhr){
									console.error("reset_user_password got bad HTTP status: "+xhr.status);
								});
						});
}

$(document).ready(function(){
    $('.set-tag-vote').on('change paste keyup', changeTagVote);
	$('.set-tag-weight').on('change paste keyup', changeTagWeight);
	$('.reset-password-button').on('click', clickResetPassword);
}); 

