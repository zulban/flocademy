"use strict";

$(document).ready(function(){
    //home page    
    $("#question-template").find('.answer').on('click', clickAnswer);
    refreshQuestion($(".question-container"));
	refreshFeed(DEFAULT_FEED_LENGTH);
    
    //planks
	$('.plank-button').on('click', clickPlankButton);
    $('.unhide-plank-button').on('click', unhidePlank);
    $('.previous-question-button').on('click',clickPreviousQuestionButton);
	//the static plank container is meant to load all plank_data found in the PLANK_DATA_JSONS global set by jinja
	fillContainerWithPlankDataJsons(".static-plank-container");
    
    //user page
	fillQuestionTagContainer();
    
    //render the one static plank if it exists, used in flo render pages
	setPlankWithUrlParameter("#url-plank-container");
    
    //pressing enter on input fields is the same as clicking id=submit in that form
    $("input").bind("keypress",function(e){
        if (e.which == 13){
            $(e.target).closest("form").find("#submit").trigger("click");
        }
    });
    
    //by default, prompt-plank buttons make the prompt disappear
    $('.prompt-button').on('click', function(){$(this).parent(".prompt-plank").fadeOut(FADE_TIME);});
    
    //welcome prompt
    if (SHOW_WELCOME_PROMPT){
        showWelcomePrompt();
    }
    
    //a button in the verify/confirm email process
    $(".set-email-button").on("click",clickVerifyEmailButton);
}); 