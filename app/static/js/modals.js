"use strict";

function hideModals(){
    $(".modal").css("display","none");
}

function showModal(modal_id){
    $("#"+modal_id).css("display","block");
}

function showLoginModal(promptText){    
    //Check if showLoginModal was called as a callback, where promptText becomes 'this' of the caller.
    if(typeof promptText != "string"){
        promptText="";
    };
    $(".plank-prompt").text(promptText);
    
    showModal("login-modal");
}

function clickJoinGroupButton(){
    var container=$(this).closest(".plank");
    clickFormSubmit(this,"/join_group",
                    function(){window.location.replace("/user/groups");},
                    function(){makeFadingMessage(container,"Bad group code!");});
}

function clickCreateGroupButton(){
    clickFormSubmit(this,"/create_group",function(){window.location.replace("/user/groups");});
}

function clickChangePasswordButton(){
    hideFormErrors();
    clickFormSubmit(this,"/change_password",function(){
        makeFadingMessage($(".change-password-plank-body"),"New password set.",hideModals);
    });
}

function showConfirmModal(prompt,confirmFunction,yesText,cancelText){
    //Shows prompt and asks yes or cancel. yesText and cancelText are optional.
    $("#confirm-modal-title").text(prompt);
    $("#confirm-modal-button-yes").text(yesText ? yesText : "Yes");
    $("#confirm-modal-button-cancel").text(cancelText ? cancelText : "Cancel");
    
    //Clear previous event functions
    $("#confirm-modal-button-yes").off("click");
    $("#confirm-modal-button-yes").on("click",confirmFunction);
    $("#confirm-modal-button-yes").on("click",hideModals);
    
	showModal("confirm-modal");
}

function setupModal(modalID,yesSelector,summonerSelector,yesFunction){
    $(yesSelector).on("click",yesFunction);
    $(summonerSelector).on("click",function(){showModal(modalID)});
}

$(document).ready(function(){
    //Register all the modals. For now the code appears very similar but perhaps they won't be in the future?
    
    //Generic hide modals. Click 'X' or click outside the modal.
    $(".modal-close-x").on("click",hideModals);
    window.onclick = function(event) {
        if (event.target && $(event.target).attr("class") == "modal"){
            hideModals();
        }
    }
    
    setupModal("login-modal",".login-button",".login-modal-summoner",
               function(){clickFormSubmit(this,"/login",reloadDestination);});
    $(".register-button").on("click",function(){clickFormSubmit(this,"/register",reloadDestination);});
    
    setupModal("join-group-modal",".join-group-button",".join-group-modal-summoner",clickJoinGroupButton);
    setupModal("create-group-modal",".create-group-button",".create-group-modal-summoner",clickCreateGroupButton);
    setupModal("change-password-modal",".change-password-button",".change-password-modal-summoner",clickChangePasswordButton);
}); 
