"use strict";

function setMirrorHTML (mirrorContainer,panel_data){
	//Updates a single mirror container
	var embed = $(mirrorContainer).find(".mirror-embed");
	var uuid=mirrorContainer.attr("uuid");
	var rtype = panel_data.type;

	if (rtype == "video"){
		var url = panel_data.embed_url;
		if (url){
			var embed_html="<div id=\"player-wrapper\"><iframe width=\"500\" height=\"281\" src=\""+panel_data.embed_url+"\" frameborder=\"0\" allowfullscreen></iframe></div>";
			embed.append(embed_html);
		}else{
			console.error("setMirrorHTML video has no URL. URL = '"+url+"'");
		}
	}
}

function renderPrettyJSONs(){
	var jsonContainers = $(".json-container");
	if (jsonContainers.length > 0){
		renderPrettyJSON(jsonContainers);
	}
}

function renderPrettyJSON(jsonContainer){
	var uuid=jsonContainer.attr("uuid");

	jsonContainer.html("");
	setLoadingAppearance(jsonContainer,true);

	$.getJSON(SCRIPT_ROOT + '/api/flo', {
		uuid: uuid
	}, function(data) {
			setLoadingAppearance(jsonContainer,false);
			jsonContainer.text(JSON.stringify(data.response,null,2));
		}).fail(function(xhr){
			console.error("renderPrettyJSON got a bad status: "+xhr.status);
	});
}
	
$(document).ready(function(){
	renderPrettyJSONs();
}); 

