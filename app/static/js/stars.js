"use strict";

function mouseLeaveStarPanel(){
    var stars=$(this).attr("stars");
    var index=parseInt(stars)-1;
    setStarPanelAppearance($(this),index);
}

function mouseEnterStar(){
    var starIcons=$(this).parent().find(".star-icon");
    var index=$(starIcons).index(this);
    var starPanel=$(this).parent();
    var stars=parseInt(index)+1;
    $(starPanel).attr("mouse-stars",stars);
    setStarPanelAppearance(starPanel,index);
}

function answerIsSetOnPlank(plankElement){
    var index = getAnswerIndexFromQuestionPlank(plankElement);
    return index != -1;
}

function getStarWeight(plankElement){
    "Given any element in a question plank, returns the star weight from 0 to 1."
    var starPanel = $(plankElement).closest(".question-plank").find(".star-panel");
    var stars=$(starPanel).attr("stars");
    return stars/5;
}

function clickStar(){
    var starPanel=$(this).parent();
    var stars=$(starPanel).attr("mouse-stars");
    var index=parseInt(stars)-1;
    setStars(starPanel,index);
    
    if(answerIsSetOnPlank(starPanel)){
        $(this).closest(".question-plank").find(".answer.answer-selected").trigger("click");
    }
}
    
function setStars(starPanel,index){
    var stars=parseInt(index)+1;
    $(starPanel).attr("stars",stars);
    setStarPanelAppearance(starPanel,index);
}

function setStarPanelAppearance(starPanel,index){
    var starIcons=$(starPanel).children();
    if (index<0 || index>=starIcons.length){
        console.error("setStars index must be [1,"+starIcons.length+"]  index = '"+index+"'");
        return;
    }        
    for(var i=0;i<5;i++){
        if (i>index){
            $(starIcons.get(i)).removeClass("ion-android-star").addClass("ion-android-star-outline");
        }else{
            $(starIcons.get(i)).addClass("ion-android-star").removeClass("ion-android-star-outline");
        }
    }
    var labels=["Least influence","Some influence","Lots of influence","Great influence","Most influence"];
    $(starPanel).find(".star-text").text(labels[index]);
}

function registerStarPanel(starPanel,clickFunction){
    //clickFunction is called when a star is clicked
    //can accept arguments: clickFunction(stars,starPanel)
    var index=parseInt($(starPanel).attr("stars"))-1;
    setStarPanelAppearance(starPanel,index);
    
    $(starPanel).on("mouseleave",mouseLeaveStarPanel);
    $(starPanel).find(".star-icon").on("mouseenter",mouseEnterStar);
    $(starPanel).find(".star-icon").on("click",clickStar);
    $(starPanel).find(".star-icon").on("click",function (){
        var starPanel = $(this).parent();
        var stars = parseInt($(starPanel).attr("stars"));
        clickFunction(stars,starPanel);
        });
}
    
$(document).ready(function(){
    registerStarPanel($(".question-left").find(".star-panel"),clickStar)
}); 

