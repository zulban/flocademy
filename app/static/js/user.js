"use strict";

function setQuestionTagWithQuestionPlank(tag, questionPlank) {
    $(tag).attr("uuid", questionPlank.uuid);

    $(tag).find(".question-tag-title").html(questionPlank.title);
    $(tag).find(".question-tag-subtitle").html(questionPlank.subtitle);
    $(tag).find(".question-tag-prompt").html(questionPlank.prompt);

    var answerText = "Answer: " + questionPlank.answers[questionPlank.selected_answer];
    $(tag).find(".question-tag-answer").html(answerText);
}

function updateQuestionTag(uuid) {
    "Retrieves up to date information for this question tag from the server (like a new answer) and updates the tag if it exists."
    var tag = $(".question-tag[uuid='" + uuid + "']");
    if ($(tag).length > 0) {
        $.post(SCRIPT_ROOT + '/api/get_question_plank', {
            uuid: uuid
        }, function (data) {
                setQuestionTagWithQuestionPlank(tag, data.question_plank);
            }).fail(function(xhr){
                console.error("updateQuestionTag got a bad HTTP status = " + xhr.status);
        });
    }
}

function fillQuestionTagContainer() {
    var container = $(".question-tag-container").first();
    if (!container) {
        console.debug("fillQuestionTagContainer no container.");
        return;
    }
    for (var i = 0; i < QUESTION_PLANKS.length; i++) {
        var data = QUESTION_PLANKS[i];
        var questionPlank = JSON.parse(data);
        var tag = getTemplateClone("#question-tag-template");
        setQuestionTagWithQuestionPlank(tag, questionPlank);
        container.append(tag);
    }
    $('.question-tag-plank').on('click', clickQuestionTag);
}

function clickQuestionTag() {
    //bind this function to HTML question tag elements
    var uuid = $(this).attr("uuid");
    var questionContainer = $(".user-question-container").first();

    $(questionContainer).children().remove();

    $.post(SCRIPT_ROOT + '/api/get_question_plank', {
        uuid: uuid
    }, function (data) {
            setQuestionContainer(questionContainer, data.question_plank);
        }).fail(function(xhr){
            console.error("clickQuestionTag got a bad HTTP status: " + xhr.status);
    });
}

function submitUserSettings() {
    clickFormSubmit(this, "/set_user_settings", function () {
        makeFadingMessage($(".user-settings-container"), "User settings saved.");
    });
}

function clickVerifyEmailButton() {
    console.log("click verify");
    clickFormSubmit(this, "/account/verify_email", function () {
        var email = document.getElementById("email").value;
        if (email) {
            window.location.href = "/account/verify_email_sent?email=" + email;
        }
    });
}

function clickGroupLock() {
    var lock = $(".group-lock-icon");
    if (lock.hasClass("disabled")) {
        return;
    }
    var newLockStatus = !isGroupJoinLocked();
    sendLockStatus(newLockStatus);
    setLock(newLockStatus);

    function setTipVisibility(isVisible) {
        lock.find(".simple-tooltip-text").css("display", isVisible ? "inherit" : "none");
    }

    setTipVisibility(false);
    lock.one("mouseleave", function () {
        setTipVisibility(true);
    })
}

function setLockEnabled(isEnabled) {
    // Clicking a disabled lock won't do anything.
    $(".group-lock-icon").toggleClass("disabled", !isEnabled);
}

function setLock(isLocked) {
    //Refreshes content in join code elements.
    //Also adds or removes the 'locked' class, and refreshes join code appearance.
    //If isLocked=true, hides join code, asks server to generate a new join code.
    //If isLocked=false, sets loading appearance, asks server for join code for this group, and puts join code in the HTML.
    var codeText = $(".join-code-text");
    var codeLink = $(".join-code-link");
    var loadingWidth = 21;
    var group_id = getGroupIDFromLock();

    $(".group-lock-icon").toggleClass("locked", isLocked);
    setLockAppearance(isLocked);
    if (isLocked) {
        //If locked, no need to request join code from server, just send a reset code request.
        setLockEnabled(false);
        $.getJSON(SCRIPT_ROOT + '/api/groups/reset_join_code', {
            group_id: group_id
        }, function (data) {
            setLockEnabled(true)
        });
    } else {
        setLoadingAppearance(codeText, true, false, loadingWidth);
        setLoadingAppearance(codeLink, true, false, loadingWidth);

        $.getJSON(SCRIPT_ROOT + '/api/groups/get_join_code', {
            group_id: group_id
        }, function (data) {
            setLoadingAppearance(codeText, false);
            setLoadingAppearance(codeLink, false);
            codeText.html(data.join_code);
            var origin = window.location.origin;
            codeLink.html(origin+"/join_group?join_code=" + data.join_code);
            }).fail(function(xhr){
                console.error("get_group_code got a bad HTTP status: " + xhr.status);
        });
    }
}

function getGroupIDFromLock() {
    return $(".group-name.selected").attr("group_id");
}

function sendLockStatus(isLocked) {
    var group_id = getGroupIDFromLock();
    var url = '/api/groups/' + (isLocked ? 'lock_group' : 'unlock_group');
    $.getJSON(SCRIPT_ROOT + url, {
        group_id: group_id
    });
}

function isGroupJoinLocked() {
    return $(".group-lock-icon").hasClass("locked");
}

function setLockAppearance(isLocked) {
    var lock = $(".group-lock-icon");
    var tooltip = $(lock).find(".simple-tooltip-text");
    var codeText = $(".join-code-text");
    var codeLink = $(".join-code-link");

    lock.toggleClass("ion-android-lock", isLocked);
    lock.toggleClass("ion-android-unlock", !isLocked);
    codeText.toggleClass("group-locked-text", isLocked);
    codeLink.toggleClass("group-locked-text", isLocked);
    if (isLocked) {
        tooltip.html("Click here to unlock the group so that people can join it.");
        codeText.html("(locked)");
        codeLink.html("(locked)");
    } else {
        tooltip.html("Click here to lock the group. Nobody can join a locked group.");
        codeText.html("");
        codeLink.html("");
    }
}

function setupLock() {
    var lock = $(".group-lock-icon");
    if (lock.length == 0) {
        console.debug("No join code lock found on this page.");
        return;
    }
    $(".group-lock-icon").on("click", clickGroupLock);
    var group_id = getGroupIDFromLock();

    $.getJSON(SCRIPT_ROOT + '/api/groups/get_lock_status', {
        group_id: group_id
    }, function (data) {
            setLock(data.lock_status);
        }).fail(function(xhr){
            setLockEnabled(false);
            console.error("setupLock got a bad HTTP status: " + xhr.status);
    });
}

function setupManageMembers() {
    $(".manage-member-inline-menus").each(function () {
        var container = this;
        var group_id = getGroupIDFromLock();

        function setGroupPermissions(clicked, permissions) {
            var username = $(clicked).closest(".group-member-panel").attr("username");
            $.post(SCRIPT_ROOT + '/api/groups/set_group_rights', {
                group_id: group_id,
                username: username,
                permissions: permissions
            });
        }

        addInlineMenuItem(container, "Set as collaborator", function () {
            setGroupPermissions(this, "r");
        });
        addInlineMenuItem(container, "Set as learner", function () {
            setGroupPermissions(this, "");
        });
        addInlineMenuItem(container, "Remove from group", function () {
            var username = $(this).closest(".group-member-panel").attr("username");
            $.post(SCRIPT_ROOT + '/api/groups/leave_group', {
                group_id: group_id,
                username: username
            });
        });
    });
}

$(document).ready(function () {
    var container = document.getElementById('user-settings-container');
    if (container) {
        $(".user-settings-button").on("click", submitUserSettings);
    } else {
        console.debug("No user settings form found on this page.");
    }

    setupManageMembers();
    setupLock();

    "delete group inline menu"
    addInlineMenuItem($(".delete-group-inline-menu"), "Delete Group", function () {
        var group_id = getGroupIDFromLock();
        $.post(SCRIPT_ROOT + '/api/groups/delete_group', {
            group_id: group_id
        });
    });

    "leave group inline menu"
    addInlineMenuItem($(".leave-group-inline-menu"), "Leave Group", function () {
        var group_id = getGroupIDFromLock();
        $.post(SCRIPT_ROOT + '/api/groups/leave_group', {
            username: "",
            group_id: group_id
        });
    });
});