#Resize 2048x2048 PNG files.

#This script finds all PNG files in the current directory, assumes they are 2048x2048, and generates resized copies for 1024x1024 and 512x512.

for source in *.png; do
	echo "RESIZING $source"

	target="${source/'.png'/'-1024.png'}"
	convert -resize 1024x1024 $source $target

	target="${source/'.png'/'-512.png'}"
	convert -resize 512x512 $source $target
done
