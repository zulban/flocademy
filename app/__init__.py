import os
import sys

from flask import Flask

from utilities import setup_app_logger
from app.views import views_blueprint, rate_limit_manager
from constants import FLOCADEMY_ENV_KEY, get_settings_from_env


def is_reloader_process():
    "werkzeug launches the Flask app twice - the second time to track code changes for reloading."
    "This returns true when this is the second launched process."
    return bool(os.environ.get('WERKZEUG_RUN_MAIN'))


def configure_app(app, env=""):
    """Set the appropriate config based on the environment settings"""

    from constants import FLOCADEMY_ENV_KEY
    from itsdangerous import URLSafeTimedSerializer

    settings = get_settings_from_env(env)
    app.config.from_object(settings)
    app.logger.setLevel(settings.LOGGING_LEVEL)
    db.configure(settings, app=app)
    app.db = db

    if app.config["USE_SENTRY"]:
        from raven.contrib.flask import Sentry
        from werkzeug.contrib.fixers import ProxyFix
        "The ProxyFix is likely necessary when using nginx"
        app.wsgi_app = ProxyFix(app.wsgi_app)
        "DSN not specified, so Sentry will attempt to read it from environment variable SENTRY_DSN"
        sentry = Sentry(app, level=app.config["SENTRY_LOGGING_LEVEL"])

    app.url_safe_timed_serializer = URLSafeTimedSerializer(
        app.config["SECRET_KEY"])
    app.logger.warning("app is starting up: finished configuring app.")


def create_app(env=""):
    try:
        app = Flask(__name__)
        setup_app_logger(app)

        from flask_login import LoginManager
        login_manager = LoginManager()
        login_manager.init_app(app)

        from app.models import AnonymousUser, User
        login_manager.anonymous_user = AnonymousUser

        @login_manager.user_loader
        def load_user(user_id):
            return User.get_user_from_username(user_id, db)

        configure_app(app)
        app.register_blueprint(views_blueprint)
        return app
    except:
        app.logger.exception("Failed to build app on start up.")
        raise


from dbms import db

app = create_app()
