from flask_wtf import Form
from wtforms import TextField, PasswordField, validators

from app.models import User
from dbms import db
from constants import STRINGS, REGEX


class CustomErrorForm():

    def custom_error(self, field_name, error):
        if not self._errors:
            self._errors = {}
        if field_name not in self._errors:
            self._errors[field_name] = []
        self._errors[field_name].append(error)
        self._fields[field_name].errors.append(error)


class RegisterForm(Form, CustomErrorForm):
    username = TextField('choose a username', [validators.Required(),
                                               validators.Regexp(regex=REGEX.USERNAME, message=STRINGS.INVALID_USERNAME)])
    password = PasswordField(
        'password',
        [validators.Required(),
         validators.EqualTo('confirm',
                            message=STRINGS.PASSWORD_NOMATCH),
         validators.Regexp(regex=REGEX.PASSWORD, message=STRINGS.INVALID_PASSWORD)])
    confirm = PasswordField('verify password', [validators.Required()])
    email = TextField('email (optional)', [validators.Optional(),
                                validators.Email()])

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)

    def validate(self):
        rv = Form.validate(self)
        if not rv:
            return False

        if db.is_username_taken(self.username.data):
            self.custom_error("username", STRINGS.NAME_TAKEN)
            return False
        return True


class CreateGroupForm(Form, CustomErrorForm):
    group_name = TextField('Group Name', [validators.Required()])

    def validate(self):
        rv = Form.validate(self)
        if not rv:
            return False
        return True


class JoinGroupForm(Form, CustomErrorForm):
    join_code = TextField('Join Code', [validators.Required()])

    def validate(self):
        rv = Form.validate(self)
        if not rv:
            return False
        return True


class LoginForm(Form, CustomErrorForm):
    username = TextField('username', [validators.Required()])
    password = PasswordField('password')

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)
        self.user = None

    def validate(self):
        rv = Form.validate(self)
        if not rv:
            return False

        user = User.get_user_from_username(self.username.data, db)
        if not user or not user.check_password(self.password.data):
            self.custom_error("password", STRINGS.BAD_LOGIN)
            return False

        self.user = user
        return True


class VerifyEmailForm(Form, CustomErrorForm):
    password = PasswordField('password')
    email = TextField('email')

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)
        self.username = kwargs.get("username", None)

    def validate(self):
        rv = Form.validate(self)
        if not rv:
            return False

        if not self.username:
            return False
        user = User.get_user_from_username(self.username, db)
        if not user or not user.check_password(self.password.data):
            self.custom_error("password", STRINGS.BAD_PASSWORD)
            return False
        return True


class UserSettingsForm(Form, CustomErrorForm):
    display_name = TextField('Display name')
    email = TextField('Email')

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)
        self.user = None

    def validate(self):
        rv = Form.validate(self)
        if not rv:
            return False
        return True

    def set_with_user(self, user):
        self.display_name.data = user.display_name
        self.email.data = user.email


class ChangePasswordForm(Form, CustomErrorForm):
    old_password = PasswordField('old password', [validators.Required()])
    new_password = PasswordField('new password', [validators.Required(),
                                                  validators.EqualTo(
                                                  'confirm',
                                                  message=STRINGS.PASSWORD_NOMATCH),
                                                  validators.Regexp(regex=REGEX.PASSWORD, message=STRINGS.INVALID_PASSWORD)])
    confirm = PasswordField('confirm new password', [validators.Required()])

    def __init__(self, username, *args, **kwargs):
        self.username = username
        Form.__init__(self, *args, **kwargs)

    def validate(self):
        rv = Form.validate(self)
        if not rv:
            return False

        user = User.get_user_from_username(self.username, db)
        if not user or not user.check_password(self.old_password.data):
            self.custom_error("old_password", STRINGS.BAD_PASSWORD)
            return False
        return True
