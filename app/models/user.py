from werkzeug.security import check_password_hash


class User():

    def __init__(self, username, db):
        self.username = str(username)
        self.db = db

    def get_user_from_username(username, db):
        "This method was created to remove the User (and thus app) dependency from DatabaseManager"
        if db.is_username_taken(username):
            return User(username, db)
        return None

    @property
    def pw_hash(self):
        if self.is_anonymous:
            return ""
        return self.db.get_pw_hash_from_username(self.username)

    @property
    def email(self):
        if self.is_anonymous:
            return ""
        return self.db.get_email_from_username(self.username)

    @property
    def display_name(self):
        if self.is_anonymous:
            return "Anonymous"
        return self.db.get_display_name_from_username(self.username)

    def check_password(self, attempt):
        return self.db.check_password(self.username, attempt)

    def get_id(self):
        return str(self.username)

    @property
    def is_admin(self):
        if self.is_anonymous:
            return False
        return self.db.get_admin_level(self.username) > 0

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    @property
    def is_authenticated(self):
        return True

    def __repr__(self):
        return "<User '%s'>" % self.username
