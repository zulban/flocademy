import uuid
from flask import session

from app.models.user import User
from constants import KEYS


class AnonymousUser(User):

    def __init__(self):
        if KEYS.ANONYMOUS in session:
            self.username = session[KEYS.ANONYMOUS]
        else:
            self.username = "Anonymous-" + uuid.uuid4().hex
            session[KEYS.ANONYMOUS] = self.username

    def check_password(self, attempt):
        return False

    @property
    def is_active(self):
        return False

    @property
    def is_anonymous(self):
        return True

    @property
    def is_authenticated(self):
        return False

    def __repr__(self):
        return "<AnonymousUser '%s'>" % self.username
