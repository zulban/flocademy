echo
echo "INSTALLING THE FLOCADEMY WEBSITE."
echo "This scripts configures this machine to run the FLOcademy website. Installs dependencies and sets up web configuration files."
echo "This script is meant to be run locally on the web server machine. It creates the flocademy folder in the current folder."
echo "Requires the local .ssh folder to have read access to the FLOcademy github repo. This can be set up with the remote-server-setup.sh script."

sudo echo
if ! [ $? -eq 0 ]; then
    echo "Aborted. Bad password.";
    exit 1;
fi

cd ~/
echo 
echo "CLONING FLOCADEMY TO THE FOLDER: '"`pwd`"'"
git clone git@github.com:Zulban/flocademy.git
if ! [ $? -eq 0 ]; then
    echo "Aborted. Failed to clone github repo."
    echo "Maybe the .ssh folder was not copied for read-only github access. Or the flocademy folder already exists.";
    exit 1;
fi

cd flocademy
bash setup/install-development-dependencies.sh
if ! [ $? -eq 0 ]; then
    echo "Aborted. Install dependencies failed.";
    exit 1;
fi

echo
echo "INSTALLING WEB SERVER DEPENDENCIES"
venv/bin/pip3 install uwsgi

sudo python3 setup/modify_sshd_config.py
if ! [ $? -eq 0 ]; then
    echo "Failed to modify sshd_config file.";
    exit 1;
fi

#Restart SSH daemon to apply sshd_config
sudo systemctl reload sshd

#Set firewall UFW settings
sudo ufw app list
sudo ufw allow OpenSSH
sudo ufw enable
sudo ufw status

#Install dependencies that only the web server requires.
sudo apt-get install python3-dev nginx -y

#Create logging folder specified by uwsgi_config.ini file.
sudo mkdir -p /var/log/uwsgi
sudo chown -R zulban:zulban /var/log/uwsgi

#setup and copy the uWSGI service to the system
echo
echo "SETTING UP SENTRY"
read -p "Configure sentry DSN? [y/n]: " yn;
SENTRY_DSN=""
if [[ $yn =~ [yY] ]] ; then
    echo "Provide the Sentry DSN. This is a long URL similar to this pattern:  https://[a-f0-9:]+@sentry.io/[0-9]+"
    read -p "Sentry DSN: " SENTRY_DSN;
fi
#flocademy_service always must be modified, to replace the invalid <sentry-dsn> placeholder
sudo python3 setup/modify_flocademy_service.py "$SENTRY_DSN"
sudo cp setup/flocademy.service /etc/systemd/system/

#start and enable the uWSGI service so that it starts at boot:
sudo systemctl stop flocademy
sudo systemctl start flocademy
sudo systemctl enable flocademy

#Configure nginx.
#server_name can be ip, or domain if set up
echo
echo "CONFIGURING NGINX"
CURRENT_IP=`dig +short myip.opendns.com @resolver1.opendns.com`
echo "Provide a server_name for nginx. This can be an IP, a domain, or domains. Here's two examples:"
echo "    www.flocademy.ca"
echo "    www.flocademy.ca flocademy.ca"
echo "Or just press enter to use the current external IP:"
echo "    $CURRENT_IP"
read -p "server_name: " server_name;

if [ -z "$server_name" ]; then #if user just pressed enter, use CURRENT_IP
    echo "Using current external IP for nginx server_name: $CURRENT_IP"
    server_name=$CURRENT_IP
fi

read -p "Configure website for HTTPS? [y/n]: " yn;
if [[ $yn =~ [yY] ]] ; then
    sudo python3 setup/create_nginx_config_file.py "$server_name" --https
    
    echo
    DH_SOURCE=~/dhparam.pem
    DH_TARGET=/etc/ssl/certs/
    if [ -f $DH_SOURCE ]; then
        echo "Copying '$DH_SOURCE' to '$DH_TARGET'"
        sudo cp $DH_SOURCE $DH_TARGET
    else
        echo "GENERATING STRONG DH GROUP"
        echo "To avoid this computation, before running the install script place a DH file here: '$DH_SOURCE'"
        sudo openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048
    fi
    
    echo "LETS ENCRYPT CERTIFICATES"
    echo "Expected are four .pem files. If not, run certbot or copy the files to this server."
    echo "Contents of /etc/letsencrypt/live/"
    sudo ls -l /etc/letsencrypt/live/
    
    sudo cp setup/ssl-flocademy.ca.conf /etc/nginx/snippets/ssl-flocademy.ca.conf
    sudo cp setup/ssl-params.conf /etc/nginx/snippets/ssl-params.conf
    
    #allow https traffic
    sudo ufw allow 'Nginx Full'
    #disallow http traffic
    sudo ufw delete allow 'Nginx HTTP'
else
    sudo python3 setup/create_nginx_config_file.py "$server_name"
fi

#enable the Nginx server block configuration:
sudo ln -s /etc/nginx/sites-available/flocademy /etc/nginx/sites-enabled

echo
echo "TESTING NGINX CONFIGURATION FOR ERRORS"
sudo nginx -t

#restart nginx to read new config:
sudo systemctl restart nginx

sudo ufw allow 'Nginx Full'

echo
echo "SETTING UP DATABASE"
read -p "Initialize database with baseline data? [y/n]: " yn;
if [[ $yn =~ [yY] ]] ; then
    venv/bin/python3 manage_data.py update production data/baseline
fi

echo
echo "ADMINISTRATION"
read -p "Create new admin account? [y/n]: " yn;
if [[ $yn =~ [yY] ]] ; then
    export FLOCADEMY_ENV=production
    venv/bin/python3 manage_data.py createadmin production
    export FLOCADEMY_ENV=
fi

bash scripts/restart_nginx_and_service.sh

if [ $? -eq 0 ]; then
    echo
    echo "FINISHED INSTALLING FLOCADEMY:"
    git describe
fi

