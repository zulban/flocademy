"""This script sets sshd_config values."""

import re
import os
import sys

is_sudo = os.getuid() == 0
if not is_sudo:
    print("Abort. This script must be run as sudo.")
    sys.exit(1)

path = "/etc/ssh/sshd_config"
settings = {"ClientAliveInterval": "600", "PasswordAuthentication": "no",
            "PubkeyAuthentication": "yes", "ChallengeResponseAuthentication": "no"}
try:
    with open(path, "r") as f:
        data = f.read()

    "Replace all values in sshd_config with the values in settings dictionary."
    "If the key is there, replace. If not, append."

    r = "%s [^\n]*\n"
    for key in settings:
        value = settings[key]
        if key in data:
            data = re.sub(r % key, "%s %s\n" % (key, value), data)
        else:
            data += "\n\n#Added via script:\n%s %s" % (key, value)

    with open(path, "w") as f:
        f.write(data)
except:
    print("Abort.")
    sys.exit(1)
