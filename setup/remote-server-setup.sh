echo "Run this script to install the FLOcademy website to a remote web server."
echo "This script asks for the IP address of a web server with a clean install of a Ubuntu 16.04."

SECRETS_FOLDER=_secrets
if ! [ -d $SECRETS_FOLDER ]; then
    echo "Aborted. Cannot find secrets folder. This includes SSH files for read only access to github repo.";
    echo "Expected folder with .ssh folder contents = '"$SECRETS_FOLDER"'";
    exit 1;
fi

read -p "IP address of remote web server: " ip;
if ! [[ $ip =~ [12]?[0-9]{1,2}\.[12]?[0-9]{1,2}\.[12]?[0-9]{1,2}\.[12]?[0-9]{1,2} ]]
    then echo "Aborted. Not an IP: "$ip;
    exit 1;
fi

#Clear this IP from our history.
#It's possible this IP belonged to a now deleted machine.
#If so, it would throw a security warning because its public keys are different from what was last known.
ssh-keygen -f ~/.ssh/known_hosts -R $ip

#Create new user, set up SSH keys for this machine to have access, and for the remote machine to have read-only github access.
NEW_USER=zulban
ssh root@$ip "adduser --gecos \"\" $NEW_USER" #gecos removes name, phone, etc prompt
ssh root@$ip "usermod -aG sudo $NEW_USER"
rsync -rz $SECRETS_FOLDER/ root@$ip:/home/$NEW_USER/.ssh/
rsync -rz server-bashrc root@$ip:/home/$NEW_USER/.bashrc
ssh root@$ip "chown -R $NEW_USER:$NEW_USER /home/$NEW_USER/.ssh"

#Send the local install script to the server, and SSH into it to continue the process.
echo 
echo "SENDING INSTALL FILES TO REMOTE MACHINE"
BASH_SCRIPT_FILENAME="install-flocademy-server.sh"
echo "Once you have connected to the remote machine and navigate to its home directory, run this bash script:"
echo "    bash $BASH_SCRIPT_FILENAME"
echo 
rsync $BASH_SCRIPT_FILENAME $NEW_USER@$ip:~/

#The DH param file may not have to be a secret, but I am not sure, so it is.
DH_SOURCE=$SECRETS_FOLDER/dhparam.pem
if [ -f $DH_SOURCE ]; then
    rsync $DH_SOURCE $NEW_USER@$ip:~/
fi

ssh $NEW_USER@$ip