
#UBUNTU DEPENDENCIES
echo 
echo "INSTALLING UBUNTU DEPENDENCIES"
sudo apt-get update -y
if ! [ $? -eq 0 ]; then
    echo "Aborted. This script requires sudo."
    exit 1;
fi

sudo apt-get install python3 python3-dev git python3-venv redis-server mongodb-server build-essential -y

#SELENIUM WEB DRIVER
echo 
echo "INSTALLING SELENIUM WEB DRIVER FOR BROWSER TEST SUITE."
sudo apt-get install xvfb unzip -y

FOLDER=_chromedriver
rm -rf $FOLDER
mkdir -p $FOLDER
cd $FOLDER
wget -q https://chromedriver.storage.googleapis.com/2.35/chromedriver_linux64.zip
if ! [ $? -eq 0 ]; then
    echo "Aborted. Failed to download Chrome Web Driver."
    exit 1;
fi

unzip *.zip
cd ..

#VIRTUAL ENVIRONMENT
echo 
echo "SETTING UP PYTHON VIRTUAL ENVIRONMENT WITH PYTHON LIBRARIES."
python3.6 -m venv venv
venv/bin/pip3 install --upgrade pip
venv/bin/pip3 install -r setup/requirements.txt

chmod +x manage_app.py
chmod +x manage_data.py

#SECURITY CONSTANTS
echo
echo "SECURITY"
echo "Set security keys manually? If no, random keys will be set."
read -p "Random keys will cause all sessions from other deployments to expire. Also, some features like recaptcha might not work. [y/n]: " yn;
if [[ $yn =~ [yY] ]] ; then
    venv/bin/python3 setup/constants-security-template.py
else
    venv/bin/python3 setup/constants-security-template.py --random
fi
