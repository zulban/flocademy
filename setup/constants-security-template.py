"""
This file contains security secrets for the FLOcademy server.

This file must be filled in and copied to 'constants/security.py' for FLOcademy to run. This
can be done programmatically by running this file from the app folder:
        python3 setup/constants-security-template.py

If you want to make changes and you're viewing this file from 'constants/security.py',
consider changing the file setup/constants-security-template.py' instead.
"""

import string
import sys
import re
import random


class SECURITY:
    EMAIL_VERIFICATION_SALT = "{{EMAIL_VERIFICATION_SALT}}"
    RECAPTCHA_PUBLIC_KEY = "{{RECAPTCHA_PUBLIC_KEY}}"
    RECAPTCHA_PRIVATE_KEY = "{{RECAPTCHA_PRIVATE_KEY}}"

    "This key is used to encrypt sessions. When it changes, all sessions expire."
    SECRET_KEY = "{{SECRET_KEY}}"


def get_keys(text):
    """Finds all double curly bracket items in this file.
    Returns all items as 'keys' (without brackets) and 'brackets' (with brackets)"""
    r = "{{[^{}\\n]+}}"
    brackets = re.findall(r, text)
    keys = [b[2:-2] for b in brackets]
    return zip(keys, brackets)


def random_chars(size=32, chars=string.ascii_uppercase +
                 string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def set_security_constants(use_random):
    "Prompts the user for security constants, puts them in this file, and saves to constants/security.py"
    with open(__file__, "r") as f:
        data = f.read()

    items = get_keys(data)
    for key, bracket in items:
        if use_random:
            a = random_chars()
        else:
            a = input("\n%s: " % key)
        data = data.replace(bracket, a)

    path = "constants/security.py"
    with open(path, "w") as f:
        f.write(data)
    print("Wrote security constants to file: '%s'" % path)

if __name__ == "__main__":
    use_random = "--random" in sys.argv
    set_security_constants(use_random)
