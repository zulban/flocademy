#Run this script to generate new private keys from the Let's Encrypt service.

sudo add-apt-repository ppa:certbot/certbot
sudo apt-get update
sudo apt-get install certbot

sudo certbot certonly --webroot -w /home/zulban/flocademy/app/static -d flocademy.ca -d www.flocademy.ca

echo
echo "TESTING NGINX CONFIGURATION FOR ERRORS"
sudo nginx -t

echo
sudo systemctl restart nginx

# https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-ubuntu-16-04
# https://certbot.eff.org/#ubuntuxenial-nginx