echo "CREATING BACKUP OF THE LETS ENCRYPT PEM FILES '$SOURCE_FOLDER'"
SOURCE_FOLDER=/etc/letsencrypt/
NEW_FILE=~/letsencrypt_pem_backup.tar.gz
sudo tar -zcvf $NEW_FILE $SOURCE_FOLDER

echo "To restore Lets Encrypt, extract the files in '$NEW_FILE' to '$SOURCE_FOLDER' on the new server, then restart nginx."