import sys
import os


def show_help():
    print(
        "Usage:\n   python3 create_nginx_config_file.py <server-names> [--https]")

if len(sys.argv) not in (2, 3):
    print("Aborted create_nginx_config_file.")
    show_help()
    sys.exit()

server_name = sys.argv[1]

use_https = "--https" in sys.argv
filename = "nginx-https-flocademy" if use_https else "nginx-http-flocademy"

with open("setup" + os.sep + filename, "r") as f:
    data = f.read() % server_name

path = "/etc/nginx/sites-available/flocademy"
with open(path, "w") as f:
    f.write(data)
