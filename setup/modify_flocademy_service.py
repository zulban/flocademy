
"This script opens the flocademy.service file and inserts the SENTRY_DSN environment variable from the sys.argv CLI arguments."
path = "setup/flocademy.service"

with open(path, "r") as f:
    data = f.read()

import sys
key = "{{SENTRY_DSN}}"
if key not in data:
    print(
        "Aborted. Could not find '%s' in contents of file '%s'" %
        (key, path))
    sys.exit(1)

if len(sys.argv) != 2:
    print("Aborted. Bad number of arguments: %s" % len(sys.argv))
    sys.exit(1)

sentry_dsn = sys.argv[1]
data = data.replace(key, sentry_dsn)

with open(path, "w") as f:
    f.write(data)
print(
    "Successfully wrote sentry DSN value '%s' to file '%s'" %
    (sentry_dsn, path))
