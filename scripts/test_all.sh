set -ex

export FLOCADEMY_ENV=testing
./manage_app.py test --unit --story --flo --coverage
