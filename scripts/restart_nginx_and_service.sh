set -ex

echo systemctl daemon-reload
systemctl daemon-reload
echo Stopping systemctl flocademy
sudo systemctl stop flocademy
echo Starting systemctl flocademy
sudo systemctl start flocademy
echo Enabling systemctl flocademy
sudo systemctl enable flocademy

echo Restarting nxginx
sudo systemctl restart nginx

echo Done.
