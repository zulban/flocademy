function show_usage(){
    echo 
    echo "This script dumps, restores, and gets info for FLOcademy databases."
    echo "'dump' creates dump files."
    echo "'load' stops database services, replaces the databases with dumps from the source folder, and restarts database services."
    echo "'info' provides information about all databases."
    echo
    echo "Usage:"
    echo "     bash database_io.sh dump"
    echo "     bash database_io.sh load <source-folder>"
    echo "     bash database_io.sh info"
}

function confirm(){
    read -p "Are you sure? [y/n]: " yn;
    if [[ $yn =~ [^yY] ]] ; then
        echo "Aborted."
        exit 1
    fi
}

if [ "$1" == "dump" ]; then
	bash redis_dump.sh
elif [ "$1" == "load" ] && [ -d "$2" ]; then

    echo "This operation will disrupt database services."
    confirm
    
    #REDIS
    RDB_PATH="$2"/dump.rdb
    if [ -e $RDB_PATH ]; then
        echo "Stopping redis service."
        sudo service redis-server stop
        echo "Copying redis dump file."
        sudo cp $RDB_PATH /var/lib/redis/
        echo "Starting redis service."
        service redis-server start
    else
        echo "Did not find file: $RDB_PATH"
    fi

elif [ "$1" == "info" ]; then
    echo
    echo "REDIS INFO"
    echo 'info Persistence' | redis-cli 
else
    show_usage
    exit 1
fi

echo
echo "Done."

