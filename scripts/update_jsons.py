#!venv/bin/python3

"""Migrate JSONs

Opens up JSONs in target directory, applies some function to them and saves.

Usage:
  update_jsons.py <folder>

Options:
    --verbose           More console output.

    -h --help           Show this screen.
    -v --version        Show version.
"""

import os.path
import json

from utilities import *
from utilities import get_matching_paths_recursively


def mutate(data):
    # this is the only function you need to change for migrations
    if "resources" in data:
        data["mirrors"] = data["resources"]
        data.pop("resources")


def migrate(folder):
    paths = get_matching_paths_recursively(folder, ".json")
    print_green("Mutating %s jsons." % len(paths))
    for path in paths:
        with open(path, "r") as f:
            try:
                j = json.loads(f.read())
            except ValueError:
                print("ValueError for: '%s'" % path)
                continue
        mutate(j)
        with open(path, "w") as f:
            f.write(json.dumps(j))

    print_green("Done.")


def main(args):
    folder = args["<folder>"]
    if not os.path.isdir(folder):
        print("Not a directory: '%s'" % folder)
        return
    migrate(folder)

if __name__ == "__main__":
    args = docopt(__doc__, version="1.0")
    main(args)
