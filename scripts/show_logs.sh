set -ex

for path in /var/log/nginx/ /var/log/uwsgi/ /var/log/wsgi /var/log/flask logs; do
    echo
    echo FILES IN: $path
    sudo ls $path
done

for path in /var/log/nginx/access.log /var/log/nxginx/error.log /var/log/syslog /var/log/uwsgi/uwsgi_config.log; do
    echo
    echo CONTENTS OF: $path
    sudo tail -n 10 $path
done

#show most recent log
for filename in logs/*.log; do
    continue;
done
echo
echo CONTENTS OF: $filename
sudo tail -n 30 $filename
