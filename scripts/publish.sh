
#This script tags and pushes the latest commit.
#Aborts if there are uncommitted changes.

set -ex


#First verify that everything is committed.
okay="nothing to commit, working tree clean"
if [[ $(git status) != *$okay* ]] ;
then
	echo "Commit your changes before publishing."
	exit
fi

echo "Current project version (git describe):"
git describe

echo
echo "Input new version (example: '0.1.3'):"
read version

#If version doesn't start with v, add it
if [[ $version != v* ]] ;
then
	version="v$version"
fi

echo "Version tag message (example: 'added support for abc, fixed bug 123'):"
read message

git tag -a $version -m "$message"
git push --follow-tags

echo "Tagged and pushed '$version'!"
