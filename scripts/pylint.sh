# This script runs the code analysis tool pylint.
# Run this script from the 'flocademy' folder.

set -ex

echo "Installing pylint dependencies."

sudo apt install pylint3
pip3 install pylint-flask

echo
echo

FILENAME=pylint.log
pylint3 --load-plugins=pylint_flask app > $FILENAME

echo "Created file '$FILENAME'."
