
DEFAULT_HOST=http://localhost:5000

if [ -z "$1" ]
then
	echo "Which host to target with the locust stress test? Press enter for the default: $DEFAULT_HOST"
	read -p "    Host: " HOST;
fi

if [ -z "$HOST" ]
then
	HOST=$DEFAULT_HOST
fi

echo "Launching locust."

sleep 5 && chromium-browser http://localhost:8089 &
venv/bin/locust -f tests/performance/locust_anonymous_student.py --host=$HOST
