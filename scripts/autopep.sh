# This script reformats all Python code recursively found here, except /venv/.

set -ex

find . -not \( -path ./venv -prune \) -type f -name "*.py" | while read line; do
   echo "running autopep8 on: $line"

   #Do not apply the fix W6.
   #W6 is py2to3, which wrecks my weird flask imports with periods before the module names.
   autopep8 --ignore=W6 --in-place --aggressive $line
done

echo "Done."
