#!venv/bin/python3

"Generates and displays a bunch of random UUIDs"

import uuid

for i in range(10):
    print(uuid.uuid4().hex)
