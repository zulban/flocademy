#!/bin/bash

#http://gihnius.net/2014/07/57-redis-rdb-snapshot/
#https://github.com/gihnius/rdb-backup

## A simple redis rdb backup script. Specify the path to the dump.rdb file, the folder you wish
## Usage:
##    redis_backup.sh <rdb-path> <target-folder> <dump-wait-seconds>

echo "Creating a dump file of the redis database."

RDB_PATH=${1:-"/var/lib/redis/dump.rdb"}

TARGET_FOLDER=${2:-"$HOME"}

DUMP_WAIT=${3:-2} ## default wait for 2 seconds

test -f "$RDB_PATH" || {
    echo No rdb file found. ; exit 1
}
test -d "$TARGET_FOLDER" || {
    echo Creating backup directory $TARGET_FOLDER && mkdir -p "$TARGET_FOLDER"
}

## launch bgsave
echo bgsave | redis-cli
echo "Waiting $DUMP_WAIT seconds."
sleep $DUMP_WAIT
ATTEMPTS=0
while [ $ATTEMPTS -lt 5 ] ; do
    RDB_SAVED=$(echo 'info Persistence' | redis-cli | awk '/rdb_bgsave_in_progress:0/{print "yes"}')
    RDB_OKAY=$(echo 'info Persistence' | redis-cli | awk '/rdb_last_bgsave_status:ok/{print "yes"}')    
    if [ "$RDB_SAVED" == "yes" ] && [ "$RDB_OKAY" == "yes" ] ; then
        echo "Copying '$RDB_PATH' to '$TARGET_FOLDER'."
        cp "$RDB_PATH" "$TARGET_FOLDER"/
        if [ $? = 0 ] ; then
            echo "Done."
            exit 0
        else 
            echo "Failed to copy!"
        fi
    fi
    ATTEMPTS=$((ATTEMPTS + 1))
    RETRY_WAIT=$((DUMP_WAIT * ATTEMPTS))
    echo "Dump not done yet. Retrying in $RETRY_WAIT seconds."
    sleep $RETRY_WAIT
done
exit 1