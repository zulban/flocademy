
""" Code Analysis

This examines code files and produces various reports.

functions - Lists the functions and signatures for all the scripts found in a folder, or in a specific file.
uncommented - Lists all functions and classes with no comments.

Usage:
  code_analysis.py functions <folder-or-file> [--filter=<text>]
  code_analysis.py uncommented <folder-or-file> [--exclude=<comma-separated-paths>]

Options:
    --filter=<text>      If provided, will only show results containing this text.
    -h --help           Show this screen.
    -v --version        Show version.
"""

import os.path

from utilities import *


def get_uncommented_objects(filename):
    with open(filename, "r") as f:
        lines = f.readlines()
    uncommented = []
    for i, line in enumerate(lines):
        if i == len(lines) - 1:
            break
        if line.strip().startswith("def ") or line.strip().startswith("class "):
            if not lines[i + 1].strip().startswith("\""):
                uncommented.append(line.strip())
    return sorted(uncommented)


def get_script_functions(filename):
    with open(filename, "r") as f:
        lines = f.readlines()
    defs = [line.strip() for line in lines if line.strip().startswith("def ")]
    return sorted(defs)


def show_script_functions(filename, filter_text=None):
    print("\n" + "*" * 40)
    print(filename.upper())
    print("*" * 40)
    for f in get_script_functions(filename):
        if not filter_text or filter_text in f:
            print(f)


def get_files_from_ambiguous(target, extension=".py"):
    "If target is a filename, returns [filename]"
    "If target is a folder, returns [filename1, filename2] with all files in the folder."
    if os.path.isfile(target):
        return [target]
    elif os.path.isdir(target):
        return get_matching_paths_recursively(target, extension)
    else:
        print("Target is not a file or folder.")
        return []


def list_uncommented(target, excluded_paths):
    for path in get_files_from_ambiguous(target):
        if any([exclude in path for exclude in excluded_paths]):
            continue
        items = get_uncommented_objects(path)
        if items:
            print("\n" + path.upper())
            for item in items:
                print(item)


def list_functions(target, filter_text):
    for path in get_files_from_ambiguous(target):
        show_script_functions(path, filter_text)


def main(args):
    if args["functions"]:
        list_functions(folder=args["<folder-or-file>"],
                       filter_text=args["--filter"])
    if args["uncommented"]:
        excluded = args["--exclude"]
        if excluded:
            excluded = excluded.split(",")
        else:
            excluded = []
        list_uncommented(args["<folder-or-file>"],
                         excluded)

if __name__ == "__main__":
    args = docopt(__doc__, version="1.0")
    main(args)
