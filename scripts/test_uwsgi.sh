# for testing. requires open port 5000
# sudo ufw allow 5000

set -ex

source venv/bin/activate
export FLOCADEMY_ENV=production
uwsgi --socket 0.0.0.0:5000 --protocol=http -w wsgi:app
