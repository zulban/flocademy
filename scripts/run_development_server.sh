export FLOCADEMY_ENV=development

#Drop database
./manage_data.py drop development --no-confirmation

#Add baseline data and debug data to database
./manage_data.py update development data/baseline/:data/test/debug_users.csv:data/test/debug_groups.csv --no-confirmation

#Run app in development mode
./manage_app.py run
