from app import app

from constants import ProductionSettings
app.secret_key = ProductionSettings.SECRET_KEY

if __name__ == "__main__":
    app.run(host=app.config["HOST"])
