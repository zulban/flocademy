import re
import os.path

import uuid as uuid_module

from utilities.path import *
from utilities.colors import *
from utilities.generic import write_pretty_json


def uuid_repair(folder, verbose=True):
    # recursively searches the folder, replacing the uuids of all jsons with valid uuids.
    # does this intelligently, matching uuids from all files.
    if not os.path.isdir(folder):
        if verbose:
            print_red("Not a path: '%s'" % folder)
        return

    paths = get_matching_paths_recursively(folder, ".json")
    if verbose:
        print_green("Repairing UUIDs in %s jsons." % len(paths))

    old_new = find_uuids(paths)
    swap_uuids(paths, old_new, verbose=verbose)

    return paths


def is_string_a_uuid(uuid):
    if len(uuid) != 32:
        return False
    for c in uuid:
        if c not in "0123456789abcdef":
            return False
    return True


def random_uuid():
    return uuid_module.uuid4().hex


def find_uuids(paths):
    old_new = {}
    for path in paths:
        with open(path, "r") as f:
            text = f.read()
        for uuid in get_uuids_from_json_string(text):
            if is_string_a_uuid(uuid):
                new_uuid = uuid
            else:
                new_uuid = random_uuid()
            old_new[uuid] = new_uuid
    return old_new


def swap_uuids_in_data(data, old_new):
    # this mutates data
    for key in data:
        if key in ("_id", "schema_uuid") and type(data[key]) is str:
            old = data[key]
            data[key] = old_new[old]
        elif type(data[key]) is dict:
            swap_uuids_in_data(data[key], old_new)
        elif type(data[key]) is list:
            for item in [i for i in data[key] if type(i) is dict]:
                swap_uuids_in_data(item, old_new)
    return data


def swap_uuids(paths, old_new, verbose=True):
    for path in paths:
        with open(path, "r") as f:
            try:
                data = json.loads(f.read())
            except ValueError:
                if verbose:
                    print_yellow("ValueError for: '%s'" % path)
                continue

            swap_uuids_in_data(data, old_new)

        write_pretty_json(path, data)
    if verbose:
        print_green("Done.")


def search_flo_for_children_uuids(flo):
    """Searches the values of all top level key/value, returns a list of UUIDs found.
    Values that are strings are included.
    Also, values that are lists are included if every item in that list is a string uuid."""
    children=[]
    for key in flo:
        if key in ("_id","schema_uuid"):
            continue
        a=flo[key]
        if type(a) is str and is_string_a_uuid(a):
            children.append(a)
        elif type(a) in (list,tuple):
            for b in a:
                if type(b) is str and is_string_a_uuid(b):
                    children.append(b)
                else:
                    break
    return children
    

def get_uuids_from_json_string(text):
    keys = ("_id", "schema_uuid")
    r = "\"(%s)\"[ \t]*:[ \t]*\"(.*?)\"" % "|".join(keys)
    return [i[1] for i in re.findall(r, text)]
