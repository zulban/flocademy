import os
import json
import shutil

from utilities.path import *
from utilities.colors import *
from constants import UUID
from utilities.generic import get_schema_paths

"This class suggests a folder path for FLOs, and can also move them there. Useful for sorting many FLOs found in a single folder."


class FloMover():

    def __init__(self, data_folder):
        self.data_folder = data_folder
        self.schema_folder = self.data_folder + "schemas/"
        if not os.path.isdir(self.schema_folder):
            print_yellow(
                "FloMover schema_folder not a folder: '%s'" %
                self.schema_folder)

        "schema titles are used as folder names."
        "set schemas"
        self.schemas = {}
        for path in get_schema_paths():
            flo = get_flo_from_path(path)
            key="_id"
            if flo and key in flo:
                self.schemas[flo[key]] = flo

    def suggest_path_from_flo_path(self, flo_path):
        "suggests a new path for the flo found at this flo_path"
        flo = get_flo_from_path(flo_path)
        return self.suggest_path_from_flo(flo)

    def suggest_path_from_flo(self, flo):
        filename = flo["_id"] + ".json"
        return self.suggest_flo_folder_from_flo(flo) + filename

    def suggest_flo_folder_from_flo(self, flo):
        "given a flo, returns a suggested path for it like 'data/mirrors/video/'"

        if "_schema" in flo:
            return self.schema_folder

        schema_uuid = flo["schema_uuid"]
        if schema_uuid == UUID.MIRROR_SCHEMA:
            return self.data_folder + "mirrors/" + flo["type"].lower().replace(" ", "-") + "/"

        subfolder = self.schemas[schema_uuid].get(
            "title",
            "no-title").lower().replace(" ",
                                        "-")
        return self.data_folder + subfolder + "/"

    def move_flos(self, folder):
        for flo_path in get_matching_paths_recursively(folder, ".json"):
            self.move_flo_with_path(flo_path)

    def move_flo_with_path(self, flo_path):
        if not os.path.isfile(flo_path):
            print_red("move_flo_with_path abort. not a file: '%s'" % flo_path)
            return

        new_path = self.suggest_path_from_flo_path(flo_path)
        if new_path == flo_path:
            return
        new_folder = new_path[:-len(new_path.split("/")[-1])]
        if not os.path.isdir(new_folder):
            print_yellow("Making directory: '%s'" % new_folder)
            os.makedirs(new_folder)
        print_green("Moving: '" + flo_path + "'\n->   '" + new_path + "'")
        shutil.move(flo_path, new_path)
