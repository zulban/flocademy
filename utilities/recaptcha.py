import requests
import json
from constants.config import TestingSettings, DevelopmentSettings

def verify_recaptcha(request_form, secret_key):
    "Returns true if the request_form has a Google recaptcha value that matches a success for this secret key."
    
    "Always success if private key matches TestingSettings"
    if secret_key in (TestingSettings.RECAPTCHA_PRIVATE_KEY, DevelopmentSettings.RECAPTCHA_PRIVATE_KEY):
        return True
    
    token = request_form.get("g-recaptcha-response", None)
    if not token:
        return False
    
    try:
        url = "https://www.google.com/recaptcha/api/siteverify"
        data = {"response": token, "secret": secret_key}
        response = requests.post(url, data=data)
        json_response = json.loads(response.text)
        return json_response["success"]
    except:
        return False
