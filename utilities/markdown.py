import copy
import markdown
from markupsafe import escape, Markup


def markdownify(flo):
    a = copy.deepcopy(flo)
    if type(a) is dict:
        for key in flo:
            a[key] = markdownify(flo[key])

    if type(a) is list:
        a = [markdownify(i) for i in a]

    if type(a) is str and is_markdown(a):
        md = markdown.markdown(escape(a))
        md = md.replace("<code>", "<pre>")
        md = md.replace("</code>", "</pre>")
        a = Markup(md)
    return a


def is_markdown(text):
    whitelist = ("\n", "\n* ", "```")
    for w in whitelist:
        if w in text:
            return True
    return False
