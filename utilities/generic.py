import re

import logging
from logging.handlers import TimedRotatingFileHandler
import datetime
import os
import sys
import os.path
import json
import time
import random

from constants import *
from utilities.colors import *
from utilities.path import *


def is_valid_username(username):
    return re.match(REGEX.USERNAME, username)


def is_valid_password(password):
    return re.match(REGEX.PASSWORD, password)


def clamp(value, minimum, maximum):
    return min(max(value, minimum), maximum)


def reorder_prioritizing_uniques(item_key_list):
    """used in schema shuffling prioritization.

    given item_key_list = ((item1,key1),(item2,key2), ...)
    given there are n unique keys in that list.
    returns a list (item1,item2, ...) where the first n items had unique keys. The remaining items keep their order."""
    unused_keys = set([i[1] for i in item_key_list])
    unique_indexes = []
    for index, item_key in enumerate(item_key_list):
        item, key = item_key
        if key in unused_keys:
            unused_keys.remove(key)
            unique_indexes.append(index)
        if not unused_keys:
            break

    results = [item_key_list[index][0] for index in unique_indexes]
    unique_index_set = set(unique_indexes)
    for index in range(len(item_key_list)):
        if index not in unique_index_set:
            results.append(item_key_list[index][0])
    return results


def generate_random_password():
    "Used for admin password resets."
    a = 1000
    return str(random.randint(a, a * 10 - 1))


def setup_app_logger(app):
    def get_log_name():
        dt = datetime.datetime.now()
        return "%s-%s-%s.log" % (dt.year, str(dt.month).zfill(2), str(dt.day).zfill(2))

    app.logger.setLevel(logging.INFO)

    try:
        os.mkdir(PATH.LOGS)
    except FileExistsError:
        pass
    
    path=PATH.LOGS+os.sep+"flocademy-log-"
    fh = TimedRotatingFileHandler(path, when="d",
                                          interval=1)
    ch = logging.StreamHandler()

    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)

    app.logger.handlers = []
    app.logger.addHandler(fh)
    app.logger.addHandler(ch)

URL_REGEX = re.compile(
    r'^(?:(http)s?://)?'  # http:// or https://
    r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'  # domain...
    r'localhost|'  # localhost...
    r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'  # ...or ip
    r'(?::\d+)?'  # optional port
    r'(?:/?|[/?]\S+)$', re.IGNORECASE)


def is_string_a_url(text):
    return URL_REGEX.match(text)


def get_route_method_from_line(line):
    """given a line like:
           @app.route('/api/set_user_list', methods=["POST"])
       returns:
           /api/set_user_list"""

    "match route given any app name or blueprint name after @"
    if not re.match("@.*\\.route", line):
        return None, None

    "remove everything before"
    line = line.split("(")[1].strip()[1:]

    if "POST" in line:
        method = "post"
    elif "GET" in line:
        method = "get"
    else:
        method = None

    for i in range(len(line)):
        if line[i] in "'\"":
            break
    route = line[:i]
    return route, method


def clean_flo_mirror(flo):
    if flo.get("schema_uuid") != UUID.MIRROR_SCHEMA:
        return flo
    keys = (
        "width", "height", "video_description", "video_best_format", "video_best_resolution",
          "backend", "length", "word_count", "page_count", "publisher", "formats", "kilobytes", "version", "file_count")
    for key in keys:
        if key in flo and not flo.get(key):
            flo.pop(key)
    return flo


def get_schema_paths():
    return get_matching_paths_recursively(PATH.SCHEMAS, ".json")


def show_duration(label, start_time, print_function=None):
    "Compares start_time to current time and prints a short report."
    duration = time.time() - start_time
    if not print_function:
        if duration < 0.05:
            print_function = print_green
        elif duration < 0.2:
            print_function = print_yellow
        else:
            print_function = print_red
    print(label)
    print_function("       %.3f seconds" % duration)


def clean_flos(folder, clean_mirrors=False, prettify=False):
    "prettify autoindents jsons for human readability"
    "clean_mirrors: removes empty key values like 'file-count'=0 inserted by new FLO web interface."

    if not os.path.isdir(folder):
        print_red("Not a path: '%s'" % folder)
        return

    paths = get_matching_paths_recursively(folder, ".json")
    print_green("Found %s FLOs." % len(paths))
    if clean_mirrors:
        print_yellow("Cleaning mirrors.")
    if prettify:
        print_yellow("Prettify.")
    for path in paths:
        flo = get_flo_from_path(path)
        if clean_mirrors:
            flo = clean_flo_mirror(flo)
        if prettify:
            flo_string = flo_to_pretty_json_string(flo)
        else:
            flo_string = json.dumps(flo)
        with open(path, "w") as f:
            f.write(flo_string)


def write_pretty_json(path, data):
    with open(path, "w") as f:
        f.write(flo_to_pretty_json_string(data))


def flo_to_pretty_json_string(data):
    return json.dumps(data, indent=2, sort_keys=True)


def random_join_code():
    return "".join([random.choice(JOIN_CODE_CHARS) for i in range(JOIN_CODE_LENGTH)])


def concatenate_strings_from_data(data,text="",delimit="||"):
    """This recursively searchse the FLO for all string values and returns one string of all combined.
    Used in a very simple implementation of search."""
    
    if type(data) is dict:
        for key in sorted(data.keys()):
            text+=concatenate_strings_from_data(data[key],delimit=delimit)
    elif type(data) in (list,tuple):
        for item in data:
            text+=concatenate_strings_from_data(item,delimit=delimit)
    else:
        text+=delimit+str(data)
    return text


def strip_password_from_data(data):
    """mutates the dictionary to strip password keys and values."""
    
    if type(data) == dict:
        for key in list(data.keys()):
            if key in ("password","verify_password","confirm_password","confirm"):
                data.pop(key)
            else:
                strip_password_from_data(data[key])
    elif type(data) in (list,tuple):
        for item in data:
            strip_password_from_data(item)

def get_embed_url_from_url(url):
    """Given a standard URL to a web resource like a YouTube video, returns a link for embedding."""
    if "youtube.com" in url:
        video_id = url[url.find("v=") + 2:].split("&")[0]
        return "https://www.youtube.com/embed/" + video_id
    elif "vimeo.com" in url:
        video_id = url.split("/")[-1]
        return "https://player.vimeo.com/video/" + video_id
    return ""


def make_mongo_results_serializable(data):
    if type(data) is dict:
        data["_id"]="mongo:"+str(data["_id"])
    elif type(data) in (tuple,list):
        for item in data:
            make_mongo_results_serializable(item)

def is_flo_a_schema(flo):
    if type(flo) is not dict:
        return False
    
    if "_schema" in flo:
        return True
    
    return False
