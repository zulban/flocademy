import sys
import traceback
import os.path
import os

from utilities import *
from utilities.generic import get_schema_paths

from jsonschema import Draft4Validator, ValidationError

class FLOValidator():
    def __init__(self, schema_path, verbose=False):
        self.schema_paths=get_matching_paths_recursively(schema_path,".json")
        self.verbose=verbose
        self.validators={}
        
        "This schema evaluates FLO schemas."
        meta = Draft4Validator.META_SCHEMA
        self.schema_validator = Draft4Validator(meta)
        
        "This schema evaluates any generic FLO."
        flo_schema = get_json(PATH.FLO_SCHEMA)
        self.generic_flo_validator = Draft4Validator(flo_schema)
    
    def is_valid_flo(self,flo,show_errors=False):
        error_message=self.get_validation_error_message(flo)
        if error_message and show_errors:
            print("\nFLOValidator rejected FLO with UUID '%s'. Details:")
            print(error_message)
        return not error_message
    
    def is_valid_flo_at_path(self,path):
        return self.is_valid_flo(get_json(path))
    
    def get_validation_error_message(self,flo):
        "Returns a string describing why this FLO is not valid. If valid, returns an empty string."
        
        if not flo:
            return "FLO is empty or False."
        
        if not self.schema_validator:
            return "FLOValidator setup failed. No schema_validator."
        
        if not self.generic_flo_validator:
            return "FLOValidator setup failed. No generic_flo_validator."
        
        if not self.generic_flo_validator.is_valid(flo):
            return "Generic FLO validation failed for '%s'"%flo.get("_id","unknown id")
        
        if is_flo_a_schema(flo):
            if not self.schema_validator.is_valid(flo):
                return "Schema validation failed for '%s'"%flo.get("_id","unknown id")
            return ""
        else:
            return self._get_validation_error_message_for_flo(flo)
    
    def _get_validation_error_message_for_flo(self,flo):
        schema_id=flo.get("schema_uuid","")
        if not schema_id:
            return "Generic FLO validation failed, no schema_uuid for FLO '%s'"%flo.get("_id","unknown id")
            
        if schema_id not in self.validators:
            j=self._get_schema_json(schema_id)
            if not j:
                return "FLOValidator setup failed. No flo_validator found for schema_id '%s'."%schema_id
            
            if not self.schema_validator.is_valid(j):
                return "FLOValidator setup failed. Found an invalid schema JSON for schema_id '%s'."%schema_id
            
            self.validators[schema_id]=Draft4Validator(j)
            
        v=self.validators[schema_id]
        if not v.is_valid(flo):
            message="FLO validation failed for '%s' for its schema '%s'"%(flo.get("_id","unknown id"),schema_id)
            message+="\n"+self.get_detailed_validation_errors(flo,v)
            return message
        
        "this FLO is valid, return no error message"
        return ""
        
    def _get_schema_json(self, schema_id):
        filename=schema_id+".json"
        j=None
        for path in self.schema_paths:
            if path.endswith(filename):
                return get_json(path)
        return None
    
    def verify_flos_in_folder(self,folder):
        "Evaluates all the FLOs in the folder and outputs evaluation results to console."
        if not os.path.isdir(folder):
            print_yellow("Not a directory: '%s'" % folder)
            return
        
        paths = get_matching_paths_recursively(folder, ".json")
        print_green("Verifying %s JSONs in '%s'." % (len(paths), folder))
        valid_count = 0
        for path in sorted(paths):
            if self.is_valid_flo_at_path(path):
                valid_count += 1
                if self.verbose:
                    print_green("SUCCESS: '%s'" % path)
            else:
                print_red("FAIL: '%s'" % path)
                print(self.get_validation_error_message(flo))
        invalid_count = len(paths) - valid_count
        if valid_count:
            print_green("%s valid FLOs." % valid_count)
        if invalid_count:
            print_red("%s invalid FLOs." % invalid_count)
            
    def get_detailed_validation_errors(self, flo, validator):
        try:
            validator.validate(flo)
            return ""
        except ValidationError:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            lines=traceback.format_exception(exc_type, exc_value, exc_traceback,
                                      limit=2)
            return "\n".join(lines)            


"""
kept for quick reference while developing new FLOValidator

class FLOValidatorOLD():

    def __init__(self, verbose=False):
        self.verbose = verbose

        "keys are uuids"
        self.schemas = {}
        "keys are uuids"
        self.validators = {}

        self.invalid_schemas = []
        
        self.setup()
        if self.verbose:
            print("FLOValidator found %s validators."%len(self.validators))

    def is_valid(self, item):
        if type(item) == str:
            return self.is_valid_from_path(item)
        elif type(item) == dict:
            return self.is_valid_flo(item)
        else:
            raise NotImplementedError

    def is_valid_from_path(self, path):
        flo = get_flo_from_path(path)
        return self.is_valid_flo(flo)
    
    def is_valid_flo(self, flo):
        return not self._get_invalid_message(flo)
    
    def _get_invalid_message(self, flo):
        "returns a string describing why this is an invalid flo. If valid, returns empty string."
        if not self.flo_validator:
            return "invalid FLO, no flo_schema or flo_validator."
        
        if not self.flo_validator.is_valid(flo):
            return "invalid FLO, is_valid = False"

        uuid = flo.get("_id","")
        if not uuid:
            return "invalid FLO, UUID is None"
        
        schema = self.get_schema_from_flo(flo)
        if not schema:
            return "invalid FLO, schema is None"
        
        if uuid in self.validators:
            v = self.validators[uuid]
        else:
            v = Draft4Validator(schema)
            self.validators[uuid] = v
            
        if v.is_valid(flo):
            return ""
        else:
            return "invalid FLO, did not pass validation. schema = '%s' \n\nself.validators = '%s'"%(str(schema),str(self.validators))

    def get_schema_from_flo(self, flo):
        if not flo:
            if self.verbose:
                print("get_schema_from_flo got empty flo")
            return None

        if "_schema" in flo:
            return Draft4Validator.META_SCHEMA

        key = "schema_uuid"
        if key not in flo:
            if self.verbose:
                print("Can't get schema from flo: no schema uuid")
            return None

        schema_uuid = flo[key]
        return self.get_schema_from_uuid(schema_uuid)

    def get_schema_from_uuid(self, uuid):
        if not self.schemas:
            self.set_schemas()
        return self.schemas.get(uuid, 0)

    def setup(self):
        meta = Draft4Validator.META_SCHEMA
        schema_validator = Draft4Validator(meta)

        try:
            with open(PATH.FLO_SCHEMA, "r") as f:
                flo_schema = json.loads(f.read())
            self.flo_validator = Draft4Validator(flo_schema)
        except:
            self.flo_validator = None
            
        paths=get_schema_paths()
        for path in paths:
            flo = get_flo_from_path(path)
            if not flo or "_id" not in flo:
                self.invalid_schemas.append(path)
                continue

            uuid = flo["_id"]
            if schema_validator.is_valid(flo):
                self.schemas[uuid] = flo
            else:
                self.invalid_schemas.append(path)

    def verbose_validation_from_path(self, path):
        flo = get_flo_from_path(path)
        if not flo and self.verbose:
            print("Resource is null.")
            return
        schema = self.get_schema_from_flo(flo)
        if not schema and self.verbose:
            print("Schema is null.")
            return

        if not self.flo_validator and self.verbose:
            print("flo_validator is null.")
            return

        self.show_validation_with_validator(flo, Draft4Validator(schema))
        self.show_validation_with_validator(flo, self.flo_validator)

    def show_validation_with_validator(self, flo, validator):
        try:
            validator.validate(flo)
        except ValidationError:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            traceback.print_exception(exc_type, exc_value, exc_traceback,
                                      limit=2, file=sys.stdout)

    def verify_learning_flos(self, folder):
        if not os.path.isdir(folder):
            print_yellow("Not a directory: '%s'" % folder)
            return

        if not folder.endswith(os.sep):
            folder += os.sep
        paths = get_matching_paths_recursively(folder, ".json")
        print_green("Verifying %s JSONs in '%s'." % (len(paths), folder))
        valid_count = 0
        for path in sorted(paths):
            if self.is_valid_from_path(path):
                valid_count += 1
                if self.verbose:
                    print_green("SUCCESS: '%s'" % path)
            else:
                print_red("FAIL: '%s'" % path)
                self.verbose_validation_from_path(path)
        invalid_count = len(paths) - valid_count
        if valid_count:
            print_green("%s valid FLOs." % valid_count)
        if invalid_count:
            print_red("%s invalid FLOs." % invalid_count)
"""