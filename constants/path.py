
class PATH():
    DATA = "data/"
    WEB_GENERATED_DATA = "data/_web_generated/"
    SCHEMAS = "data/baseline/jsons/schemas/"
    LOGS = "logs/"
    CODE_COVERAGE = "logs/coverage/"
    UNIT_TESTS = "tests/unit/"
    STORY_TESTS = "tests/story/"
    TESTS_WRITE_FOLDER = "tests/_tmp/"
    THUMBNAILS = "app/static/img/thumbnails/"
    FLO_HTML_TEMPLATES = "flos/"
    HTML_TEMPLATES = "app/templates/"

PATH.THUMBNAIL_DEFAULT = "default.png"
PATH.FLO_SCHEMA = PATH.SCHEMAS + "/841ae5f2453c41deb381717667397e8c.json"

class DATA_PATHS():
    BASELINE = "data/baseline/"
    TAG_VOTES = "data/baseline/tag_votes.csv"
    MIRROR_URLS = "data/baseline/mirror_urls.csv"
    SCHEMAS = "data/baseline/jsons/schemas/"

    TEST_USERS = "data/test/debug_users.csv"
    TEST_GROUPS = "data/test/debug_groups.csv"
    TEST_RECOMMEND = "data/test/test_qualitative_mock.csv"
