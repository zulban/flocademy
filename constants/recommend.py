
"this is the value when a user answers that they don't care about a tag"
NO_DESIRE_VALUE = -1
"this is the index when a user answers that they don't care about a tag"
NO_DESIRE_INDEX = -1
"this is the mark when a user answers that they don't care about a tag"
NO_DESIRE_MARK = 0
"The next question is based on the most voted tag for this many of the top feed items."
NEXT_QUESTION_FEED_DEPTH = 20
"This value is the recommendation scores if the FLO should not be recommended to this user."
HIDDEN_FLO_SCORE = -1000000


class DESIRE_TYPES:
    YES_BONUS = "yes bonus"
    AGREE_BONUS = "agree bonus"
    BONUS_PENALTY = "bonus penalty"
    FULL = "full"
    JUST_PENALTY = "just penalty"
DESIRE_TYPES_LIST = [getattr(DESIRE_TYPES, i)
                     for i in dir(DESIRE_TYPES) if i.upper() == i]


class TAG_VOTES:
    UP = "vote_up"
    DOWN = "vote_down"
TAG_VOTES_LIST = [getattr(TAG_VOTES, i)
                  for i in dir(TAG_VOTES) if i.upper() == i]
