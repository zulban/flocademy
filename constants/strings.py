class STRINGS():
    BAD_LOGIN = "Bad username or password."
    BAD_PASSWORD = "Bad password."
    FIELD_REQUIRED = "This field is required."  # from validator default messages
    NAME_TAKEN = "Username is taken."
    PASSWORD_NOMATCH = "Passwords must match."

    DEFAULT_LABEL_UP = "up"
    DEFAULT_LABEL_DOWN = "down"

    INVALID_USERNAME = "Invalid username. Must be at least 3 characters (letters or numbers). You can also use periods and underscores, but not twice in a row."
    INVALID_PASSWORD = "Invalid password. Must be at least 8 characters."
    INVALID_EMAIL = "Invalid email address."
