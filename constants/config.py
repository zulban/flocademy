import os
import logging

from constants.security import SECURITY


class Config(object):
    SIMULATE_LATENCY = False
    TESTING = False
    DROP_DB = False
    LOAD_BASELINE_DATA = False
    WTF_CSRF_ENABLED = True
    SECRET_KEY = "dev"
    HOST = ""

    REDIS_HOST = "localhost"
    REDIS_PORT = 6379
    REDIS_DB = 0
    
    MONGO_DB_COLLECTION = "flocademy_testing"
    MONGO_TIMEOUT = 3

    USE_SENTRY = False
    SENTRY_LOGGING_LEVEL = logging.ERROR

    RECAPTCHA_PUBLIC_KEY = ""
    RECAPTCHA_PRIVATE_KEY = ""

    USE_RATE_LIMITS = False
    LOGGING_LEVEL = logging.INFO


class MigrationConfig(Config):
    DROP_DB = False


class AnalysisConfig(Config):
    DROP_DB = False


class ProductionSettings(Config):
    SECRET_KEY = SECURITY.SECRET_KEY
    HOST = "0.0.0.0"
    REDIS_DB = 2
    TESTING = False
    
    MONGO_DB_COLLECTION = "flocademy_production"

    USE_SENTRY = True
    SENTRY_LOGGING_LEVEL = logging.WARNING

    RECAPTCHA_PUBLIC_KEY = SECURITY.RECAPTCHA_PUBLIC_KEY
    RECAPTCHA_PRIVATE_KEY = SECURITY.RECAPTCHA_PRIVATE_KEY

    USE_RATE_LIMITS = True


class DevelopmentSettings(Config):
    # Random secret key to invalidate all sessions from previous runs
    SECRET_KEY = os.urandom(32)

    TESTING = True
    SIMULATE_LATENCY = True
    REDIS_DB = 1
    
    MONGO_DB_COLLECTION = "flocademy_development"

    LOGGING_LEVEL = logging.DEBUG

    USE_RATE_LIMITS = True

    RECAPTCHA_PUBLIC_KEY = SECURITY.RECAPTCHA_PUBLIC_KEY
    RECAPTCHA_PRIVATE_KEY = SECURITY.RECAPTCHA_PRIVATE_KEY

    HOST = "0.0.0.0"


class TestingSettings(Config):
    # Random secret key to invalidate all sessions from previous runs
    SECRET_KEY = os.urandom(32)

    TESTING = True
    HEADLESS_BROWSER_TESTS = False
    DROP_DB = True
    
    MONGO_DB_COLLECTION = "flocademy_testing"

    WTF_CSRF_ENABLED = False

    # No log output during tests.
    LOGGING_LEVEL = logging.CRITICAL + 10
    
    RECAPTCHA_PUBLIC_KEY = "mock public key"
    RECAPTCHA_PRIVATE_KEY = "using this test key will bypass sending the request to Google, and always succeed."
