from tests.constants import MOCK_PASSWORD

USER_LIST_NAMES = ("in_progress", "completed", "favorites", "hidden")


class REGEX:
    # https://stackoverflow.com/questions/12018245/regular-expression-to-validate-username
    USERNAME = "(?=.{3,64}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])"
    EMAIL = r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"
    PASSWORD = "^.{8,1000}$"


class USER_LISTS:
    IN_PROGRESS = USER_LIST_NAMES[0]
    COMPLETED = USER_LIST_NAMES[1]
    FAVORITES = USER_LIST_NAMES[2]
    HIDDEN = USER_LIST_NAMES[3]

TEST_USER = ("zulban", MOCK_PASSWORD, "some_test_email_for_zulban@gmail.com")


class USER_SETTINGS():

    "Use these strings to make user settings db requests."
    "They're also used in constructing the redis keys."
    TAGS_IN_FEED = "tags_in_feed"
    CHILDREN_IN_FEED = "children_in_feed"
DEFAULT_USER_SETTINGS = {USER_SETTINGS.TAGS_IN_FEED: False,
                         USER_SETTINGS.CHILDREN_IN_FEED: False}


class PROMPTS():

    "This enum is a list of prompts. For example, the welcome prompt which tells you to answer questions and you can click 'okay'."
    WELCOME = "welcome"
    EXPLORE = "explore"
    STARS = "stars"
PROMPT_LIST = [getattr(PROMPTS, i) for i in dir(PROMPTS) if i.upper() == i]
EXPLORE_PROMPT_QUESTION_THRESHOLD = 6
STARS_PROMPT_QUESTION_THRESHOLD = 12
