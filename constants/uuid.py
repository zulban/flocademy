
class UUID():
    MIRROR_SCHEMA = "ace61d9339f2440e8c9b3e82c11ba63a"
    VIDEO_LESSON_SCHEMA = "890b8eb411d64e16abef931b45fad4bc"
    TAG_SCHEMA = "b525d0c74f754a88a8af4fffa954795e"
    FLO_META_SCHEMA = "841ae5f2453c41deb381717667397e8c"  # the most abstract, high level schema of them all!

    LESSON = "c9cf1055715c48868459cd5c376663ca"
    REFERENCE_WITH_PARENT_FLO = "eecc0ce4ab49408cafc5ddc0843c7927"  # mirror to the green series 1-3 video, which has a video lesson plan FLO referencing it
    UNIQUE_DESIRES = "64ae032db1564d038b33b2a4507e0ac3"
    REFERENCE_WITH_URLS = "c5d6b01f91e74abba63e536c98330b2a"
    NOT_A_CHILD = "1bd48b9de1f44a598a7c5819590e420c"

    TAG_AUTOMATION = "1966b70f3ddc431b90324ea5bdd27766"  # automation tag
    TAG_YES_BONUS = "72cac0bf55744634b36d16306bf3f830"  # industry
    TAG_BONUS_PENALTY = "61de135d4c4d454f93e75e1f1222c5a3"  # privacy
    TAG_FULL = "1dddcbcdb2ae47f691f3e069975ff4a7"  # age
    TAG_JUST_PENALTY = "5b0203162b604ed09a1bb8f63d627777"  # programming skill
    TAG_TEACHERS = "469893912724402c98468044e216df1c"
    
    class RECOMMENDABLE():
        REFERENCE="2c0b8111dc8044a7945536f5fc26c036"
        ASSIGNMENT_WRITTEN_INSTRUCTIONS="e443430f6d1e4b43bd9711862985ee0b"
