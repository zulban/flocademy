import subprocess
import os
from constants.config import DevelopmentSettings, TestingSettings, ProductionSettings

FAKE_LOADING_TIME = 0.3
FLOCADEMY_ENV_KEY = "FLOCADEMY_ENV"


def get_version_from_git():
    "Returns the version description from 'git describe'. This is a tag plus (sometimes) a commit hash."
    "The python git library isn't happy with the nginx/flask/path configuration to find git."
    try:
        version = subprocess.check_output(("/usr/bin/git", "describe"))
        version = version.decode().strip()
    except:
        version = "unknown"
    return version
VERSION = get_version_from_git()


def get_settings_from_env(env="", verbose=True):
    settings_map = {'development': DevelopmentSettings,
                    'testing': TestingSettings,
                    'production': ProductionSettings}
    if not env:
        env = os.environ.get(FLOCADEMY_ENV_KEY)
    if not env or env not in settings_map:
        raise ValueError(
            "configure_app failed. Bad env: %s = '%s'. To fix:\n    export %s=development" %
            (FLOCADEMY_ENV_KEY, env, FLOCADEMY_ENV_KEY))
    if verbose:
        print("Getting settings for environment: '%s'" % env)
    return settings_map[env]
