
from constants.colors import *
from constants.environment import *
from constants.path import *
from constants.recommend import *
from constants.security import *
from constants.strings import *
from constants.user import *
from constants.uuid import *
from constants.config import *

"Miscellaneous constants:"
import string
JOIN_CODE_LENGTH = 6
JOIN_CODE_CHARS = string.ascii_uppercase + string.digits


class DEFAULT():
    WEIGHT = 0.4
    FEED_LENGTH = 10


class KEYS():
    ANONYMOUS = "anonymous_username"
    DESIRE_TYPE = "desire_type"
