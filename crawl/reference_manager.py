"""Resource Manager

Attempts to create a mirror FLO JSON with its metadata. Takes as input a URL or a filename containing URLs.

Usage:
  mirror_manager.py url <url>
  mirror_manager.py file <filename>

Options:
    --verbose           More console output.

    -h --help           Show this screen.
    -v --version        Show version.
"""

import pafy
import os.path
import csv
import uuid
import json
import os

CACHE = "mirror_cache.csv"
REQUIRED_KEYS = ("title", "length", "license", "description", "uuid")
VIDEO_MIRROR_SCHEMA = "ace61d9339f2440e8c9b3e82c11ba63a"


class ResourceManager():

    def __init__(self):
        self.cache = {}
        self.read_cache()

    def read_cache(self):
        if not os.path.isfile(CACHE):
            return
        with open(CACHE, "r") as f:
            data = f.read()
        self.cache = json.loads(data)
        self.repair_cache()
        print("Read %s URLs into cache." % len(self.cache))

    def repair_cache(self):
        for url in self.cache:
            for key in REQUIRED_KEYS:
                if key not in self.cache[url]:
                    self.cache[url][key] = ""

    def save_cache(self):
        self.repair_cache()
        with open(CACHE, "w") as f:
            f.write(json.dumps(self.cache))

    def write_json_from_url(self, url):
        folder = "_jsons"
        if not os.path.exists(folder):
            os.makedirs(folder)

        print("\nGetting mirror for url = '%s'" % url)
        mirror = self.get_mirror_from_url(url)
        print("Made new FLO. uuid = '%s'" % mirror["uuid"])
        filename = mirror["uuid"] + ".json"
        path = folder + os.sep + filename
        with open(path, "w") as f:
            f.write(json.dumps(mirror))
        print("Wrote to '%s'" % path)

    def write_jsons_from_urls(self, path):
        urls = self._get_urls_from_path(path)
        for i, url in enumerate(urls):
            self.write_json_from_url(url)

    def get_mirror_from_url(self, url):
        if url in self.cache:
            return self.cache[url]

        mirror = None
        if self._is_youtube_url(url):
            mirror = self._get_mirror_from_youtube(url)

        if not mirror:
            raise NotImplementedError(
                "unsupported url for get_mirror: '%s'" %
                url)

        mirror["schema_uuid"] = VIDEO_MIRROR_SCHEMA
        self.cache[url] = mirror
        self.save_cache()
        return mirror

    def _get_mirror_from_youtube(self, url):
        video = pafy.new(url)

        formats = [s.extension for s in video.streams]
        formats = list(set(formats))

        best = video.getbest()

        mirror = {"title": video.title,
                     "length": video.length,
                     "video_description": video.description,
                     "authors": [video.author],
                     "formats": formats,
                     "type": "video",
                     "video_best_resolution": best.resolution,
                     "uuid": uuid.uuid4().hex}
        return mirror

    def _is_youtube_url(self, url):
        # for http and https youtube urls
        return url.index("www.youtube.com") in (7, 8)

    def _get_urls_from_path(self, path):
        if os.path.isfile(path):
            with open(path, "r") as f:
                lines = f.readlines()
            return [i.strip() for i in lines if i.strip()]

        print("Aborted. Not a file: '%s'" % path)
        return []


def main(args):
    rm = ResourceManager()

    if args["url"]:
        url = args["<url>"]
        rm.write_json_from_url(url)
    elif args["file"]:
        path = args["<filename>"]
        rm.write_jsons_from_urls(path)

    rm.save_cache()

if __name__ == "__main__":
    args = docopt(__doc__, version="1.0")
    main(args)
