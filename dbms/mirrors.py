import json
from urllib.parse import urlparse

from constants import *
from utilities import get_embed_url_from_url, random_uuid

class DatabaseMirrors():

    """This class manages all the data relating to mirrors: urls, votes."""
    
    def get_mirrors_for_uuid(self, uuid, add_embed_urls=True):
        flo = self.get_flo_from_uuid(uuid)
        return get_mirrors_for_flo(flo,add_embed_urls=add_embed_urls)
    
    def get_mirrors_for_flo(self, flo, add_embed_urls=True):
        if not flo:
            return []        
        uuid=flo.get("_id")
        if not uuid:
            return []
        
        mirrors=[]
        "get mirrors assigned to this FLO"        
        mirrors+=list(self.mongo.mirrors.find({"mirror_for":uuid}))
        
        "get mirrors directly inside the FLO data"
        flo_mirrors=flo.get("mirrors",[])
        if flo_mirrors:
            for flo_mirror_uuid in flo_mirrors:
                mirrors+=list(self.mongo.mirrors.find({"mirror_for":flo_mirror_uuid}))
        
        "add the embed_url key to the mirrors if necessary"
        if add_embed_urls:
            for mirror in mirrors:
                url=mirror.get("url")
                embed=get_embed_url_from_url(url)
                if embed:
                    mirror["embed_url"] = embed                    
        
        return mirrors
    
    def get_urls_for_uuid(self,uuid):
        urls=self.mongo.mirrors.find({"mirror_for":uuid}).distinct("url")
        return [] if not urls else urls

    def add_mirror_for_uuid(self, uuid, url):
        vote_fields=("good","bad","spam","broken")        
        url_label=urlparse(url).netloc
        
        mirror={"_id":"mirror:"+random_uuid(),
                "mirror_for":uuid,
                "url":url,
                "votes":{i:0 for i in vote_fields},
                "url_label":url_label}
        mirror["votes"]["good"]=random.randint(1,4)
        
        self.mongo.mirrors.insert_one(mirror)