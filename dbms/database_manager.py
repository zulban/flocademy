import time
import logging
from redis import StrictRedis
import pymongo
from pymongo import MongoClient

from dbms.recommend import DatabaseRecommend
from dbms.users import DatabaseUsers
from dbms.flo import DatabaseFlo
from dbms.mirrors import DatabaseMirrors
from dbms.io import *
from dbms.search import *
from dbms.supplemental import *

from utilities import FLOValidator


class DatabaseManager(DatabaseRecommend, DatabaseUsers, DatabaseMirrors, DatabaseFlo, DatabaseIO, DatabaseSearch, DatabaseSupplemental):

    """This class unites all the functionality from the many specific dbms classes.

    It also performs high level functions that apply to the entire database."""

    def configure(self, my_configuration, app=None):
        if app:
            self.logger = app.logger
        else:
            self.logger = logging.getLogger("flocademy:no-app")
            self.logger.setLevel(my_configuration.LOGGING_LEVEL)
            
        "redis"
        self.redis = StrictRedis(
            host=my_configuration.REDIS_HOST,
            port=my_configuration.REDIS_PORT,
            db=my_configuration.REDIS_DB)
        
        "mongo"
        self.mongo_client = MongoClient(serverSelectionTimeoutMS=my_configuration.MONGO_TIMEOUT)
        self.mongo=self.mongo_client[my_configuration.MONGO_DB_COLLECTION]
        try:
            self.mongo_client.server_info()
        except pymongo.errors.ServerSelectionTimeoutError as e:
            self.mongo=None
            self.logger.error("\nMongo connection timeout on setup.")
            self.logger.error(e)

        self.flo_validator = FLOValidator(DATA_PATHS.SCHEMAS, verbose=my_configuration.TESTING)

        self.config = my_configuration
        if self.config.DROP_DB:
            self.drop_all()
        if self.config.LOAD_BASELINE_DATA:
            self.load_baseline_data()

        self.setup_thumbnails()
        self.recalculate_all_uuid_score_components()
        self.recalculate_scores()
        self.calculate_no_answers_feed()

    def drop_all(self):
        self.redis.flushdb()
        for name in self.mongo.collection_names():
            self.mongo_client.drop_database(self.config.MONGO_DB_COLLECTION)
