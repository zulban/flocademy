import hashlib
import os

from constants import *
from utilities import get_matching_paths_recursively, is_string_a_uuid, search_flo_for_children_uuids


class DatabaseSupplemental():

    """This class manages all the data used peripherally by FLOcademy like thumbnails and gravatar."""

    def get_gravatar_url_from_username(self, username):
        "Returns a partial gravatar URL. It still requires a number added to the end for width."
        email = self.get_email_from_username(username)
        if not email:
            email = username
        h = hashlib.md5(email.lower().encode()).hexdigest()
        return "https://www.gravatar.com/avatar/" + h + "?d=retro&size="

    def get_thumbnail_for_flo(self, flo, depth=0):
        "returns the filename of the thumbnail for this flo"
        
        if not flo:
            return PATH.THUMBNAIL_DEFAULT

        "first check if uuid specifically has a thumbnail"
        uuid = flo.get("_id")
        filename = self.uuid_thumbnails.get(uuid)
        if filename:
            return filename

        "next check if it's a mirror and that type is in our thumbnails."
        schema_uuid = flo.get("schema_uuid")
        if schema_uuid == UUID.MIRROR_SCHEMA:
            mirror_type = flo.get("type")
            filename = self.named_thumbnails.get(mirror_type)
            if filename:
                return filename
            else:
                self.logger.warning(
                    "get_thumbnail_for_flo got a mirror but failed to find thumbnail.\nmirror_type = '%s'" %
                    mirror_type)
                
        
        "if children, and we haven't recursed too much, use the thumbnail of its first child"
        children=search_flo_for_children_uuids(flo)
        if children and depth<2:
            child=self.get_flo_from_uuid(children[0])
            return self.get_thumbnail_for_flo(flo,depth=depth+1)
        
        "generic case - use its schema thumbnail"
        filename = self.uuid_thumbnails.get(schema_uuid)
        if filename:
            return filename
        
        return PATH.THUMBNAIL_DEFAULT

    def setup_thumbnails(self):
        self.uuid_thumbnails={}
        self.named_thumbnails={}
        extension=".png"
        paths = get_matching_paths_recursively(PATH.THUMBNAILS, extension)
        for path in paths:
            filename = path.split(os.sep)[-1]
            label = filename.split(extension)[0]
            if is_string_a_uuid(label):
                self.uuid_thumbnails[label]=filename
            else:
                self.named_thumbnails[label]=filename

    def get_template_path_from_flo(self, flo):        
        try:
            self.template_filenames
        except:
            self.template_filenames={}
            folder=PATH.HTML_TEMPLATES+PATH.FLO_HTML_TEMPLATES
            for filename in os.listdir(folder):
                uuid=filename.split(".html")[0].split(os.sep)[-1]
                self.template_filenames[uuid]=PATH.FLO_HTML_TEMPLATES+filename
        
        default = PATH.FLO_HTML_TEMPLATES + "default.html"
        schema_uuid = flo.get("schema_uuid")
        return self.template_filenames.get(schema_uuid,default)
