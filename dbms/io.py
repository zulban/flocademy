from utilities import *
import time


class DatabaseIO():

    """This class manages the input and output of files for the database: JSON and database CSV scripts.

    CSV script and JSON are easily viewable and editable, and therefore good for prototyping and debugging."""

    def load_baseline_data(self):
        self.update(DATA_PATHS.BASELINE)

    def load_baseline_tag_votes(self):
        self.execute_csv_script(DATA_PATHS.TAG_VOTES)

    def load_baseline_mirrors(self):
        self.execute_csv_script(DATA_PATHS.MIRROR_URLS)

    def load_debug_users(self):
        self.execute_csv_script(DATA_PATHS.TEST_USERS)

    def load_debug_groups(self):
        self.execute_csv_script(DATA_PATHS.TEST_GROUPS)

    def load_baseline_flos(self, uuids=None):
        "Loads all stable FLOs, unless optional uuids list is given, to only load those."
        paths = get_matching_paths_recursively(DATA_PATHS.BASELINE, ".json")
        if uuids:
            paths = [p for p in paths if any([uuid in p for uuid in uuids])]
        for path in paths:
            flo = get_flo_from_path(path)
            self.save_flo(flo)

    def update(self, file_or_folder, verbose=False):
        "Updates the database with all JSON and database CSV scripts found in this folder."
        if os.path.isdir(file_or_folder):
            folder = file_or_folder
            jsons = get_matching_paths_recursively(folder, ".json")
            csvs = get_matching_paths_recursively(folder, ".csv")
            for path in jsons + csvs:
                self._update_with_file(path, verbose=verbose)
        else:
            self._update_with_file(file_or_folder, verbose=verbose)

    def _update_with_file(self, path, verbose=False):
        if verbose:
            print("update with file: '%s'" % path)
        if path.endswith(".json"):
            flo = get_flo_from_path(path)
            self.save_flo(flo)
        elif path.endswith(".csv"):
            self.execute_csv_script(path)
        else:
            print("_update_with_file failed for file: '%s'" % path)

    def write_flo_json(self, flo):
        "Writes this FLO as a JSON file to the 'web generated' path."
        path = PATH.WEB_GENERATED_DATA + os.sep + uuid + ".json"
        if not os.path.isdir(PATH.WEB_GENERATED_DATA):
            os.mkdir(PATH.WEB_GENERATED_DATA)
        write_pretty_json(path, flo)

    def execute_csv_script(self, path):            
        if not os.path.isfile(path):
            self.logger.error(
                "execute_database_script_csv failed. Not a file = '%s'" %
                path)
            return
        with open(path, "r") as f:
            lines = [i.strip()
                     for i in f.readlines() if i.strip() and not i.startswith("#")]
        for line in lines:
            try:
                self.execute_script_line(line)
            except ValueError as e:
                self.show_csv_error(e,line)
    
    def show_csv_error(self, e, line):
        "show the error and line, unless many errors have already occurred"
        try:
            self.csv_script_fail_count+=1
        except:
            self.csv_script_fail_count=1
        
        if self.csv_script_fail_count==1:
            print_yellow("WARNING: database script got a bad line.")
            print_yellow("    '%s'" % line)
            print(e)
        elif self.csv_script_fail_count==2:
            print_yellow("WARNING: database script got multiple bad lines.")

    def execute_script_line(self, line):
        cmd, *args = line.split("\t")
        if cmd == "set_mirror_url":
            uuid, url = args
            self.add_mirror_for_uuid(uuid, url)
            return
        elif cmd == "set_user":
            username, password = args[:2]
            email = "" if len(args) < 3 else args[2]
            admin_level = 0 if len(args) < 4 else args[3]
            self.create_user(
                username,
                password,
                email,
                admin_level=admin_level)
            return
        elif cmd == "set_user_answer":
            username, tag_uuid, index, _, _ = args
            answer = (tag_uuid, int(index), DEFAULT.WEIGHT)
            self.set_answer(
                username,
                answer,
                expire_time=0,
                adjust_scores=False,
                recalculate_scores=False)
            return
        elif cmd == "set_tag_votes":
            flo_uuid, tag_uuid, up, down, *_ = args

            "verify that flo and tag exist"
            if self.config.TESTING:
                flo = self.get_flo_from_uuid(flo_uuid)
                tag = self.get_flo_from_uuid(tag_uuid)
                if not flo:
                    print_yellow(
                        "WARNING: set_tag_votes for non-existing FLO. line = '%s'" %
                        line)
                if not tag:
                    print_yellow(
                        "WARNING: set_tag_votes for non-existing tag. line = '%s'" %
                        line)

            up, down = int(up), int(down)
            self.set_tag_vote(flo_uuid, tag_uuid, TAG_VOTES.UP, up)
            self.set_tag_vote(flo_uuid, tag_uuid, TAG_VOTES.DOWN, down)
            return
        elif cmd == "set_mock_flos":
            uuid, count = args
            for i in range(int(count)):
                flo = {"uuid": uuid %
                       (i + 1), "schema_uuid": UUID.VIDEO_LESSON_SCHEMA}
                self.set_flo(flo)
            return
        elif cmd == "set_mock_tag":
            uuid, desire_type = args
            if desire_type not in DESIRE_TYPES_LIST:
                raise ValueError
            flo = {
                "_id": uuid, "schema_uuid":
                    UUID.TAG_SCHEMA, "desire_type": desire_type,
                 "answers": [{"text": "abc", "value": "0"}, {"text": "abcdef", "value": "1"}]}
            self.set_flo(flo)
            return
        elif cmd == "set_user_account":
            username, display = args[:2]
            email = "" if len(args) < 3 else args[2]
            self.set_user_email(username, email)
            self.set_user_display_name(username, display)
            return
        elif cmd == "create_group":
            username, group_name, color = args
            self.create_group(username, group_name, color)
            return
        elif cmd == "join_group":
            username, group_name, owner = args[:3]
            permissions = "" if len(args) < 4 else args[3]
            group_ids = self.get_group_ids_from_username(owner)
            for group_id in group_ids:
                if group_name == self.get_group_name(group_id):
                    self.join_group(username, group_id, permissions)
                    return
            raise ValueError(
                "Failed to find a group '%s' with owner '%s'." %
                (group_name, owner))
        self.logger.error(
            "execute_script_line got a weird command = '%s'" %
            cmd)
