from utilities import *
import re

class DatabaseSearch():

    """This class handles text search functionality for the front-end."""

    def search(self, username, query):
        """returns a list of search results of types:

        {"type" : "plank_data",
        "plank_data" : pd }

        username is an argument because plank_data includes user plank buttons being clicked
        """
        if not query:
            return []

        query = query.strip()
        if is_string_a_url(query):
            return self.search_with_url(username, query)
        if is_string_a_uuid(query):
            return self.search_with_uuid(username, query)
        return self.search_with_string(username, query)

    def search_with_string(self, username, query):
        self.logger.debug("search_with_string '%s'" % query)
        query = query.lower()
        uuids = self.get_uuids_containing_text(query)
        return [{"type": "plank_data",
                 "plank_data": self.get_plank_from_username_uuid(username, uuid)} for uuid in uuids]
    
    def get_uuids_containing_text(self, text):
        text=text.lower().encode()
        
        uuids=[]
        for uuid in self.get_uuids():
            key="search_content:"+uuid
            content=self.redis.get(key)
            if text in content:
                uuids.append(uuid)
        return uuids

    def search_with_uuid(self, username, query):
        self.logger.debug("search_with_uuid '%s'" % query)
        results = self.search_with_string(username, query)

        # if exists, put flo with that uuid as first result
        index = 0
        for i, r in enumerate(results):
            if r["plank_data"]["uuid"] == query:
                index = i
                break
        if index:
            r = results.pop(index)
            results.insert(0, r)

        return results

    def search_with_url(self, username, url):
        self.logger.debug("search_with_url '%s'" % url)
        uuids = set()

        trim = url
        if "://" in trim:
            trim = trim.split("://")[1]
        if trim.startswith("www."):
            trim = trim[4:]
        while trim and trim.endswith("/"):
            trim = trim[:-1]
            
        mirrors=self.mongo.mirrors.find()
        for mirror in mirrors:
            uuid=mirror.get("mirror_for")
            url=mirror.get("url","")
            if trim in url:
                uuids.add(uuid)

        "if we found no results but have parameters, try again without parameters."
        if not uuids and "?" in trim:
            supertrim = trim.split("?")[0]
            return self.search_with_url(username, supertrim)
        
        return [{"type": "plank_data",
                 "plank_data": self.get_plank_from_username_uuid(username, uuid)} for uuid in uuids]
