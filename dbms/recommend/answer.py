import time

from constants import *

from utilities import is_string_a_uuid


class DatabaseAnswer():

    """This class handles all user answers for tags - their indexes and weights."""

    def get_answer_index_from_username_uuid(self, username, uuid):
        index = self.redis.hget("answer_indexes:" + username + ":hash", uuid)
        try:
            return int(index.decode())
        except:
            return NO_DESIRE_INDEX

    def get_user_weight_from_username_uuid(self, username, uuid):
        weight = self.redis.hget("user_weights:" + username + ":hash", uuid)
        if not weight:
            return DEFAULT.WEIGHT
        try:
            return float(weight.decode())
        except (ValueError, AttributeError):
            return DEFAULT.WEIGHT

    def get_user_weights(self, username):
        weights = self.redis.hgetall("user_weights:" + username + ":hash")
        return {key.decode(): weights[key] for key in weights}

    def set_tag_weight(self, uuid, weight):
        if not self.is_uuid_a_tag(uuid):
            self.logger.warning(
                "set_tag_weight must be applied to a tag uuid. uuid = '%s'" %
                uuid)
            return
        key = "tag_weight:" + uuid
        self.redis.set(key, weight)

    def get_tag_weight(self, uuid):
        key = "tag_weight:" + uuid
        weight = self.redis.get(key)
        if not weight:
            return DEFAULT.WEIGHT
        return float(weight.decode())

    def get_tag_weights(self):
        weights = {}
        for uuid in self.get_tag_uuids():
            weight = self.get_tag_weight(uuid)
            if not weight:
                weight = DEFAULT.WEIGHT
            weights[uuid] = weight
        return weights

    def set_answer(self, username, answer, expire_time,
                   adjust_scores=True, recalculate_scores=False):
        """Set an answer for this username. Optionally, rescore FLOs.
        If adjust_scores is true, re-use the previous score and adjust all FLO scores based on the new answer.
        If recalculate_scores is true, totally recalculate all scores for all FLOs for all tags for this user."""

        if not expire_time and self.is_username_anonymous(username):
            raise ValueError("set_answer must expire if given an anonymous user: '%s'" %
                             username)
        tag_uuid, index, weight = answer

        if adjust_scores and not recalculate_scores:
            old_desire = self.get_desire_from_username_and_tag(
                username, tag_uuid)

        key1 = "answer_indexes:" + username + ":hash"
        key2 = "answered_uuids:" + username + ":set"
        key3 = "answered_uuids:" + username + ":list"
        self.redis.hset(key1, tag_uuid, index)
        self.redis.sadd(key2, tag_uuid)
        self.redis.lpush(key3, tag_uuid)

        if weight < 0 or weight > 1:
            self.logger.warning(
                "set_answer got a weight outside valid values. Aborted.")
            return
        
        self.mongo.tag_answers.update({"_id":username},
            {"$set":{tag_uuid:{"i":index,"w":weight}}},upsert=True)

        "set influence from stars weight"
        if self.is_username_taken(username):
            key = "user_weights:" + username + ":hash"
            self.redis.hset(key, tag_uuid, weight)

        if recalculate_scores:
            self.recalculate_scores(username)
        elif adjust_scores:
            self.update_scores_for_username(username, answer, old_desire)
        
        if expire_time:
            try:
                expire_time = int(expire_time)
                self.redis.expire(key1, expire_time)
                self.redis.expire(key2, expire_time)
                self.redis.expire(key3, expire_time)
            except ValueError:
                pass

    def get_answered_uuids_from_username(self, username):
        "returns a list of all uuids that this user has answered."
        key = "answered_uuids:" + username + ":set"
        answered = self.redis.smembers(key)
        return [i.decode() for i in answered]

    def get_answer_count_from_username(self, username):
        "Returns the number of questions this username has answered."
        key = "answered_uuids:" + username + ":set"
        return self.redis.scard(key)

    def get_value_from_tag_and_index(self, uuid, index):
        flo = self.get_flo_from_uuid(uuid)
        if not flo:
            return NO_DESIRE_VALUE
        answers = flo.get("answers")
        if not answers:
            return NO_DESIRE_VALUE
        try:
            answer = answers[index]
        except (IndexError, TypeError):
            self.logger.warning(
                "get_value_from_tag_and_index got a weird index = '%s' uuid = '%s'" %
                (index, uuid))
            return NO_DESIRE_VALUE
        return float(answer.get("value", NO_DESIRE_VALUE))

    def set_answer_with_user(self, user, answer, adjust_scores=True, recalculate_scores=False):
        tag_uuid, index, weight = answer

        if not type(index) is int:
            self.logger.warning(
                "set_answer_with_user got an invalid index = '%s'. Aborted." %
                index)
            return

        if not is_string_a_uuid(tag_uuid):
            self.logger.warning(
                "set_answer_with_user didn't get a uuid. tag_uuid = '%s'. Aborted." %
                tag_uuid)
            return

        if self.config.SIMULATE_LATENCY:
            time.sleep(FAKE_LOADING_TIME)

        "upon creation, anonymous users may not have scope to see db. So set a mirror before doing anything else."
        if user.is_anonymous:
            user.db = self

        # 72 hour expiry for anonymous answers
        expire_time = 259200 if user.is_anonymous else 0

        if not self.is_uuid_a_tag(tag_uuid):
            self.logger.error("send_answer got a non-tag uuid.")
            return 400

        self.set_answer(user.username, answer, expire_time,
                        adjust_scores=adjust_scores, recalculate_scores=recalculate_scores)
        return 200
