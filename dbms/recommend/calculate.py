import math

from constants import *


class DatabaseCalculate():

    """This class performs all calculations involved in recommendations and ordering for registered users."""

    def get_tag_scopes_for_uuid(self, uuid):
        "returns a dictionary {tag_uuid:scope} for all tag scopes for this uuid."
        return {tag_uuid: self.calculate_tag_scope_for_uuid(uuid, tag_uuid)
                for tag_uuid in self.get_voted_tags_from_uuid(uuid)}

    def get_username_score_components(self, username, uuid, recalculate=False):
        if recalculate:
            self.recalculate_score(username, uuid)

        score = self.get_uuid_score_for_username(uuid, username)
        key = "grade:%s:%s" % (username, uuid)
        grade = self.redis.get(key)
        key = "best_grade:%s:%s" % (username, uuid)
        best_grade = self.redis.get(key)

        try:
            score = float(score.decode())
        except:
            score = 0
        try:
            grade = float(grade.decode())
        except:
            grade = 0
        try:
            best_grade = float(best_grade.decode())
        except:
            best_grade = 0

        return score, grade, best_grade

    def get_uuid_popularity(self, uuid, recalculate=False):
        key = "popularity:" + uuid
        popularity = self.redis.get(key)
        if popularity is None or recalculate:
            total_votes = self.calculate_tag_vote_count(uuid)
            popularity = math.log(total_votes + 10)
            self.redis.set(key, popularity)
            return popularity
        try:
            popularity = float(popularity.decode())
        except:
            popularity = 0
        return popularity

    def get_uuid_score_components(self, uuid, tag_uuid, recalculate=False):
        key1 = "tag_power:%s:%s" % (uuid, tag_uuid)
        key2 = "tag_scope:%s:%s" % (uuid, tag_uuid)

        if recalculate or not self.redis.exists(key1) or not self.redis.exists(key2):
            self.recalculate_uuid_score_components(uuid, tag_uuid)

        popularity = self.get_uuid_popularity(uuid, recalculate=recalculate)
        tag_power = self.redis.get(key1)
        tag_scope = self.redis.get(key2)
        try:
            tag_power = float(tag_power.decode())
        except:
            tag_power = 0
        try:
            tag_scope = float(tag_scope.decode())
        except:
            tag_scope = 0

        return popularity, tag_power, tag_scope

    def set_uuid_score_components(
            self, uuid, tag_uuid, popularity, tag_power, tag_scope):
        key = "popularity:" + uuid
        self.redis.set(key, popularity)
        key = "tag_power:%s:%s" % (uuid, tag_uuid)
        self.redis.set(key, tag_power)
        key = "tag_scope:%s:%s" % (uuid, tag_uuid)
        self.redis.set(key, tag_scope)

    def get_number_of_tags(self):
        return self.mongo.flos.count({"is_tag":{"$eq":True}})

    def set_uuid_score_for_username(
            self, uuid, score, username, grade=0, best_grade=0):
        "Set the score for this UUID which is used to compare recommendations for this user."
        "If grade and best_grade are not set, the optimized set_answer without full recalculation may fail."

        score_components = score, grade, best_grade
        self.set_username_score_components(uuid, username, score_components)

    def set_username_score_components(self, uuid, username, score_components):
        score, grade, best_grade = score_components

        key = "uuid_recommendations:" + username
        self.redis.zadd(key, score, uuid)
        "Set the score component cache."
        key = "grade:%s:%s" % (username, uuid)
        self.redis.set(key, grade)
        key = "best_grade:%s:%s" % (username, uuid)
        self.redis.set(key, best_grade)

    def get_uuid_score_for_username(self, uuid, username):
        key = "uuid_recommendations:" + username
        score = self.redis.zscore(key, uuid)
        if not score:
            return 0
        return score

    def get_mark(self, tag_scope, desire):
        "return the mark of a flo with this tag_scope, according to this desire"
        "returns a number within [-weight, weight]"
        if not desire:
            return NO_DESIRE_MARK

        value, weight, desire_type = desire

        def verify_value(x):
            if type(x) not in (float, int):
                raise ValueError("bad type: '%s'" % type(x))
            if x < 0 or x > 1:
                raise ValueError("bad value range: '%s'" % x)
        if value == NO_DESIRE_INDEX:
            return NO_DESIRE_MARK
        verify_value(value)
        verify_value(tag_scope)

        if desire_type not in DESIRE_TYPES_LIST:
            self.logger.error(
                "get_mark got weird desire_type = '%s' desire = '%s'" %
                (desire_type,desire))
            return NO_DESIRE_MARK

        if desire_type == DESIRE_TYPES.YES_BONUS:
            unweighted_mark = value * tag_scope
        elif desire_type == DESIRE_TYPES.AGREE_BONUS:
            unweighted_mark = 1 - abs(value - tag_scope)
        elif desire_type == DESIRE_TYPES.BONUS_PENALTY:
            unweighted_mark = (1 - abs(value - tag_scope) * 2) * tag_scope
        elif desire_type == DESIRE_TYPES.FULL:
            unweighted_mark = (1 - abs(value - tag_scope)) * 2 - 1
        elif desire_type == DESIRE_TYPES.JUST_PENALTY:
            unweighted_mark = (value - 1) * tag_scope

        if unweighted_mark < -1 or unweighted_mark > 1:
            self.logger.warning(
                "get_mark calculated a bad unweighted_mark. Clamping the value.")
            unweighted_mark = clamp(unweighted_mark, -1, 1)
        return unweighted_mark * weight

    def get_best_mark(self, tag_scope, desire):
        """returns a number from 0 to 1, representing the best mark this
        desire_type can produce given this tag_scope"""
        if not desire:
            return 0
        _, weight, desire_type = desire
        if desire_type not in DESIRE_TYPES_LIST:
            self.logger.warning(
                "get_best_mark failed. DESIRE_TYPES_LIST does not contain desire_type = '%s'. Returning 0." %
                desire_type)
            return 0
        if desire_type != DESIRE_TYPES.JUST_PENALTY:
            return tag_scope * weight
        return 0

    def update_score_for_username(
            self, username, uuid, answer, old_desire, new_desire):
        tag_uuid, index, weight = answer
        old_score_components = self.get_username_score_components(
            username, uuid, recalculate=False)
        score_components = self.calculate_updated_score_components(
            old_score_components,
            username,
            uuid,
            answer,
            old_desire,
            new_desire)
        self.set_username_score_components(uuid, username, score_components)

    def calculate_updated_score_components(
            self, old_score_components, username, uuid, answer, old_desire, new_desire):
        tag_uuid, index, weight = answer
        old_score, old_grade, old_best_grade = old_score_components
        popularity, tag_power, tag_scope = self.get_uuid_score_components(
            uuid, tag_uuid, recalculate=False)
        new_best_mark = self.get_best_mark(tag_scope, new_desire) * tag_power
        new_mark = self.get_mark(tag_scope, new_desire) * tag_power
        old_best_mark = self.get_best_mark(tag_scope, old_desire) * tag_power
        old_mark = self.get_mark(tag_scope, old_desire) * tag_power

        delta_best_mark = new_best_mark - old_best_mark

        delta_mark = new_mark - old_mark
        new_grade = old_grade + delta_mark
        new_best_grade = old_best_grade + delta_best_mark
        if new_best_grade:
            new_score = (new_grade / new_best_grade) * popularity
        else:
            new_score = 0

        return (new_score, new_grade, new_best_grade)

    def update_scores_for_username(self, username, answer, old_desire):
        "Use a new answer to adjust the score for all UUIDs, instead of totally recalculating with all tags on all UUIDs."
        tag_uuid, index, weight = answer
        new_desire = self.get_desire_from_username_and_tag(username, tag_uuid)
        for uuid in self.get_recommendable_uuids():
            self.update_score_for_username(
                username,
                uuid,
                answer,
                old_desire,
                new_desire)

    def recalculate_uuid_score_components(self, uuid, tag_uuid):
        """score_components are numbers used to calculate the recommendation score.
        These uuid_score_components apply to UUIDs regardless of the user, and only change when up/down votes change on that UUID."""
        popularity = self.get_uuid_popularity(uuid, recalculate=True)
        tag_scope = self.calculate_tag_scope_for_uuid(uuid, tag_uuid)
        tag_power = self.calculate_tag_power(uuid, tag_uuid)
        self.set_uuid_score_components(
            uuid,
            tag_uuid,
            popularity,
            tag_power,
            tag_scope)

    def recalculate_all_uuid_score_components(self):
        for uuid in self.get_recommendable_uuids():
            for tag_uuid in self.get_tag_uuids():
                self.recalculate_uuid_score_components(uuid, tag_uuid)

    def calculate_score_for_uuid_with_desires(self, uuid, desires):
        "returns a number that can be used to compare how recommended uuids are."
        if not desires:
            return 0
        if not self.can_recommend_this_uuid(uuid):
            return HIDDEN_FLO_SCORE

        popularity = self.get_uuid_popularity(uuid)

        voted_tags = self.get_voted_tags_from_uuid(uuid)
        grade, best_grade = 0, 0
        for desire_uuid in desires:
            "calculation is only necessary if uuid has votes on this tag"
            if desire_uuid not in voted_tags:
                continue

            desire = desires[desire_uuid]
            tag_scope = self.calculate_tag_scope_for_uuid(uuid, desire_uuid)
            tag_power = self.calculate_tag_power(uuid, desire_uuid)

            "add to highest possible score counter"
            best_grade += self.get_best_mark(tag_scope, desire) * tag_power
            grade += self.get_mark(tag_scope, desire) * tag_power

        if not best_grade:
            return 0

        return (grade / best_grade) * popularity

    def calculate_tag_scope_for_uuid(self, uuid, tag_uuid):
        "returns a number from 0 to 1 representing how much this tag applies to this mirror."
        up = self.get_tag_vote(uuid, tag_uuid, TAG_VOTES.UP)
        down = self.get_tag_vote(uuid, tag_uuid, TAG_VOTES.DOWN)
        total = up + down
        if not total:
            return 0
        return up / total

    def calculate_tag_power(self, uuid, tag_uuid):
        """Returns a number from 0 to 1 representing what share of the total points
        this tag_uuid should be responsible for, in scoring this uuid.
        If the tag_uuid has 90% of all votes for this uuid, the number is close to 1."""

        total = self.calculate_tag_vote_count(uuid)
        if not total:
            return 0
        up = self.get_tag_vote(uuid, tag_uuid, TAG_VOTES.UP)
        down = self.get_tag_vote(uuid, tag_uuid, TAG_VOTES.DOWN)
        return (up + down) / total

    def recalculate_score(self, username, uuid, desires=None):
        "totally recalculate score for this FLO, using every answered tag that applies to this FLO."
        "desires is optional value, gets desires if it's set to None."
        if not desires:
            desires = self.get_desires_from_username(username)

        if self.is_uuid_on_user_list(uuid, username, USER_LISTS.HIDDEN):
            "score is a negative value if it is on the hidden list"
            score = HIDDEN_FLO_SCORE
        else:
            score = self.calculate_score_for_uuid_with_desires(uuid, desires)
        self.set_uuid_score_for_username(uuid, score, username)

    def recalculate_scores(self, username=""):
        if username:
            desires = self.get_desires_from_username(username)
            uuids = self.get_recommendable_uuids()
            for uuid in uuids:
                self.recalculate_score(username, uuid, desires=desires)
        else:
            for username in self.get_usernames():
                self.recalculate_scores(username)
