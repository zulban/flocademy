import time

from constants import *

from utilities import reorder_prioritizing_uniques


class DatabaseSuggest():

    def get_top_scoring_uuids_for_username(self, username, number, offset=0):
        "offset can be used to skip some amount of top scoring results"
        if offset and offset >= self.get_recommendable_count():
            return []

        key = "uuid_recommendations:" + username
        stop_index = offset + number - 1
        uuids = self.redis.zrevrange(key, offset, stop_index)

        if not uuids and self.get_answer_count_from_username(username) == 0:
            return self.get_recommended_uuids_for_no_answers(number, offset=offset)

        return [i.decode() for i in uuids]

    def get_next_question_uuid(self, username):
        "returns an unanswered UUID that is the best question to ask this user next."
        if self.config.SIMULATE_LATENCY:
            time.sleep(FAKE_LOADING_TIME)

        answered = set(self.get_answered_uuids_from_username(username))

        if self.get_number_of_tags() == len(answered):
            return None

        "count votes for each tag found on the first several recommended FLOs"
        tag_votes = {tag_uuid: 0 for tag_uuid in self.get_tag_uuids()}
        uuids = self.get_top_scoring_uuids_for_username(
            username, NEXT_QUESTION_FEED_DEPTH)
        for uuid in uuids:
            for tag_uuid in self.get_voted_tags_from_uuid(uuid):
                if tag_uuid in answered:
                    continue
                up = self.get_tag_vote(uuid, tag_uuid, TAG_VOTES.UP)
                down = self.get_tag_vote(uuid, tag_uuid, TAG_VOTES.DOWN)
                tag_votes[tag_uuid] += up + down

        "return the tag_uuid with the most votes"
        best_value = max(tag_votes.values())
        if best_value:
            for tag_uuid in tag_votes:
                if tag_votes[tag_uuid] == best_value:
                    return tag_uuid

        "if best_value is zero, choose a random question we haven't answered yet."
        unanswered = set(self.get_tag_uuids()).difference(answered)
        if unanswered:
            "return a random UUID, first item in set to avoid tuple conversion random.choice"
            self.logger.debug(
                "get_next_question_uuid returning a best_value=0 unanswered UUID.")
            for uuid in unanswered:
                return uuid

        "if there are no unanswered questions, return None"
        self.logger.debug("get_next_question_uuid returning None.")
        return None

    def can_recommend_this_flo(self, flo):
        "This returns True if FLOcademy includes this FLO in its recommendation system."
        if not flo:
            return False
        uuid = flo.get("_id")
        schema_uuid = flo.get("schema_uuid")
        if not flo.get("is_recommendable",True):
            return False
        if not schema_uuid:
            return False
        if schema_uuid in (UUID.TAG_SCHEMA,UUID.FLO_META_SCHEMA):
            return False
        return True

    def can_recommend_this_uuid(self, uuid):
        flo = self.get_flo_from_uuid(uuid)
        return self.can_recommend_this_flo(flo)

    def get_feed_planks(
            self, username, requested_feed_length, offset=0, schema_shuffle=True):
        "returns the requested number of planks for a feed for this user."

        "schema_shuffle guarantees that the first n items in a feed have different schemas, given a feed with n different schemas."
        if self.config.SIMULATE_LATENCY:
            time.sleep(FAKE_LOADING_TIME)

        if not type(requested_feed_length) is int:
            self.logger.warning(
                "get_feed_planks failed. bad requested_feed_length = '%s'" %
                requested_feed_length)
            return []

        uuids = self.get_top_scoring_uuids_for_username(
            username, requested_feed_length, offset=offset)
        feed = [self.get_plank_from_username_uuid(username,uuid) for uuid in uuids]

        if schema_shuffle:
            feed_and_schemas = [(item, item.get("schema_uuid","")) for item in feed]
            feed = reorder_prioritizing_uniques(feed_and_schemas)
        feed = [i for i in feed if i]
        return feed

    def get_most_relevant_tags(self, uuid):
        "Returns a list of tag uuids that are most significant, popular, or relevant for this uuid."

        "the 'relevance' of a tag is simply the sum of up/down votes for this uuid."
        scores = []
        for tag_uuid in self.get_voted_tags_from_uuid(uuid):
            score = self.get_tag_vote(uuid, tag_uuid, TAG_VOTES.UP)
            score += self.get_tag_vote(uuid, tag_uuid, TAG_VOTES.DOWN)
            scores.append((score, tag_uuid))

        scores = reversed(sorted(scores, key=lambda x: x[0]))
        return [item[1] for item in scores]
