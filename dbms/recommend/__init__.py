from dbms.recommend.anonymous import *
from dbms.recommend.answer import *
from dbms.recommend.calculate import *
from dbms.recommend.desire import *
from dbms.recommend.suggest import *
from dbms.recommend.vote import *


class DatabaseRecommend(DatabaseAnonymous, DatabaseAnswer, DatabaseCalculate, DatabaseDesire, DatabaseSuggest, DatabaseVote):
    pass
