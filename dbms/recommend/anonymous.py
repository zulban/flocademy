
class DatabaseAnonymous():

    """This class handles calculations and recommendations for anonymous users, or users with no answers yet."""

    def get_recommended_uuids_for_no_answers(self, number, offset=0):
        "Returns a decent feed when no answers have yet been given."
        key = "no_answers_feed"
        stop_index = offset + number - 1
        uuids = self.redis.zrevrange(key, offset, stop_index)
        if not uuids:
            self.calculate_no_answers_feed()
            uuids = self.redis.zrevrange(key, 0, number - 1)
        return [i.decode() for i in uuids]

    def set_no_answer_score_for_uuid(self, score, uuid):
        "This sets individual scores for the 'no answer feed' FLOs."
        key = "no_answers_feed"
        self.redis.zadd(key, score, uuid)

    def calculate_no_answers_feed(self):
        "Builds the sorted list of UUIDs to recommend by default, when no answers have yet been given."
        "FLOs with the most votes are sorted to the top."
        for uuid in self.get_recommendable_uuids():
            total = self.get_total_votes_on_uuid(uuid)
            self.set_no_answer_score_for_uuid(total, uuid)
