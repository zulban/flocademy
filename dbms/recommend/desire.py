from constants import *


class DatabaseDesire():

    def get_desire_type_from_tag_uuid(self, uuid):
        result = self.mongo.flos.find_one({"_id":uuid})
        return None if not result else result.get(KEYS.DESIRE_TYPE,None)

    def get_desires_from_username(self, username):
        "desires={uuid:(value,weight,desire_type)}"
        desires = {}
        for uuid in self.get_answered_uuids_from_username(username):
            desire = self.get_desire_from_username_and_tag(username, uuid)
            if desire:
                desires[uuid] = desire
        return desires

    def get_desire_from_username_and_tag(self, username, tag_uuid):
        index = self.get_answer_index_from_username_uuid(username, tag_uuid)
        if index == NO_DESIRE_INDEX:
            return None
        value = self.get_value_from_tag_and_index(tag_uuid, index)
        if value == NO_DESIRE_VALUE:
            return None
        weight = self.get_user_weight_from_username_uuid(username, tag_uuid)
        desire_type = self.get_desire_type_from_tag_uuid(tag_uuid)
        return (value, weight, desire_type)

    def get_desire_from_answer(self, answer):
        "answer = (tag_uuid, index, weight)"
        "desire = (value, weight, desire_type)"
        tag_uuid, index, weight = answer
        value = self.get_value_from_tag_and_index(tag_uuid, index)
        desire_type = self.get_desire_type_from_tag_uuid(tag_uuid)
        return (value, weight, desire_type)
