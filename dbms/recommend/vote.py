from constants import *


class DatabaseVote():

    def get_tag_vote(self, uuid, tag_uuid, category):
        "returns the number of votes cast on this UUID for tag UUID, with a category like up or down."
        key = "tag_vote:" + uuid + ":" + tag_uuid + ":" + category
        tag_vote = self.redis.get(key)
        if not tag_vote:
            return 0
        return int(tag_vote.decode())

    def calculate_tag_vote_count(self, uuid):
        """returns the number of votes cast for all tags for this uuid.
        A vote can be up or down."""

        key = "tag_vote_count:" + uuid
        count = self.redis.get(key)
        if not count:
            return 0
        return int(count.decode())

    def set_tag_vote(self, uuid, tag_uuid, category, number):
        "Set how much this tag_uuid applies to this uuid, according to this category (up/down) and number of votes."

        if category not in TAG_VOTES_LIST:
            self.logger.warning(
                "set_tag_vote failed. TAG_VOTES_LIST does not contain category = '%s'" %
                category)
            return

        key = "tag_vote:" + uuid + ":" + tag_uuid + ":" + category
        number_before = self.get_tag_vote(uuid, tag_uuid, category)
        self.redis.set(key, number)

        "update the counter, which tracks the number of all votes cast on this uuid"
        key = "tag_vote_count:" + uuid
        old_count = self.calculate_tag_vote_count(uuid)
        new_count = old_count + number - number_before
        self.redis.set(key, new_count)

        "update the set of all tag uuids that this uuid has a vote for."
        key = "uuid_voted_tags:" + uuid + ":set"
        self.redis.sadd(key, tag_uuid)

    def get_total_votes_on_uuid(self, uuid, ignore_tags=[]):
        "Adds up the total number of votes associated to this UUID."
        total = 0
        for tag_uuid in self.get_voted_tags_from_uuid(uuid):
            if tag_uuid in ignore_tags:
                continue
            total += self.get_tag_vote(uuid, tag_uuid, TAG_VOTES.UP)
            total += self.get_tag_vote(uuid, tag_uuid, TAG_VOTES.DOWN)
        return total

    def get_all_tag_votes(self):
        "tag_votes={ (flo.uuid,tag.uuid,'vote_up') : 12 } "
        tag_votes = {}
        for uuid in self.get_uuids():
            for tag_uuid in self.get_voted_tags_from_uuid(uuid):
                for category in TAG_VOTES_LIST:
                    key = (uuid, tag_uuid, category)
                    tag_votes[key] = self.get_tag_vote(
                        uuid, tag_uuid, category)
        return tag_votes
