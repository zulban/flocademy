
class DatabaseUUID():

    """This class handles all functions whose arguments and return values are simply UUIDs."""

    def get_voted_tags_from_uuid(self, uuid):
        "returns a set of all tags with votes associated to this flo uuid"
        uuids = self.redis.smembers("uuid_voted_tags:" + uuid + ":set")
        return [i.decode() for i in uuids]

    def get_tag_uuids(self):
        return self.mongo.flos.find({"is_tag":{"$eq":True}}).distinct("_id")

    def get_schema_uuids(self):
        return self.mongo.flos.find({"is_schema":{"$eq":True}}).distinct("_id")

    def get_flo_count(self):
        return self.mongo.flos.count()

    def get_recommendable_count(self):
        return self.mongo.flos.count({"is_recommendable":{"$eq":True}})

    def is_uuid_a_tag(self, uuid):
        flo = self.get_flo_from_uuid(uuid)
        if not flo:
            return False
        return flo.get("is_tag",False)

    def get_uuids(self, include_tags=True):
        result=self.get_flos(include_tags=include_tags,return_mongo_cursor=True)
        return result.distinct("_id")

    def get_recommendable_uuids(self):
        return self.mongo.flos.find({"is_recommendable":{"$eq":True}}).distinct("_id")
    
    def count_recommendable_uuids(self):
        return self.mongo.flos.count({"is_recommendable":{"$eq":True}})
