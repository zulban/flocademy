import os
import os.path
import json

from utilities import *
from constants import *


class DatabaseFloManager():

    """This class handles FLO write, update, and delete."""

    def set_flo(self, flo):
        
        "deprecated FLO format has 'uuid' instead of '_id', convert if necessary before saving."
        if "uuid" in flo and "_id" not in flo:
            flo["_id"]=flo["uuid"]
            flo.pop("uuid")
        uuid=flo.get("_id")
                                
        self._mutate_flo_with_flocademy_keys(flo)
        self._update_children(flo)
        self._set_search_content(flo)
        
        "Adds this FLO to the database. Assumes FLO is valid."
        self.mongo.flos.update({"_id":flo.get("_id","")},
            flo,
            upsert=True)
    
    def _set_search_content(self,flo):
        """The current search system is very simple.
        This concatenates all strings in a FLO and caches it for later search queries."""
        text=concatenate_strings_from_data(flo).lower()
        key="search_content:"+flo.get("_id","")
        self.redis.set(key,text)
    
    def _update_children(self,flo):
        """Sets is_child to True for FLOs.
        If the parent to this FLO was already added, sets is_child to True for this FLO.
        If this FLO has children, sets is_child to True for them.
        """
        
        try:
            self.unset_children
        except:
            self.unset_children=set()
            
        def set_as_child(uuid,flo=None):
            f={"_id":uuid}
            self.mongo.flos.update(f,{"$set":{"is_child":True}})
            
            if not flo:
                flo = self.get_flo_from_uuid(uuid)
            schema_id=flo.get("schema_uuid","")
                
            if schema_id==UUID.MIRROR_SCHEMA:
                self.mongo.flos.update(f,{"$set":{"is_recommendable":False}})
            
        children=search_flo_for_children_uuids(flo)
        for child_uuid in children:
            f={"_id":child_uuid}
            if self.mongo.flos.count(f):
                set_as_child(child_uuid)
            else:
                self.unset_children.add(child_uuid)
        
        uuid=flo.get("_id")
        if uuid in self.unset_children:
            self.unset_children.remove(uuid)
            set_as_child(uuid,flo)
    
    def _mutate_flo_with_flocademy_keys(self, flo):
        """
        This mutates the FLO, setting the proper values for keys like is_schema, is_recommendable, etc.
        These are convenience values, and can be calculated from existing data.
        """
        
        for key in ("is_schema","is_tag","is_recommendable"):
            if key in flo:
                raise ValueError("found key '%s' in FLO:\n"+json.dumps(flo,indent=4,sort_keys=1))
        
        is_schema=is_flo_a_schema(flo)
        flo["is_schema"]=is_schema
        
        is_tag=flo.get("schema_uuid","") == UUID.TAG_SCHEMA
        flo["is_tag"]=is_tag
        
        is_recommendable=not is_schema and not is_tag
        flo["is_recommendable"]=is_recommendable

    def save_flo(self, flo, write_json_file=False):
        "If FLO is valid, add it to the database. Optionally create a new JSON file as well."
        "Returns True if successful."

        if not flo:
            self.logger.error("create_flo failed: flo is null")
            return False

        if type(flo) is not dict:
            self.logger.error("save_flo failed, flo is not a dictionary.")
            return False

        if not self.flo_validator.is_valid_flo(flo):
            self.logger.error("save_flo failed, invalid flo.")
            return False

        uuid = flo.get("uuid")
        if uuid in self.get_uuids():
            self.logger.error(
                "create_flo failed: uuid already created. uuid = '%s'" %
                uuid)
            return False

        self.set_flo(flo)

        if write_json_file:
            self.write_flo_json(flo)

        return True
