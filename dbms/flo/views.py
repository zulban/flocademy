import os
import os.path
import json

from constants import *
from utilities import is_string_a_uuid, search_flo_for_children_uuids


class DatabaseFloView():

    """This class produces various views of FLOs: planks, question planks, tag planks, etc."""

    def get_tags(self):
        uuids = self.get_tag_uuids()
        return [self.get_flo_from_uuid(uuid) for uuid in uuids]

    def get_flo_from_uuid(self, uuid):
        return self.mongo.flos.find_one({"_id":uuid})

    def get_next_question_plank(self, username):
        uuid = self.get_next_question_uuid(username)
        if not uuid:
            return {}
        question_plank = self.get_question_plank_from_username_uuid(
            username, uuid)

        "store prompt values in a separate dictionary so the entire dictionary can be set to false easily."
        key = "show_prompts"
        if key in question_plank:
            self.logger.warning(
                "get_next_question_plank failed. question_plank contains key = '%s'" %
                key)
            return {}

        question_plank[key] = {}

        "Insert a boolean for whether the explore prompt should first appear."
        question_plank[key][
            "explore"] = self.should_show_explore_prompt(username)

        "Insert a boolean for whether the stars prompt should first appear."
        question_plank[key]["stars"] = self.should_show_stars_prompt(username)

        return question_plank

    def get_flo_planks(self, username):
        "returns a list of FLO plank_data for all FLOs"
        planks = [self.get_plank_from_username_uuid(username,
                                                    uuid) for uuid in self.get_uuids()]
        return sorted(planks, key=lambda x: x["title"].lower())

    def get_flos(self, include_tags=True, return_mongo_cursor=False):
        f={}
        if not include_tags:
            f["is_tag"]={"$ne":True}
        result=self.mongo.flos.find(f)
        return result if return_mongo_cursor else list(result)

    def get_plank_from_username_uuid(self, username, uuid):
        flo = self.get_flo_from_uuid(uuid)
        if not flo:
            self.logger.error(
                "get_plank_from_username_uuid unknown uuid = '%s'" %
                uuid)
            return {}

        title = flo.get("title", "Untitled FLO")
        description = flo.get("description", "No description.")
        schema_uuid = flo.get("schema_uuid", "")
        schema = self.get_flo_from_uuid(schema_uuid)
        if schema:
            schema_title = schema.get("title", "Untitled schema")
        else:
            if "_schema" in flo:
                schema_title = "Meta"
            else:
                self.logger.error("db: flo has no schema for plank_data.")
                schema_title = "Unknown schema"

        thumbnail = self.get_thumbnail_for_flo(flo)
        plank_tags = self.get_plank_tags_from_uuid(uuid)
        active_plank_buttons = self.get_active_plank_buttons(username, uuid)
        plank_data = {"title": title,
                      "uuid": uuid,
                      "description": description,
                      "active_plank_buttons": active_plank_buttons,
                      "schema_uuid": schema_uuid,
                      "schema_title": schema_title,
                      "tags": plank_tags,
                      "thumbnail": thumbnail}
        return plank_data

    def get_plank_tags_from_uuid(self, uuid):
        plank_tags = []
        for tag_uuid in self.get_most_relevant_tags(uuid):
            up = self.get_tag_vote(uuid, tag_uuid, TAG_VOTES.UP)
            down = self.get_tag_vote(uuid, tag_uuid, TAG_VOTES.DOWN)
            if up or down:
                percent = int(100 * up / (up + down))
            else:
                percent = 50
            flo = self.get_flo_from_uuid(tag_uuid)
            label_up = flo.get("label_up", STRINGS.DEFAULT_LABEL_UP)
            label_down = flo.get("label_down", STRINGS.DEFAULT_LABEL_DOWN)
            title = flo.get("title", "")
            subtitle = flo.get("subtitle", "")

            plank_tag = {
                "title": title,
                "subtitle": subtitle,
                "percent": percent,
                "label_up": label_up,
                "label_down": label_down}
            plank_tags.append(plank_tag)
        return plank_tags

    def is_flo_a_tag(self, flo):
        return flo.get("schema_uuid") == UUID.TAG_SCHEMA

    def get_question_plank_from_username_uuid(self, username, uuid):
        if not uuid:
            return {}
        flo = self.get_flo_from_uuid(uuid)
        if not self.is_flo_a_tag(flo):
            self.logger.error(
                "get_question_plank_from_username_uuid failed. not a tag uuid = '%s'" %
                uuid)
            return {}
        answers = [answer["text"] for answer in flo["answers"]]
        weight = self.get_user_weight_from_username_uuid(username, uuid)
        selected_answer = self.get_answer_index_from_username_uuid(
            username, uuid)
        question = {"prompt": flo["prompt"],
                    "uuid": flo["_id"],
                    "description": flo["description"],
                    "answers": answers,
                    "selected_answer": selected_answer,
                    "title": flo["title"],
                    "subtitle": flo.get("subtitle", ""),
                    "user_weight": weight}
        return question

    def get_question_planks_from_username(self, username, alpha_sort=True):
        uuids = self.get_answered_uuids_from_username(username)
        planks = [
            self.get_question_plank_from_username_uuid(
                username,
                uuid) for uuid in uuids]
        if alpha_sort:
            planks = sorted(planks, key=lambda x: x["title"] + x["subtitle"])
        return planks
