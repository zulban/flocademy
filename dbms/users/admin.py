
class DatabaseAdmin():

    def set_admin_level(self, username, admin_level):
        self.mongo.users.update({"_id":username},
            {"$set":{"admin_level":admin_level}})

    def get_admin_level(self, username):
        result=self.mongo.users.find_one({"_id":username})
        return 0 if not result else result.get("admin_level",0)
