import hashlib
import time
from werkzeug.security import check_password_hash, generate_password_hash

from utilities import generate_random_password, EXPLORE_PROMPT_QUESTION_THRESHOLD, is_valid_username, is_valid_password
from constants import *


class DatabaseAccount():

    """This class manages account data, settings, passwords, user lists."""

    def is_uuid_on_user_list(self, uuid, username, list_name):
        user_lists=self.mongo.user_lists.find_one({"_id":username})
        if not user_lists:
            return False
        return uuid in user_lists.get(list_name,[])

    def add_uuid_to_user_list(self, uuid, username, list_name):
        if list_name not in USER_LIST_NAMES:
            self.logger.error("got a weird list_name = '%s'" % list_name)
            return
        
        f={"_id":username}
        self.mongo.user_lists.update(f,
            {"$push":{list_name:uuid}},
            upsert=True)
        self.mongo.user_lists.update(f,
            {"$push":{"plank_buttons."+uuid:list_name}},
            upsert=True)
        
        if list_name == USER_LISTS.HIDDEN:
            "Set the UUID score to negative so it doesn't get recommended."
            self.recalculate_score(username, uuid)
            if self.get_answer_count_from_username(username) == 0:
                "No user answers means this negative score is higher than the other null scores."
                "Recalculate all sets all to zeros, avoids a fringe bug."
                self.recalculate_scores(username)

    def remove_uuid_from_user_list(self, uuid, username, list_name):
        if list_name not in USER_LIST_NAMES:
            self.logger.error("got a weird list_name = '%s'" % list_name)
            return
        
        self.mongo.user_lists.update({"_id":username},
            {"$pull":{list_name:uuid,"plank_buttons."+uuid:list_name}})

        if list_name == USER_LISTS.HIDDEN:
            "Adding to hidden sets score to very negative, so recalculation is necessary to remove from hidden."
            self.recalculate_score(username, uuid)

    def get_user_list(self, username, list_name):
        "returns a list of uuids in the list 'list_name' for this username."
        if list_name not in USER_LIST_NAMES:
            self.logger.error("got a weird list_name = '%s'" % list_name)
            return
        return self.mongo.user_lists.find({"_id":username}).distinct(list_name)

    def get_user_list_count(self, username, list_name):
        result=self.mongo.user_lists.find({"_id":username}).distinct(list_name)
        return len(result)

    def get_active_plank_buttons(self, username, uuid):
        result=self.mongo.user_lists.find_one({"_id":username})
        return [] if not result else result.get("plank_buttons",{}).get(uuid,[])

    def is_email_verified_for_username(self, username):
        result = self.mongo.users.find_one({"_id":username})
        return False if not result else result.get("verified_email")

    def set_email_verified_for_username(self, username, is_verified):
        a=1 if is_verified else 0
        self.mongo.users.update({"_id":username},{"$set":{"verified_email":a}})

    def get_pw_hash_from_username(self, username):
        result = self.mongo.users.find_one({"_id":username})
        return "<no username hash>" if not result else result.get("pw_hash","<no pw_hash found for user>")

    def get_email_from_username(self, username):
        result = self.mongo.users.find_one({"_id":username})
        return "" if not result else result.get("email","")

    def get_display_name_from_username(self, username):
        result = self.mongo.users.find_one({"_id":username})
        return "" if not result else result.get("display_name","")

    def create_user(self, username, password, email,
                    admin_level=0, ip="N/A", user_agent="N/A"):
        if not is_valid_username(username) or not is_valid_password(password):
            self.logger.error(
                "create_user called with invalid username or password. username = '%s' password = '%s'" %
                (username, password))
            return None
        
        register_info={"time":time.time(),
                       "ip":ip,
                       "user_agent":user_agent}
        doc={"_id":username,
             "pw_hash":generate_password_hash(password),
             "email":email,
             "display_name":username,
             "register_info":register_info,
             "admin_level":admin_level}
        
        try:
            self.mongo.users.insert(doc)
        except pymongo.errors.DuplicateKeyError:
            self.logger.error(
                "create_user called, user already exists. Doing nothing.")
            return None

    def set_user_password(self, username, password):
        pw_hash = generate_password_hash(password)
        self.mongo.users.update({"_id":username},
            {"$set":{"pw_hash":pw_hash}})

    def set_user_email(self, username, email, set_unverified=True):
        self.mongo.users.update({"_id":username},
            {"$set":{"email":email}})
        if set_unverified:
            self.set_email_verified_for_username(username, False)

    def set_user_display_name(self, username, display_name):
        self.mongo.users.update({"_id":username},
            {"$set":{"display_name":display_name}})

    def drop_users(self):
        self.mongo.users.drop()

    def get_usernames(self):
        return self.mongo.users.find().distinct("_id")

    def transition_from_anonymous(self, anonymous_username, username):
        "This transfers the anonymous account's answers to username, then deletes the anonymous answers."

        keys = (
            "answer_indexes:%s:hash", "answered_uuids:%s:set", "answered_uuids:%s:list",
              "uuid_recommendations:%s", "user_weights:%s:hash", "prompt_answer:%s")
        for key in keys:
            anon_key = key % anonymous_username
            if self.redis.exists(anon_key):
                self.redis.rename(anon_key, key % username)
                self.redis.delete(anon_key)
        
        "if there are anonymous answers, rename the _id from anonymous to this new username"
        f={"_id":anonymous_username}
        if self.mongo.tag_answers.count(f)>0:
            "mongodb cannot rename _id, must find, copy, remove old, and insert"
            doc=self.mongo.tag_answers.find_one(f)
            doc["_id"]=username
            self.mongo.tag_answers.remove(f)
            self.mongo.tag_answers.insert_one(doc)

    def is_username_taken(self, username):
        return bool(self.mongo.users.find_one({"_id":username}))

    def is_username_anonymous(self, username):
        return not self.is_username_taken(username)

    def reset_user_password(self, username):
        "Clears the password for 'username' and returns a new random password."
        new_password = generate_random_password()
        if self.is_username_taken(username):
            self.set_user_password(username, new_password)
            return new_password
        else:
            self.logger.warning(
                "reset_user_password attempted on a non-existing username: '%s'" %
                username)
            return new_password

    def check_password(self, username, attempt):
        "Returns true if 'attempt' is the correct password for this username."
        h = self.get_pw_hash_from_username(username)
        return check_password_hash(h, attempt)
