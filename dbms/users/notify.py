from constants import *


class DatabaseNotify():

    """This class handles all the notifications and prompts for users."""

    def set_prompt_answer(self, username, prompt_name, answer):
        self.mongo.notifications.update({"_id":username},
            {"$set":{prompt_name:answer}},
            upsert=True)
        
    def get_prompt_answer(self, username, prompt_name):
        result=self.mongo.notifications.find_one({"_id":username})
        return 0 if not result else result.get(prompt_name,0)

    def should_show_explore_prompt(self, username):
        """The 'explore' prompt asks the user to view current recommendations, or continue answering questions.
        This returns true if this prompt should appear instead of a question."""
        has_seen = self.get_prompt_answer(username, PROMPTS.EXPLORE)
        if has_seen:
            return False
        count = self.get_answer_count_from_username(username)
        return count == EXPLORE_PROMPT_QUESTION_THRESHOLD

    def should_show_stars_prompt(self, username):
        """The 'stars' prompt explains the star system.
        This returns true if this prompt should appear instead of a question."""
        has_seen = self.get_prompt_answer(username, PROMPTS.STARS)
        if has_seen:
            return False
        count = self.get_answer_count_from_username(username)
        return count == STARS_PROMPT_QUESTION_THRESHOLD
