from constants import *
import time
from utilities import random_uuid, random_join_code


class DatabaseGroup():

    """This class manages all user group functionality."""

    def create_group(self, username, group_name, color):
        group_id=random_uuid()
        doc={"_id":group_id,
             "create_time":time.time(),
             "creator":username,
             "name":group_name,
             "color":color,
             "users":[]}
        self.mongo.groups.insert_one(doc)
        
        self.join_group(username, group_id, rights="rw")
        return group_id

    def delete_group(self, group_id):
        self.mongo.groups.delete_one({"_id":group_id})

    def set_group_name(self, group_id, group_name):
        self.mongo.groups.update({"_id":group_id},
            {"$set":{"name":group_name}})

    def get_group_name(self, group_id):
        result=self.mongo.groups.find_one({"_id":group_id})
        return "" if not result else result.get("name","")

    def set_group_color(self, group_id, color):
        self.mongo.groups.update({"_id":group_id},
            {"$set":{"color":color}})

    def get_group_color(self, group_id):
        default="00FF00"
        result=self.mongo.groups.find_one({"_id":group_id})
        return default if not result else result.get("color",default)

    def group_id_exists(self, group_id):
        result=self.mongo.groups.find_one({"_id":group_id})
        return bool(result)

    def group_exists_from_join_code(self, join_code):
        join_code = join_code.upper()
        group_id = self.get_group_id_from_join_code(join_code)
        return self.group_id_exists(group_id)

    def get_group_ids_from_username(self, username):
        "Returns group_ids for all groups this username has joined."
        result=self.mongo.users.find_one({"_id":username})
        return [] if not result else result.get("groups",[])

    def join_group(self, username, group_id, rights):        
        if not self.group_id_exists(group_id):
            raise ValueError(
                "join_group failed, group doesn't exist. username='%s' group_id='%s' rights='%s'" %
                (username, group_id, rights))
        if self.is_username_in_group_id(username, group_id):
            self.logger.warning(
                "join_group: username '%s' already in group '%s'." %
                (username, group_id))
        else:
            doc={"username":username,
                 "join_time":time.time()}
            self.mongo.groups.update({"_id":group_id},
                {"$push":{"users":doc}})
            self.mongo.users.update({"_id":username},
                {"$push":{"groups":group_id}})
            
            self.set_group_rights(username, group_id, rights)

    def leave_group(self, username, group_id):
        if not self.group_id_exists(group_id):
            raise ValueError(
                "leave_group failed, group doesn't exist. username='%s' group_id='%s'" %
                (username, group_id))
        if not self.is_username_in_group_id(username, group_id):
            self.logger.warning(
                "leave_group aborted: username '%s' not in group '%s'." %
                (username, group_id))
        else:
            self.mongo.groups.update({"_id":group_id},
                {"$pull":{"users":{"username":username}}})
            self.mongo.users.update({"_id":username},
                {"$pull":{"groups":group_id}})

    def is_username_in_group_id(self, username, group_id):
        result=self.mongo.users.find_one({"_id":username})
        return False if not result else group_id in result.get("groups",[])

    def get_usernames_from_group_id(self, group_id):
        result=self.mongo.groups.find_one({"_id":group_id})
        if not result:
            return []
        return [item.get("username","") for item in result.get("users",[])]

    def set_group_rights(self, username, group_id, rights=""):
        """rights:
        'w' means write/edit/delete rights usually for owners
        'r' means read/spy rights usually for collaborators
        'rw' is both
        '' is a regular member."""
        self.mongo.groups.update({"_id":group_id,"users.username":username},
            {"$set":{"users.$.rights":rights}})

    def get_group_rights(self, username, group_id):
        result=self.mongo.groups.find_one({"_id":group_id})
        if not result:
            return ""
        for item in result.get("users",[]):
            if item.get("username","")==username:
                return item.get("rights","")
        self.logger.warning(
                "get_group_rights did not find username '%s' in group '%s'." %
                (username, group_id))
        return ""

    def get_group_join_code(self, group_id):
        result=self.mongo.groups.find_one({"_id":group_id})
        if not result:
            return ""

        "if a group code exists already, simply return that."
        code = result.get("join_code","")
        if code:
            return code

        "generate new code that isn't used elsewhere"
        used_codes=self.mongo.groups.find().distinct("join_code")
        code = random_join_code()
        while code in used_codes:
            code+=random_join_code()[0]

        "update group with new code"
        self.mongo.groups.update({"_id":group_id},
            {"$set":{"join_code":code}})
        return code

    def get_group_lock_status(self, group_id):
        default=False
        result=self.mongo.groups.find_one({"_id":group_id})
        return default if not result else result.get("is_locked",default)

    def set_group_lock_status(self, group_id, is_locked):
        self.mongo.groups.update({"_id":group_id},
            {"$set":{"is_locked":is_locked}})

    def reset_group_join_code(self, group_id):
        self.mongo.groups.update({"_id":group_id},
            {"$set":{"join_code":""}})

    def get_group_id_from_join_code(self, join_code):
        "Returns the group id corresponding to this group join code."
        result=self.mongo.groups.find_one({"join_code":join_code})
        return "" if not result else result.get("_id","")

    def join_group_with_join_code(self, username, join_code, rights):
        join_code = join_code.upper()
        group_id = self.get_group_id_from_join_code(join_code)
        self.join_group(username, group_id, rights)
        return group_id

    def get_group_panel_from_group_id(self, group_id):
        "A group panel contains all the basic information to visually display a group."
        return {"group_id": group_id,
                "name": self.get_group_name(group_id),
                "color": self.get_group_color(group_id)}

    def get_group_panels_from_username(self, username):
        "Returns panel information for all groups for this user."
        group_ids = self.get_group_ids_from_username(username)
        group_panels = [
            self.get_group_panel_from_group_id(
                gid) for gid in group_ids]
        return group_panels

    def get_member_panels(self, group_id):
        "Returns a list of group member panels, starting with highest permission users."
        usernames = self.get_usernames_from_group_id(group_id)
        planks = []
        for username in usernames:
            plank = self.get_member_panel_from_username_group_id(
                username, group_id)
            planks.append(plank)
        planks = sorted(
            planks,
            key=lambda x: len(x["rights"]),
            reverse=True)
        return planks

    def get_member_panel_from_username_group_id(self, username, group_id):
        "A member panel has all the information for one user for a group page."
        display_name = self.get_display_name_from_username(username)
        gravatar_url = self.get_gravatar_url_from_username(username)
        rights = self.get_group_rights(username, group_id)
        if "w" in rights:
            role = "Owner"
        elif "r" in rights:
            role = "Collaborator"
        else:
            role = "Learner"

        counters = {"questions": self.get_answer_count_from_username(username)}
        for list_name in USER_LIST_NAMES:
            counters[list_name] = self.get_user_list_count(username, list_name)

        return {"username": username, "display_name": display_name,
                "role": role, "gravatar_url":
                    gravatar_url, "counters": counters,
                "rights": rights}
