#!venv/bin/python3

"""Database Manager

Run scripts that create, remove, update, and destroy data in the databases. Also performs database dumps.

"drop" clears all target databases.
"update" loads data into the target databases. Source can be a JSON, database CSV script, database dump, or a folder containing combinations of those. Source can also be a colon delimited list of paths, for example:  /home/zulban:/home/alex
"dump" creates a dump of all databases in the target folder.
"createadmin" Prompts a username and password for a new admin account.

Data Cleanup Utilities:
   "pretty" auto-indents and key sorts all FLOs in a target folder.
   "flomove" moves all FLOs in the target folder, into their automatically named folder paths in data/.
   "floclean" removes empty key values in FLOs, like 'file-count'=0 inserted by new FLO web interface. Also runs 'pretty' after.
   "uuid" replaces UUIDs so that they are 32 character hex. The same UUID across multiple files gets the same 32-hex UUID.

Usage:
   manage_data.py drop <environment> [--no-confirmation]
   manage_data.py update <environment> <folders-or-folder-or-file> [--verbose] [--no-confirmation]
   manage_data.py createadmin <environment>
   manage_data.py pretty <flo-folder>
   manage_data.py uuid <flo-folder>
   manage_data.py flomove <flo-folder>
   manage_data.py floclean <flo-folder>

Options:
    --verbose           More console output.
    --no-confirmation   Skip having the user confirm database operations.

    -h --help           Show this screen.
    -v --version        Show version.
"""

import os
import unittest
import time
import getpass
import shutil
from dbms import db

from utilities import *


def run_create_admin():
    print("Creating new admin account.")
    username = input(
        "New admin username. Press enter to just use '%s'.\n: " %
        getpass.getuser())
    if not username:
        username = getpass.getuser()
    password = getpass.getpass("Password for '%s': " % username)
    confirm = getpass.getpass("Confirm password for '%s': " % username)
    if password != confirm:
        print_red("Oops, no match. Abort.")
        return
    db.create_user(username, password, "", admin_level=1)


def run_db_operation(label, func, ask_confirmation=True):
    "Run a generic database operation. Can ask for user confirmation."
    print("Performing '%s'.\n   Redis database '%s'. Mongo collection '%s'" %
          (label, db.config.REDIS_DB, db.config.MONGO_DB_COLLECTION))

    if ask_confirmation:
        expected = "yes"
        print_green(
            "Run '%s'?\nType '%s' (exactly) to confirm." %
            (label, expected))
        confirmation = input(": ")

    if not ask_confirmation or confirmation == expected:
        start_time = time.time()
        func()
        print("Operation took '%.3f' seconds." % (time.time() - start_time))
    else:
        print_red("Aborted.")


def main(args):
    env = args["<environment>"]
    ask_confirmation = not args["--no-confirmation"]
    verbose = args["--verbose"]

    try:
        configuration = get_settings_from_env(env, verbose=verbose)
    except ValueError:
        print("Aborted. Bad environment: '%s'" % env)
        return

    db.configure(configuration)

    if args["drop"]:
        run_db_operation("database drop for %s" % env,
                         lambda: db.drop_all(),
                         ask_confirmation=ask_confirmation)

    if args["update"]:
        update_paths = args["<folders-or-folder-or-file>"].split(":")
        run_db_operation("database update for %s" % env,
                         lambda: [db.update(p, verbose=verbose)
                                  for p in update_paths],
                         ask_confirmation=ask_confirmation)

    if args["createadmin"]:
        run_create_admin()

    if args["pretty"]:
        folder = args["<flo-folder>"]
        clean_flos(folder, prettify=True)

    if args["uuid"]:
        folder = args["<flo-folder>"]
        uuid_repair(folder)

    if args["flomove"]:
        folder = args["<flo-folder>"]
        fm = FloMover(PATH.DATA)
        fm.move_flos(folder)

    if args["floclean"]:
        folder = args["<flo-folder>"]
        clean_flos(folder, prettify=True, clean_mirrors=True)

    print("Done.")

if __name__ == "__main__":
    args = docopt(__doc__, version="1.0")
    main(args)
