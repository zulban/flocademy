#!venv/bin/python3

"""Flask App Manager

Run the app or app features. App is configured according to the FLOCADEMY_ENV environment variable.

"run" runs a server in any configuration for testing and development.
"test" runs tests. There are unit (including function) tests, story tests (browser), and flo validation tests.
"--coverage" shows a code coverage report upon completion of the tests.

Usage:
   manage_app.py run
   manage_app.py test [--story] [--unit] [--flo] [--verbose] [--headless-browser] [--coverage]

Options:
    --verbose           More console output.

    -h --help           Show this screen.
    -v --version        Show version.
"""

import os
import unittest
import time
import webbrowser
import shutil
from app import app, configure_app
from coverage import coverage

from utilities import *


def run_flo_validation(folder, verbose=True):
    fv = FLOValidator(DATA_PATHS.SCHEMAS, verbose=verbose)
    fv.verify_flos_in_folder(folder)


def run_test_suites(folders):
    "Runs all the test suites found in these folders."
    paths = []
    for folder in folders:
        paths += get_matching_paths_recursively(folder, ".py")
    modules = [path[:-3].replace(os.sep, ".") for path in paths]
    suites = [unittest.defaultTestLoader.loadTestsFromName(m) for m in modules]
    test_suite = unittest.TestSuite(suites)
    unittest.TextTestRunner().run(test_suite)


def run_tests(code_coverage=False, run_unit=False,
              run_story=False, run_flo=False, verbose=False, headless=False):
    configure_app(app, env="testing")

    if code_coverage:
        cov = coverage(
            branch=True,
            omit=[
                'flask/*',
                'tests/*',
                'analysis/*',
                'venv/*'])
        cov.start()

    folders = []
    if run_unit:
        folders.append(PATH.UNIT_TESTS)
    if run_story:
        if headless:
            app.config["HEADLESS_BROWSER_TESTS"] = True
        folders.append(PATH.STORY_TESTS)
    run_test_suites(folders)

    if run_flo:
        run_flo_validation(PATH.DATA, verbose=verbose)

    if code_coverage:
        cov.stop()
        cov.save()
        shutil.rmtree(PATH.CODE_COVERAGE,ignore_errors=True)
        os.makedirs(PATH.CODE_COVERAGE)
        percent = cov.html_report(directory=PATH.CODE_COVERAGE)
        print("Code coverage: %s%%" % round(percent, 2))
        print("Code coverage report: '%s'" % PATH.CODE_COVERAGE)
        webbrowser.open(PATH.CODE_COVERAGE + os.sep + "index.html")


def main(args):
    if args["run"]:
        app.run(host=app.config["HOST"], debug=app.config["TESTING"])

    if args["test"]:
        run_tests(run_unit=args["--unit"],
                  run_story=args["--story"],
                  run_flo=args["--flo"],
                  verbose=args["--verbose"],
                  headless=args["--headless-browser"],
                  code_coverage=args["--coverage"])

    print("Done.")

if __name__ == "__main__":
    args = docopt(__doc__, version="1.0")
    main(args)
