
""" FLOcademy Performance Analyzer

Reports the runtime of various operations.

Usage:
  performance.py [options]

Options:
    --show-plot          Show plots.
    -h --help           Show this screen.
    -v --version        Show version.
"""

import time
import math
import matplotlib.pyplot as pyplot

from dbms.database_manager import DatabaseManager
from config import TestingSettings
from utilities import *


def plot_and_time(func):
    "decorator for PerformanceUtility"

    def func_wrapper(self):
        start_time = time.time()
        x, y = func(self)
        total_time = time.time() - start_time
        if self.show_plot:
            self.setup_plot(x, y, func.__name__, "", "")
        print_yellow(
            str(func.__name__) + " took %s seconds." %
            round(total_time, 2))
        return x, y
    return func_wrapper


class PerformanceUtility():

    def __init__(self, db, show_plot, verbose=False):
        self.db = db
        self.db.drop_all()

        self.show_plot = show_plot
        self.verbose = verbose

        self.score = 0
        self.trial_count = 0

    def set_mock_data(self, flo_count, tag_count):
        "Creates FLOs and tags. FLOs also get tag votes set for all tags."
        if self.verbose:
            print_yellow(
                "Setting mock data.\n   FLOs = %s\n   Tags = %s" %
                (flo_count, tag_count))

        for i in range(flo_count):
            flo_uuid = "flo-%s" % i
            flo = {"uuid": flo_uuid, "schema_uuid": UUID.VIDEO_LESSON_SCHEMA}
            self.db.set_flo(flo)
            for j in range(tag_count):
                tag_uuid = "flo-tag-%s" % j
                flo_tag = {"uuid": tag_uuid, "schema_uuid": UUID.TAG_SCHEMA}
                self.db.set_flo(flo_tag)
                self.db.set_tag_vote(flo_uuid, tag_uuid, TAG_VOTES.UP, i + 5)
                self.db.set_tag_vote(flo_uuid, tag_uuid, TAG_VOTES.DOWN, i + 5)

    def answer_all_questions(self, n):
        "n users each submit an answer for all tags."
        expire_time = 1000
        timer = Timer(5)
        for i in range(n):
            username = "username-%s-%s" % (i, random_uuid)
            for j, tag_uuid in enumerate(self.db.get_tag_uuids()):
                timer.tick(
                    "answer_all_questions(%s) j=%s tag_count=%s" %
                    (n, j, len(self.db.get_tags())))
                answer = (tag_uuid, 1, DEFAULT.WEIGHT)
                self.db.set_answer(
                    username,
                    answer,
                    expire_time,
                    adjust_scores=False,
                    recalculate_scores=False)
        self.db.recalculate_scores()

    @plot_and_time
    def flo_count_versus_time(self):
        self.db.drop_all()
        x, y = [], []
        user_count = 1
        for i in range(1, 8):
            flo_count = 10 * i ** 2
            tag_count = 5
            self.set_mock_data(flo_count, tag_count)

            start_time = time.time()
            self.answer_all_questions(user_count)
            total_time = time.time() - start_time
            x.append(flo_count)
            y.append(total_time)

        self.add_to_score(sum(y))
        return x, y

    def setup_plot(self, x, y, title, xlabel, ylabel):
        pyplot.figure(self.trial_count)
        pyplot.plot(x, y, "ro")
        pyplot.axis([0, max(x), 0, max(y)])
        pyplot.ylabel(ylabel)
        pyplot.xlabel(xlabel)
        pyplot.title(title)

    def add_to_score(self, amount):
        self.score += amount
        self.trial_count += 1

    def show_score(self):
        print_green(
            "\n%s trials. Performance Score = %s" %
            (self.trial_count, round(self.score, 3)))

    @plot_and_time
    def tag_count_versus_time(self):
        self.db.drop_all()
        x, y = [], []
        user_count = 1
        for i in range(1, 10):
            flo_count = 10
            tag_count = 2 * i ** 2
            self.set_mock_data(flo_count, tag_count)

            start_time = time.time()
            self.answer_all_questions(user_count)
            total_time = time.time() - start_time
            x.append(tag_count)
            y.append(total_time)

        self.add_to_score(sum(y))
        return x, y


def main(args):
    db = DatabaseManager()
    db.configure(TestingSettings)

    show_plot = args["--show-plot"]
    pu = PerformanceUtility(db, show_plot)

    pu.flo_count_versus_time()
    pu.tag_count_versus_time()

    pu.show_score()

    if show_plot:
        pyplot.show()

if __name__ == "__main__":
    args = docopt(__doc__, version="1.0")
    main(args)
